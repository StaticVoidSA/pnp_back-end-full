﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RnR.Data.DTOs;
using RnR.Data.Generic;

namespace RnR.Data.DAOs
{
    #region Delegates

    public delegate Task<List<SearchResponseDTO>> _OnStartup();
    public delegate Task<List<SearchResponseDTO>> _FilterBy(SearchRequestDTO request);

    #endregion

    #region Intefaces

    public interface IShop
    {
        public Task<List<SearchResponseDTO>> onStartup();
        public Task<List<SearchResponseDTO>> filterByBrand(SearchRequestDTO request);
        public Task<List<SearchResponseDTO>> filterByPrice(SearchRequestDTO request);
        public Task<List<SearchResponseDTO>> filterByQuantity(SearchRequestDTO request);
    }

    #endregion

    #region Shop Service

    public class ShopService : IShop
    {

        SQLFunctions sqlFunctions = null;

        private static event _OnStartup startup;
        private static event _FilterBy filter;

        public ShopService(string _connectionString, string _query)
        {
            sqlFunctions = new SQLFunctions(_connectionString, _query);
        }

        public async Task<List<SearchResponseDTO>> onStartup()
        {
            startup += new _OnStartup(sqlFunctions.OnStartupShop);
            List<SearchResponseDTO> response = await startup.Invoke();
            startup -= new _OnStartup(sqlFunctions.OnStartupShop);
            return response;
        }

        public async Task<List<SearchResponseDTO>> filterByBrand(SearchRequestDTO request)
        {
            filter += new _FilterBy(sqlFunctions.ShopFilterByBrand);
            List<SearchResponseDTO> response = await filter.Invoke(request);
            filter -= new _FilterBy(sqlFunctions.ShopFilterByBrand);
            return response;
        }

        public async Task<List<SearchResponseDTO>> filterByPrice(SearchRequestDTO request)
        {
            filter += new _FilterBy(sqlFunctions.ShopFilterByPrice);
            List<SearchResponseDTO> response = await filter.Invoke(request);
            filter -= new _FilterBy(sqlFunctions.ShopFilterByPrice);
            return response;
        }

        public async Task<List<SearchResponseDTO>> filterByQuantity(SearchRequestDTO request)
        {
            filter += new _FilterBy(sqlFunctions.ShopFilterByQuantity);
            List<SearchResponseDTO> response = await filter.Invoke(request);
            filter -= new _FilterBy(sqlFunctions.ShopFilterByQuantity);
            return response;
        }
    }

    #endregion
}
