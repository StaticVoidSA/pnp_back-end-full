﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RnR.Data.DTOs;
using RnR.Data.Generic;

namespace RnR.Data.DAOs
{
    #region Delegates

    public delegate Task<IEnumerable<SearchResponseDTO>> _Catalogue();
    public delegate Task<List<SearchResponseDTO>> _SearchAll (SearchRequestDTO request);

    #endregion

    #region Interfaces

    public interface ISearch
    {
        public Task<IEnumerable<SearchResponseDTO>> Catalogue();
        public Task<List<SearchResponseDTO>> SearchAll(SearchRequestDTO request);
    }

    #endregion

    public class SearchService : ISearch
    {
        #region Constructor

        SQLFunctions sqlFunctions = null;

        private static event _Catalogue getCatalogue;
        private static event _SearchAll search;

        public SearchService(string _connectionString, string _query)
        {
            sqlFunctions = new SQLFunctions(_connectionString, _query);
        }

        #endregion


        public async Task<IEnumerable<SearchResponseDTO>> Catalogue()
        {
            getCatalogue += new _Catalogue(sqlFunctions.catalogue);
            IEnumerable<SearchResponseDTO> response = await getCatalogue.Invoke();
            getCatalogue -= new _Catalogue(sqlFunctions.catalogue);
            return response;
        }


        public async Task<List<SearchResponseDTO>> SearchAll(SearchRequestDTO request)
        {
            search += new _SearchAll(sqlFunctions.searchAll);
            List<SearchResponseDTO> response = await search.Invoke(request);
            search -= new _SearchAll(sqlFunctions.searchAll);
            return response;
        }
    }
}
