﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RnR.Data.DTOs;
using RnR.Data.Generic;

namespace RnR.Data.DAOs
{
    #region Delegates

    public delegate Task<bool> _CreateList(ShoppingListDTO list);
    public delegate Task<List<ListsDTO>> _GetUserLists(int userID);
    public delegate Task<bool> _DeleteList(int itemID, int userID);
    public delegate Task<bool> _AddToList(ShoppingListDTO list);
    public delegate Task<List<ShoppingListDTO>> _GetShoppingListItems(int userID, string listName);
    public delegate Task<bool> _DeleteFromList(int listID, int ID);
    public delegate Task<UserDTO> _GetUserDetails(int userID);
    public delegate Task<int> _GetListsCount(int userID);

    #endregion

    #region Interfaces

    public interface IShoppingList
    {
        public Task<bool> CreateList(ShoppingListDTO list);
        public Task<List<ListsDTO>> GetUserLists(int userID);
        public Task<bool> DeleteShoppingList(int itemID, int userID);
        public Task<bool> AddToShoppingList(ShoppingListDTO list);
        public Task<List<ShoppingListDTO>> GetListItems(int userID, string listName);
        public Task<bool> DeleteFromShoppingList(int listID, int ID);
        public Task<UserDTO> GetUserDetails(int userID);
        public Task<int> GetListsCount(int userID);
    }

    #endregion

    #region Shopping List Service

    public class ShoppingListService : IShoppingList
    {
        #region Constructor

        SQLFunctions sqlFunctions = null;

        private static event _CreateList create;
        private static event _GetUserLists getLists;
        private static event _DeleteList deleteList;
        private static event _AddToList add;
        private static event _GetShoppingListItems getListItems;
        private static event _DeleteFromList deleteFromList;
        private static event _GetUserDetails getUser;
        private static event _GetListsCount getCount;

        public ShoppingListService(string _connectionString, string _query)
        {
            sqlFunctions = new SQLFunctions(_connectionString, _query);
        }

        #endregion

        public async Task<bool> CreateList(ShoppingListDTO list)
        {
            create += new _CreateList(sqlFunctions.CreateShoppingList);
            bool response = await create.Invoke(list);
            create -= new _CreateList(sqlFunctions.CreateShoppingList);
            return response;
        }

        public async Task<List<ListsDTO>> GetUserLists(int userID)
        {
            getLists += new _GetUserLists(sqlFunctions.GetUserLists);
            List<ListsDTO> response = await getLists.Invoke(userID);
            getLists -= new _GetUserLists(sqlFunctions.GetUserLists);
            return response;
        }

        public async Task<bool> DeleteShoppingList(int itemID, int userID)
        {
            deleteList += new _DeleteList(sqlFunctions.DeleteShoppingList);
            bool response = await deleteList.Invoke(itemID, userID);
            deleteList -= new _DeleteList(sqlFunctions.DeleteShoppingList);
            return response;
        }

        public async Task<bool> AddToShoppingList(ShoppingListDTO list)
        {
            add += new _AddToList(sqlFunctions.AddToShoppingList);
            bool response = await add.Invoke(list);
            add -= new _AddToList(sqlFunctions.AddToShoppingList);
            return response;
        }

        public async Task<List<ShoppingListDTO>> GetListItems(int userID, string listName)
        {
            getListItems += new _GetShoppingListItems(sqlFunctions.GetListItems);
            List<ShoppingListDTO> response = await getListItems.Invoke(userID, listName);
            getListItems -= new _GetShoppingListItems(sqlFunctions.GetListItems);
            return response;
        }

        public async Task<bool> DeleteFromShoppingList(int listID, int ID)
        {
            deleteFromList += new _DeleteFromList(sqlFunctions.DeleteFromShoppingList);
            bool response = await deleteFromList.Invoke(listID, ID);
            deleteFromList -= new _DeleteFromList(sqlFunctions.DeleteFromShoppingList);
            return response;
        }

        public async Task<UserDTO> GetUserDetails(int userID)
        {
            getUser += new _GetUserDetails(sqlFunctions.GetUserDetails);
            UserDTO response = await getUser.Invoke(userID);
            getUser -= new _GetUserDetails(sqlFunctions.GetUserDetails);
            return response;
        }

        public async Task<int> GetListsCount(int userID)
        {
            getCount += new _GetListsCount(sqlFunctions.GetListsCount);
            int response = await getCount.Invoke(userID);
            getCount -= new _GetListsCount(sqlFunctions.GetListsCount);
            return response;
        }
    }

    #endregion
}
