﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RnR.Data.DTOs;
using RnR.Data.Generic;

namespace RnR.Data.DAOs
{
    #region Delegates

    public delegate Task<List<FavoritesDTO>> _GetFavorites(int userID);
    public delegate Task<bool> _AddToFavorites(FavoritesDTO item);
    public delegate Task<bool> _RemoveFromFavorites(int ID, int favID);
    public delegate Task<bool> _AddToFavoriteCart(CartDTO item);

    #endregion

    #region Interfaces

    public interface IFavorites
    {
        public Task<List<FavoritesDTO>> getFavorites(int userID);
        public Task<bool> addToFavorites(FavoritesDTO item);
        public Task<bool> removeFromFavorites(int ID, int favID);
        public Task<bool> addToCart(CartDTO item);
    }

    #endregion

    public class FavoritesService : IFavorites
    {
        #region Constructor

        SQLFunctions sqlFunctions = null;

        private static event _GetFavorites get;
        private static event _AddToFavorites add;
        private static event _RemoveFromFavorites remove;
        private static event _AddToFavoriteCart addToFavCart;

        public FavoritesService(string _connectionString, string _query)
        {
            sqlFunctions = new SQLFunctions(_connectionString, _query);
        }

        #endregion

        public async Task<List<FavoritesDTO>> getFavorites(int userID)
        {
            get += new _GetFavorites(sqlFunctions.GetFavorites);
            List<FavoritesDTO> response = await get.Invoke(userID);
            get -= new _GetFavorites(sqlFunctions.GetFavorites);
            return response;
        }

        public async Task<bool> addToFavorites(FavoritesDTO item)
        {
            add += new _AddToFavorites(sqlFunctions.AddToFavorites);
            bool response = await add.Invoke(item);
            add -= new _AddToFavorites(sqlFunctions.AddToFavorites);
            return response;
        }

        public async Task<bool> removeFromFavorites(int ID, int favID)
        {
            remove += new _RemoveFromFavorites(sqlFunctions.RemoveFromFavorites);
            bool response = await remove.Invoke(ID, favID);
            remove -= new _RemoveFromFavorites(sqlFunctions.RemoveFromFavorites);
            return response;
        }

        public async Task<bool> addToCart(CartDTO item)
        {
            addToFavCart += new _AddToFavoriteCart(sqlFunctions.AddToCart);
            bool response = await addToFavCart.Invoke(item);
            addToFavCart -= new _AddToFavoriteCart(sqlFunctions.AddToCart);
            return response;
        }
    }
}
