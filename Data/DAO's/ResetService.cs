﻿using System.Threading.Tasks;
using RnR.Data.DTOs;
using RnR.Data.Generic;

namespace RnR.Data.DAOs
{
    #region Delegates

    public delegate Task<ResetResponseDTO> _Reset(UserDTO user);
    public delegate Task<CompleteResponseDTO> _CompleteReset(CompleteResetDTO user);

    #endregion

    #region Interfaces

    public interface IReset
    {
        public Task<ResetResponseDTO> resetPassword(UserDTO user);
        public Task<CompleteResponseDTO> complete(CompleteResetDTO user);
    }

    #endregion

    public class ResetService : IReset
    {
        #region Constructor

        SQLFunctions sqlFunctions = null;

        private static event _Reset reset;
        private static event _CompleteReset completeReset;

        public ResetService(string _connectionString, string _query)
        {
            sqlFunctions = new SQLFunctions(_connectionString, _query);
        }

        #endregion

        public async Task<ResetResponseDTO> resetPassword(UserDTO user)
        {
            reset += new _Reset(sqlFunctions.resetPassword);
            ResetResponseDTO response = await reset.Invoke(user);
            reset -= new _Reset(sqlFunctions.resetPassword);
            return response;
        }

        public async Task<CompleteResponseDTO> complete(CompleteResetDTO user)
        {
            completeReset += new _CompleteReset(sqlFunctions.complete);
            CompleteResponseDTO response = await completeReset.Invoke(user);
            completeReset -= new _CompleteReset(sqlFunctions.complete);
            return response;
        }
    }
}
