﻿using System.Threading.Tasks;
using RnR.Data.DTOs;
using RnR.Data.Generic;

namespace RnR.Data.DAOs
{
    #region Delegates

    public delegate Task<bool> _Complete(CompleteTransactionDTO input);

    #endregion

    #region Interfaces

    public interface ICompleteTransaction
    {
        public Task<bool> CompleteCollection(CompleteTransactionDTO input);
        public Task<bool> CompleteDelivery(CompleteTransactionDTO input);
    }

    #endregion

    public class CompleteTransactionService : ICompleteTransaction
    {
        #region Constructor

        SQLFunctions sqlFunctions = null;

        private static event _Complete complete;

        public CompleteTransactionService(string _connectionString, string _query)
        {
            sqlFunctions = new SQLFunctions(_connectionString, _query);
        }

        #endregion

        public async Task<bool> CompleteCollection(CompleteTransactionDTO input)
        {
            complete += new _Complete(sqlFunctions.CompleteCollection);
            bool response = await complete.Invoke(input);
            complete -= new _Complete(sqlFunctions.CompleteCollection);
            return response;
        }

        public async Task<bool> CompleteDelivery(CompleteTransactionDTO input)
        {
            complete += new _Complete(sqlFunctions.CompleteDelivery);
            bool response = await complete.Invoke(input);
            complete -= new _Complete(sqlFunctions.CompleteDelivery);
            return response;
        }
    }
}
