﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RnR.Data.DTOs;
using RnR.Data.Generic;

namespace RnR.Data.DAOs
{
    #region Delegates

    public delegate Task<string> _CreateAddress(AddressDTO address);
    public delegate Task<List<AddressDTO>> _GetAddresses(int userID);
    public delegate Task<bool> _DeleteAddress(int addressID, int userID);
    public delegate Task<bool> _UpdateAddress(AddressDTO address);

    #endregion

    #region Enumerations

    public enum AddressAdded
    {
        True,
        False,
        Exists
    }

    #endregion

    #region Interfaces

    public interface IAddress
    {
        public Task<string> createAddress(AddressDTO address);
        public Task<List<AddressDTO>> getAddresses(int userID);
        public Task<bool> deleteAddress(int addressID, int userID);
        public Task<bool> updateAddress(AddressDTO address);
    }

    #endregion

    public class AddressService : IAddress
    {
        #region Constructor

        SQLFunctions sqlFunctions = null;

        private static event _CreateAddress create;
        private static event _GetAddresses get;
        private static event _DeleteAddress delete;
        private static event _UpdateAddress update;

        public AddressService(string _connectionString, string _query)
        {
            sqlFunctions = new SQLFunctions(_connectionString, _query);
        }

        #endregion

        public async Task<string> createAddress(AddressDTO address)
        {
            create += new _CreateAddress(sqlFunctions.createAddress);
            string response = await create.Invoke(address);
            create -= new _CreateAddress(sqlFunctions.createAddress);
            return response;
        }

        public async Task<List<AddressDTO>> getAddresses(int userID)
        {
            get += new _GetAddresses(sqlFunctions.getAddresses);
            List<AddressDTO> response = await get.Invoke(userID);
            get -= new _GetAddresses(sqlFunctions.getAddresses);
            return response;
        }

        public async Task<bool> deleteAddress(int addressID, int userID)
        {
            delete += new _DeleteAddress(sqlFunctions.deleteAddress);
            bool response = await delete.Invoke(addressID, userID);
            delete -= new _DeleteAddress(sqlFunctions.deleteAddress);
            return response;
        }

        public async Task<bool> updateAddress(AddressDTO address)
        {
            update += new _UpdateAddress(sqlFunctions.updateAddress);
            bool response = await update.Invoke(address);
            update -= new _UpdateAddress(sqlFunctions.updateAddress);
            return response;
        }
    }
}
