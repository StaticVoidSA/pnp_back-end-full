﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RnR.Data.DTOs;
using RnR.Data.Generic;

namespace RnR.Data.DAOs
{
    #region Delegates

    public delegate Task<bool> _AddToCart(CartDTO item);
    public delegate Task<bool> _RemoveFromCart(int cartItemID, int cartID);
    public delegate Task<List<CartItemDTO>> _GetCartItems(int userID);
    public delegate Task<int> _CartCount(int userID);
    public delegate Task<bool> _UpdateCartItem(CartItemUpdateDTO item);
    public delegate Task<bool> _RemoveAllItems(int userID);

    #endregion

    #region Interfaces

    public interface ICart
    {
        public Task<bool> AddToCart(CartDTO item);
        public Task<bool> RemoveFromCart(int cartItemID, int cartID);
        public Task<List<CartItemDTO>> getCartItems(int userID);
        public Task<int> cartCount(int userID);
        public Task<bool> updateCartItem(CartItemUpdateDTO item);
        public Task<bool> removeAllItems(int userID);
    }

    #endregion

    public class CartService : ICart
    {
        #region Constructor

        SQLFunctions sqlFunctions = null;

        private static event _AddToCart add;
        private static event _RemoveFromCart removeFromCart;
        private static event _GetCartItems get;
        private static event _CartCount count;
        private static event _UpdateCartItem update;
        private static event _RemoveAllItems removeAll;

        public CartService(string _connectionString, string _query)
        {
            sqlFunctions = new SQLFunctions(_connectionString, _query);
        }

        #endregion

        public async Task<bool> AddToCart(CartDTO item)
        {
            add += new _AddToCart(sqlFunctions.AddToCart);
            bool response = await add.Invoke(item);
            add -= new _AddToCart(sqlFunctions.AddToCart);
            return response;
        }

        public async Task<bool> RemoveFromCart(int cartItemID, int cartID)
        {
            removeFromCart += new _RemoveFromCart(sqlFunctions.RemoveFromCart);
            bool response = await removeFromCart.Invoke(cartItemID, cartID);
            removeFromCart -= new _RemoveFromCart(sqlFunctions.RemoveFromCart);
            return response;
        }

        public async Task<List<CartItemDTO>> getCartItems(int userID)
        {
            get += new _GetCartItems(sqlFunctions.getCartItems);
            List<CartItemDTO> response = await get.Invoke(userID);
            get -= new _GetCartItems(sqlFunctions.getCartItems);
            return response;
        }

        public async Task<int> cartCount(int userID)
        {
            count += new _CartCount(sqlFunctions.cartCount);
            int response = await count.Invoke(userID);
            count -= new _CartCount(sqlFunctions.cartCount);
            return response;
        }

        public async Task<bool> updateCartItem(CartItemUpdateDTO item)
        {
            update += new _UpdateCartItem(sqlFunctions.updateCartItem);
            bool response = await update.Invoke(item);
            update -= new _UpdateCartItem(sqlFunctions.updateCartItem);
            return response;
        }

        public async Task<bool> removeAllItems(int userID)
        {
            removeAll += new _RemoveAllItems(sqlFunctions.removeAllItems);
            bool response = await removeAll.Invoke(userID);
            removeAll -= new _RemoveAllItems(sqlFunctions.removeAllItems);
            return response;
        }
    }
}
