﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RnR.Data.DTOs;
using RnR.Data.Generic;

namespace RnR.Data.DAOs
{
    #region Delegates

    public delegate Task<List<RecipeDTO>> _Startup();
    public delegate Task<List<RecipeCompleteDTO>> _GetRecipe(int recipeID);
    public delegate Task<List<RecipeCompleteDTO>> _GetAllRecipes();
    public delegate Task<int> _AddIngredientsToCart(List<CartDTO> recipes);
    public delegate Task<bool> _AddIngredientsToFavorites(FavoritesDTO recipe);

    #endregion

    #region Interfaces

    public interface IRecipes
    {
        public Task<List<RecipeDTO>> startupRecipes();
        public Task<List<RecipeCompleteDTO>> getRecipe(int recipeID);
        public Task<List<RecipeCompleteDTO>> getAllRecipes();
        public Task<int> addIngredientsToCart(List<CartDTO> recipes);
        public Task<bool> addIngredientsToFavorites(List<FavoritesDTO> recipe);
    }

    #endregion

    public class RecipesService : IRecipes
    {
        #region Constructor

        SQLFunctions sqlFunctions = null;

        private static event _Startup startup;
        private static event _GetRecipe get;
        private static event _GetAllRecipes getAll;
        private static event _AddIngredientsToCart addToCart;
        private static event _AddIngredientsToFavorites addToFavorites;

        public RecipesService(string _connectionString, string _query)
        {
            sqlFunctions = new SQLFunctions(_connectionString, _query);
        }

        #endregion

        public async Task<List<RecipeDTO>> startupRecipes()
        {
            startup += new _Startup(sqlFunctions.startupRecipes);
            List<RecipeDTO> response = await startup.Invoke();
            startup -= new _Startup(sqlFunctions.startupRecipes);
            return response;
        }

        public async Task<List<RecipeCompleteDTO>> getRecipe(int recipeID)
        {
            get += new _GetRecipe(sqlFunctions.getRecipe);
            List<RecipeCompleteDTO> response = await get.Invoke(recipeID);
            get -= new _GetRecipe(sqlFunctions.getRecipe);
            return response;
        }

        public async Task<List<RecipeCompleteDTO>> getAllRecipes()
        {
            getAll += new _GetAllRecipes(sqlFunctions.getAllRecipes);
            List<RecipeCompleteDTO> response = await getAll.Invoke();
            getAll -= new _GetAllRecipes(sqlFunctions.getAllRecipes);
            return response;
        }

        public async Task<int> addIngredientsToCart(List<CartDTO> items)
        {
            addToCart += new _AddIngredientsToCart(sqlFunctions.addIngredientsToCart);
            int response = await addToCart.Invoke(items);
            addToCart -= new _AddIngredientsToCart(sqlFunctions.addIngredientsToCart);
            return response;
        }

        public async Task<bool> addIngredientsToFavorites(List<FavoritesDTO> recipes)
        {
            bool response = false;

            for (var i = 0; i < recipes.Count; i++)
            {
                addToFavorites += new _AddIngredientsToFavorites(sqlFunctions.AddToFavorites);
                response = await addToFavorites.Invoke(recipes[i]);
                addToFavorites -= new _AddIngredientsToFavorites(sqlFunctions.AddToFavorites);
            }

            return response;
        }
    }
}
