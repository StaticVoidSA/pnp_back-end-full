﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RnR.Data.DTOs;
using RnR.Data.Generic;

namespace RnR.Data.DAOs
{
    #region Delegates

    public delegate Task<ProductDTO> _GetProduct(ProductDTO product);
    public delegate Task<List<SearchResponseDTO>> _GetProducts();
    public delegate Task<bool> _DeleteProduct(ProductDTO product);
    public delegate Task<bool> _EditProduct(ProductUpdateDTO product);

    #endregion

    #region Interfaces

    public interface IProducts
    {
        public Task<ProductDTO> getProduct(ProductDTO product);
        public Task<List<SearchResponseDTO>> getProducts();
        public Task<bool> deleteProduct(ProductDTO product);
        public Task<bool> editProduct(ProductUpdateDTO product);
    }

    #endregion

    public class ProductsService : IProducts
    {
        #region Constructor

        SQLFunctions sqlFunctions = null;

        private static event _GetProduct get;
        private static event _GetProducts getAll;
        private static event _DeleteProduct delete;
        private static event _EditProduct edit;

        public ProductsService(string _connectionString, string _query)
        {
            sqlFunctions = new SQLFunctions(_connectionString, _query);
        }

        #endregion

        public async Task<ProductDTO> getProduct(ProductDTO product)
        {
            get += new _GetProduct(sqlFunctions.getProduct);
            ProductDTO response = await get.Invoke(product);
            get -= new _GetProduct(sqlFunctions.getProduct);
            return response;
        }

        public async Task<List<SearchResponseDTO>> getProducts()
        {
            getAll += new _GetProducts(sqlFunctions.getProducts);
            List<SearchResponseDTO> response = await getAll.Invoke();
            getAll -= new _GetProducts(sqlFunctions.getProducts);
            return response;
        }

        public async Task<bool> deleteProduct(ProductDTO product)
        {
            delete += new _DeleteProduct(sqlFunctions.deleteProduct);
            bool response = await delete.Invoke(product);
            delete -= new _DeleteProduct(sqlFunctions.deleteProduct);
            return response;
        }

        public async Task<bool> editProduct(ProductUpdateDTO product)
        {
            edit += new _EditProduct(sqlFunctions.editProduct);
            bool response = await edit.Invoke(product);
            edit -= new _EditProduct(sqlFunctions.editProduct);
            return response;
        }
    }
}
