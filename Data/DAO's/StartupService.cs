﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RnR.Data.DTOs;
using RnR.Data.Generic;

namespace RnR.Data.DAOs
{
    #region Delegates

    public delegate Task<List<StartupResponseDTO>> _ServerStartup();

    #endregion

    public class StartupService
    {
        #region Constructor

        SQLFunctions sqlFunctions = null;

        private static event _ServerStartup start;

        public StartupService(string _connectionString, string _query)
        {
            sqlFunctions = new SQLFunctions(_connectionString, _query);
        }

        #endregion

        #region Startup

        public async Task<List<StartupResponseDTO>> startup()
        {
            start += new _ServerStartup(sqlFunctions.ServerStartup);
            List<StartupResponseDTO> response = await start.Invoke();
            start -= new _ServerStartup(sqlFunctions.ServerStartup);
            return response;
        }

        #endregion
    }
}
