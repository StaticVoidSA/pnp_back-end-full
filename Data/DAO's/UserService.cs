﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RnR.Data.DTOs;
using RnR.Data.Generic;

namespace RnR.Data.DAOs
{
    #region Delegates

    public delegate Task<List<UserDTO>> GetAllUsers();
    public delegate Task<bool> DeleteUser(UserDTO user);
    public delegate Task<bool> UpdateUser(UserUpdateDTO user);
    public delegate Task<UserDTO> GetUser(UserDTO user);
    public delegate Task<UserDTO> GetUserByID(int userID);
    public delegate Task<string> GetUserFullName(string email);

    #endregion

    public interface IUserService
    {
        public Task<UserDTO> getUser(UserDTO user);
        public Task<string> GetUserFullname(string email);
        public Task<UserDTO> getUserByID(int ID);
        public Task<List<UserDTO>> getUsers();
        public Task<bool> deleteUser(UserDTO user);
        public Task<bool> updateUser(UserUpdateDTO user);
    }

    public class UserService : IUserService
    {
        #region Constructor

        SQLFunctions sqlFunctions = null;

        private static event GetAllUsers getAll;
        private static event DeleteUser delete;
        private static event UpdateUser update;
        private static event GetUser get;
        private static event GetUserByID getByID;
        private static event GetUserFullName getFullName;

        public UserService(string connectionString, string query)
        {
            sqlFunctions = new SQLFunctions(connectionString, query);
        }

        #endregion

        #region GetUser

        public async Task<UserDTO> getUser(UserDTO user)
        {
            get += new GetUser(sqlFunctions.GetUser);
            UserDTO response = await get.Invoke(user);
            get -= new GetUser(sqlFunctions.GetUser);
            return response;
        }

        #region GetUserFullName

        public async Task<string> GetUserFullname(string email)
        {
            getFullName += new GetUserFullName(sqlFunctions.getUserFullName);
            string response = await getFullName.Invoke(email);
            getFullName -= new GetUserFullName(sqlFunctions.getUserFullName);
            return response;
        }

        #endregion

        #endregion

        #region GetUserByID

        public async Task<UserDTO> getUserByID(int ID)
        {
            getByID += new GetUserByID(sqlFunctions.GetUserByID);
            UserDTO response = await getByID.Invoke(ID);
            return response;
        }

        #endregion

        #region GetUsers

        public async Task<List<UserDTO>> getUsers()
        {
            getAll += new GetAllUsers(sqlFunctions.GetAllUsers);
            List<UserDTO> response = await getAll.Invoke();
            return response;
        }

        #endregion

        #region DeleteUser

        public async Task<bool> deleteUser(UserDTO user)
        {
            delete += new DeleteUser(sqlFunctions.deleteUser);
            bool response = await delete.Invoke(user);
            return response;
        }

        #endregion

        #region UpdateUser

        public async Task<bool> updateUser(UserUpdateDTO user)
        {
            update += new UpdateUser(sqlFunctions.updateUser);
            bool response = await update.Invoke(user);
            return response;
        }

        #endregion
    }
}
