﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RnR.Data.DTOs;
using RnR.Data.Generic;

namespace RnR.Data.DAOs
{
    #region Delegates

    public delegate Task<List<CatalogueDTO>> _Get(string category);

    #endregion

    #region Interfaces

    public interface ICatalogue
    {
        public Task<List<CatalogueDTO>> getCatalogues(string category);
    }

    #endregion

    public class CatalogueService : ICatalogue
    {
        #region Constructor

        SQLFunctions sqlFunctions = null;

        private static event _Get get;

        public CatalogueService(string _connectionString, string _query)
        {
            sqlFunctions = new SQLFunctions(_connectionString, _query);
        }

        #endregion

        public async Task<List<CatalogueDTO>> getCatalogues(string category)
        {
            get += new _Get(sqlFunctions.getCatalogues);
            List<CatalogueDTO> response = await get.Invoke(category);
            get -= new _Get(sqlFunctions.getCatalogues);
            return response;
        }
    }
}
