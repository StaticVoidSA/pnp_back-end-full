﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RnR.Data.DTOs;
using RnR.Data.Generic;

namespace RnR.Data.DAOs
{
    #region Delegates

    public delegate Task<List<CollectionItemDTO>> _GetCollection(int userID);

    #endregion

    #region Interfaces

    public interface ICollectionService
    {
        public Task<List<CollectionItemDTO>> getCollections(int userID);
    }

    #endregion

    public class CollectionService : ICollectionService
    {
        #region Constructor

        SQLFunctions sqlFunctions = null;

        private static event _GetCollection get;

        public CollectionService(string _connectionString, string _query)
        {
            sqlFunctions = new SQLFunctions(_connectionString, _query);
        }

        #endregion

        public async Task<List<CollectionItemDTO>> getCollections(int userID)
        {
            get += new _GetCollection(sqlFunctions.getCollections);
            List<CollectionItemDTO> response = await get.Invoke(userID);
            get -= new _GetCollection(sqlFunctions.getCollections);
            return response;
        }
    }
}
