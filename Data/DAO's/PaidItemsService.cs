﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RnR.Data.DTOs;
using RnR.Data.Generic;

namespace RnR.Data.DAOs
{
    #region Delegates

    public delegate Task<bool> _AddItem(CartDTO item);
    public delegate Task<List<CartDTO>> _GetItems(int userID);
    public delegate Task<bool> _ClearItems(int userID);

    #endregion

    #region Interfaces

    public interface IPaidFor
    {
        public Task<bool> AddToPaidFor(CartDTO item);
        public Task<List<CartDTO>> GetPaidFor(int userID);
        public Task<bool> ClearPaidItems(int userID);
    }

    #endregion

    public class PaidItemsService : IPaidFor
    {
        #region Constructor

        SQLFunctions sqlFunctions = null;

        private static event _AddItem add;
        private static event _GetItems get;
        private static event _ClearItems clear;

        public PaidItemsService(string _connectionString, string _query)
        {
            sqlFunctions = new SQLFunctions(_connectionString, _query);
        }

        #endregion

        public async Task<bool> AddToPaidFor(CartDTO item)
        {
            add += new _AddItem(sqlFunctions.AddToPaidFor);
            bool response = await add.Invoke(item);
            add -= new _AddItem(sqlFunctions.AddToPaidFor);
            return response;
        }

        public async Task<List<CartDTO>> GetPaidFor(int userID)
        {
            get += new _GetItems(sqlFunctions.GetPaidFor);
            List<CartDTO> response = await get.Invoke(userID);
            get -= new _GetItems(sqlFunctions.GetPaidFor);
            return response;
        }

        public async Task<bool> ClearPaidItems(int userID)
        {
            clear += new _ClearItems(sqlFunctions.ClearPaidItems);
            bool response = await clear.Invoke(userID);
            clear -= new _ClearItems(sqlFunctions.ClearPaidItems);
            return response;
        }
    }
}
