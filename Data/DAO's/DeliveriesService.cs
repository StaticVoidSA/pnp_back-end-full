﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RnR.Data.DTOs;
using RnR.Data.Generic;

namespace RnR.Data.DAOs
{
    #region Delegates

    public delegate Task<List<DeliveryItemDTO>> _GetDeliveries(int userID);

    #endregion

    #region Interfaces

    public interface IDeliveriesService
    {
        public Task<List<DeliveryItemDTO>> getDeliveries(int userID);
    }

    #endregion

    public class DeliveriesService : IDeliveriesService
    {
        #region Constructor

        SQLFunctions sqlFunctions = null;

        private static event _GetDeliveries get;

        public DeliveriesService(string _connectionString, string _query)
        {
            sqlFunctions = new SQLFunctions(_connectionString, _query);
        }

        #endregion

        public async Task<List<DeliveryItemDTO>> getDeliveries(int userID)
        {
            get += new _GetDeliveries(sqlFunctions.getDeliveries);
            List<DeliveryItemDTO> response = await get.Invoke(userID);
            get -= new _GetDeliveries(sqlFunctions.getDeliveries);
            return response;
        }
    }
}
