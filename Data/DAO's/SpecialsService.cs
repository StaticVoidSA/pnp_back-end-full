﻿using RnR.Data.DTOs;
using System.Collections.Generic;
using RnR.Data.Generic;
using System.Threading.Tasks;

namespace RnR.Data.DAOs
{
    #region Delegates

    public delegate Task<List<SpecialsDTO>> _GetSpecials(string category);

    #endregion

    #region Interfaces

    public interface ISpecials
    {
        public Task<List<SpecialsDTO>> getSpecials(string category);
    }

    #endregion

    public class SpecialsService : ISpecials
    {
        #region Constructor

        private static event _GetSpecials get;

        SQLFunctions sqlFunctions = null;

        public SpecialsService(string _connectionString, string _query)
        {
            sqlFunctions = new SQLFunctions(_connectionString, _query);
        }

        #endregion


        #region GetSpecials

        public async Task<List<SpecialsDTO>> getSpecials(string category) 
        {
            get += new _GetSpecials(sqlFunctions.GetSpecials);
            List<SpecialsDTO> response = await get.Invoke(category);
            get -= new _GetSpecials(sqlFunctions.GetSpecials);
            return response;
        }

        #endregion
    }
}
