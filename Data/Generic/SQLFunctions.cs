﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using RnR.Data.DAOs;
using RnR.Data.DTOs;
using RnR.Data.Logger;
using RnR.Data.Models;
using RnR.Services.HashingService;
using RnR.Services.MailService;

namespace RnR.Data.Generic
{
    public class SQLFunctions
    {
        #region Constructor

        private readonly string connStr = string.Empty;
        private readonly string query = string.Empty;

        public SQLFunctions(string connectionString, string inputQuery)
        {
            connStr = connectionString;
            query = inputQuery;
        }

        #endregion

        #region Helpers

        public string getDiscount(double price)
        {
            return (Math.Round((price * 0.25), 2)).ToString("N2");
        }

        #endregion

        #region ServerStartup

        public async Task<List<StartupResponseDTO>> ServerStartup()
        {
            List<StartupResponseDTO> response = new List<StartupResponseDTO>();
            DataTable dt = new DataTable();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    await dataAdapter.FillAsync(dt);
                }

                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        StartupResponseDTO resp = new StartupResponseDTO();
                        resp.firstName = dt.Rows[i]["firstName"].ToString();
                        resp.surname = dt.Rows[i]["lastName"].ToString();
                        resp.email = dt.Rows[i]["email"].ToString();
                        resp.password = dt.Rows[i]["encPassword"].ToString();
                        resp.doj = Convert.ToDateTime(dt.Rows[i]["doj"]);
                        resp.userRole = dt.Rows[i]["userRole"].ToString();

                        response.Add(resp);
                    }

                    return response.ToList();

                }
                else
                {
                    StartupResponseDTO resp = new StartupResponseDTO();
                    response.Add(resp);
                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region GetAllUsers

        public async Task<List<UserDTO>> GetAllUsers()
        {
            List<UserDTO> response = new List<UserDTO>();
            DataTable dt = new DataTable();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    await dataAdapter.FillAsync(dt);
                }

                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        UserDTO resp = new UserDTO();
                        resp.firstName = dt.Rows[i]["firstName"].ToString();
                        resp.surname = dt.Rows[i]["lastName"].ToString();
                        resp.email = dt.Rows[i]["email"].ToString();
                        resp.password = dt.Rows[i]["encPassword"].ToString();
                        resp.doj = Convert.ToDateTime(dt.Rows[i]["doj"]);
                        resp.userRole = dt.Rows[i]["userRole"].ToString();

                        response.Add(resp);
                    }

                    return response.ToList();

                }
                else
                {
                    UserDTO resp = new UserDTO();
                    response.Add(resp);
                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region GetUserByEmail

        public async Task<UserDTO> GetUser(UserDTO user)
        {
            DataTable dt = new DataTable();
            UserDTO resp = new UserDTO();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@email", user.email);
                    await dataAdapter.FillAsync(dt);
                }

                bool hasUser = dt.Rows.GetEnumerator().MoveNext();

                if (hasUser)
                {
                    resp.firstName = dt.Rows[0]["firstName"].ToString();
                    resp.surname = dt.Rows[0]["lastName"].ToString();
                    resp.email = dt.Rows[0]["email"].ToString();
                    resp.password = dt.Rows[0]["encPassword"].ToString();
                    resp.doj = Convert.ToDateTime(dt.Rows[0]["doj"]);
                    resp.userRole = dt.Rows[0]["userRole"].ToString();
                    resp.userID = (int)dt.Rows[0]["userID"];

                    return resp;
                }
                else
                {
                    resp.firstName = null;
                    resp.surname = null;
                    resp.email = null;
                    resp.password = null;
                    resp.doj = DateTime.Now;
                    resp.userRole = null;

                    return resp;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region GetUserByID

        public async Task<UserDTO> GetUserByID(int userID)
        {
            DataTable dt = new DataTable();
            UserDTO resp = new UserDTO();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@userID", userID);
                    await dataAdapter.FillAsync(dt);
                }

                bool hasUser = dt.Rows.GetEnumerator().MoveNext();

                if (hasUser)
                {
                    resp.firstName = dt.Rows[0]["firstName"].ToString();
                    resp.surname = dt.Rows[0]["lastName"].ToString();
                    resp.email = dt.Rows[0]["email"].ToString();
                    resp.password = dt.Rows[0]["encPassword"].ToString();
                    resp.doj = Convert.ToDateTime(dt.Rows[0]["doj"]);
                    resp.userRole = dt.Rows[0]["userRole"].ToString();
                    resp.userID = (int)dt.Rows[0]["userID"];

                    return resp;
                }
                else
                {
                    resp.firstName = null;
                    resp.surname = null;
                    resp.email = null;
                    resp.password = null;
                    resp.doj = DateTime.Now;
                    resp.userRole = null;
                    resp.token = "Bearer null";

                    return resp;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }

        }

        #endregion

        #region GetUserFullName

        public async Task<string> getUserFullName(string email)
        {
            try
            {
                DataTable dt = new DataTable();

                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@email", email);
                    await dataAdapter.FillAsync(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    string firstName = dt.Rows[0]["firstName"].ToString();
                    string lastName = dt.Rows[0]["lastName"].ToString();
                    string fullName = firstName + " " + lastName;
                    return fullName;
                }
                else
                {
                    string fullName = null;
                    return fullName;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region DeleteUser

        public async Task<bool> deleteUser(UserDTO user)
        {
            int result;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@userID", user.userID);
                    comm.Parameters.AddWithValue("@email", user.email);

                    result = await comm.ExecuteNonQueryAsync();
                }

                if (result == -1)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region UpdateUser

        public async Task<bool> updateUser(UserUpdateDTO user)
        {
            int result;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@firstName", user.firstName);
                    comm.Parameters.AddWithValue("@lastName", user.surname);
                    comm.Parameters.AddWithValue("@email", user.email);
                    comm.Parameters.AddWithValue("@userRole", user.userRole);
                    comm.Parameters.AddWithValue("@_email", user._email);

                    result = await comm.ExecuteNonQueryAsync();
                }

                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }

        }

        #endregion

        #region GetSpecials

        public async Task<List<SpecialsDTO>> GetSpecials(string category)
        {
            DataTable dt = new DataTable();
            List<SpecialsDTO> response = new List<SpecialsDTO>();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@category", category);
                    await dataAdapter.FillAsync(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    for (int i = 0; i < 5; i++)
                    {
                        SpecialsDTO resp = new SpecialsDTO();
                        resp.barcode = dt.Rows[i]["barcode"].ToString();
                        resp.brand = dt.Rows[i]["brand"].ToString();
                        resp.category = dt.Rows[i]["category"].ToString();
                        resp.description = dt.Rows[i]["descr"].ToString();
                        resp.price = Convert.ToDouble(dt.Rows[i]["price"]);
                        resp.discount = getDiscount(Convert.ToDouble(dt.Rows[i]["price"]));
                        resp.features = dt.Rows[i]["features"].ToString();
                        resp.productID = (int)dt.Rows[i]["productID"];
                        resp.oldPrice = Convert.ToDouble(resp.price + Convert.ToDouble(resp.discount));
                        resp.quantity = dt.Rows[i]["quantity"].ToString();
                        resp.title = dt.Rows[i]["title"].ToString();
                        resp.uri = dt.Rows[i]["uri"].ToString();
                        resp.usage = dt.Rows[i]["_usage"].ToString();

                        response.Add(resp);
                    }

                    return response;
                }
                else
                {
                    response = null;
                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Shop OnStartup

        public async Task<List<SearchResponseDTO>> OnStartupShop()
        {
            List<SearchResponseDTO> responseList = new List<SearchResponseDTO>();
            DataTable dt = new DataTable();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    await dataAdapter.FillAsync(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    for (int i = 0; i <= 16; i++)
                    {
                        SearchResponseDTO resp = new SearchResponseDTO();
                        resp.productID = Convert.ToInt32(dt.Rows[i]["productID"]);
                        resp.title = dt.Rows[i]["title"].ToString();
                        resp.category = dt.Rows[i]["category"].ToString();
                        resp.brand = dt.Rows[i]["brand"].ToString();
                        resp.uri = dt.Rows[i]["uri"].ToString();
                        resp.price = Convert.ToDouble(dt.Rows[i]["price"]);
                        resp.description = dt.Rows[i]["descr"].ToString();
                        resp.features = dt.Rows[i]["features"].ToString();
                        resp.usage = dt.Rows[i]["_usage"].ToString();
                        resp.quantity = dt.Rows[i]["quantity"].ToString();
                        resp.barcode = dt.Rows[i]["barcode"].ToString();

                        responseList.Add(resp);
                    }

                    return responseList.ToList();
                }
                else
                {
                    responseList = null;
                    return responseList;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Shop FilterByBrand

        public async Task<List<SearchResponseDTO>> ShopFilterByBrand(SearchRequestDTO request)
        {
            DataTable dt = new DataTable();
            List<SearchResponseDTO> responseList = new List<SearchResponseDTO>();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    await conn.OpenAsync();
                    comm.Parameters.AddWithValue("@brand", request.brand);
                    comm.Parameters.AddWithValue("@category", request.category);
                    comm.CommandType = CommandType.Text;
                    await dataAdapter.FillAsync(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SearchResponseDTO resp = new SearchResponseDTO();
                        resp.productID = Convert.ToInt32(dt.Rows[i]["productID"]);
                        resp.title = dt.Rows[i]["title"].ToString();
                        resp.category = dt.Rows[i]["category"].ToString();
                        resp.brand = dt.Rows[i]["brand"].ToString();
                        resp.uri = dt.Rows[i]["uri"].ToString();
                        resp.price = Convert.ToDouble(dt.Rows[i]["price"]);
                        resp.description = dt.Rows[i]["descr"].ToString();
                        resp.features = dt.Rows[i]["features"].ToString();
                        resp.usage = dt.Rows[i]["_usage"].ToString();
                        resp.quantity = dt.Rows[i]["quantity"].ToString();
                        resp.barcode = dt.Rows[i]["barcode"].ToString();

                        responseList.Add(resp);
                    }

                    return responseList;
                }
                else
                {
                    responseList = null;
                    return responseList;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Shop FilterByPrice

        public async Task<List<SearchResponseDTO>> ShopFilterByPrice(SearchRequestDTO request)
        {
            DataTable dt = new DataTable();
            List<SearchResponseDTO> responseList = new List<SearchResponseDTO>();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    await conn.OpenAsync();
                    comm.Parameters.AddWithValue("@minRange", Convert.ToDouble(request.minRange));
                    comm.Parameters.AddWithValue("@maxRange", Convert.ToDouble(request.maxRange));
                    comm.Parameters.AddWithValue("@category", request.category);
                    comm.CommandType = CommandType.Text;
                    await dataAdapter.FillAsync(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SearchResponseDTO resp = new SearchResponseDTO();
                        resp.productID = Convert.ToInt32(dt.Rows[i]["productID"]);
                        resp.title = dt.Rows[i]["title"].ToString();
                        resp.category = dt.Rows[i]["category"].ToString();
                        resp.brand = dt.Rows[i]["brand"].ToString();
                        resp.uri = dt.Rows[i]["uri"].ToString();
                        resp.price = Convert.ToDouble(dt.Rows[i]["price"]);
                        resp.description = dt.Rows[i]["descr"].ToString();
                        resp.features = dt.Rows[i]["features"].ToString();
                        resp.usage = dt.Rows[i]["_usage"].ToString();
                        resp.quantity = dt.Rows[i]["quantity"].ToString();
                        resp.barcode = dt.Rows[i]["barcode"].ToString();

                        responseList.Add(resp);
                    }

                    return responseList;
                }
                else
                {
                    responseList = null;
                    return responseList;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Shop FilterByQuantity

        public async Task<List<SearchResponseDTO>> ShopFilterByQuantity(SearchRequestDTO request)
        {
            DataTable dt = new DataTable();
            List<SearchResponseDTO> response = new List<SearchResponseDTO>();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    await conn.OpenAsync();
                    comm.Parameters.AddWithValue("@quantity", request.quantity);
                    comm.Parameters.AddWithValue("@category", request.category);
                    comm.CommandType = CommandType.Text;
                    await dataAdapter.FillAsync(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SearchResponseDTO resp = new SearchResponseDTO();
                        resp.productID = Convert.ToInt32(dt.Rows[i]["productID"]);
                        resp.title = dt.Rows[i]["title"].ToString();
                        resp.category = dt.Rows[i]["category"].ToString();
                        resp.brand = dt.Rows[i]["brand"].ToString();
                        resp.uri = dt.Rows[i]["uri"].ToString();
                        resp.price = Convert.ToDouble(dt.Rows[i]["price"]);
                        resp.description = dt.Rows[i]["descr"].ToString();
                        resp.features = dt.Rows[i]["features"].ToString();
                        resp.usage = dt.Rows[i]["_usage"].ToString();
                        resp.quantity = dt.Rows[i]["quantity"].ToString();
                        resp.barcode = dt.Rows[i]["barcode"].ToString();

                        response.Add(resp);
                    }

                    return response;
                }
                else
                {
                    response = null;
                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region ShoppingList CreateShoppingList

        public async Task<bool> CreateShoppingList(ShoppingListDTO list)
        {
            int result = 0;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@listName", list.shoppingListName);
                    comm.Parameters.AddWithValue("@userID", list.userID);

                    result = await comm.ExecuteNonQueryAsync();
                }

                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region ShoppingList GetUserLists

        public async Task<List<ListsDTO>> GetUserLists(int userID)
        {
            List<ListsDTO> response = new List<ListsDTO>();
            DataTable dt = new DataTable();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@userID", userID);

                    await dataAdapter.FillAsync(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        ListsDTO resp = new ListsDTO();
                        resp.userID = (int)dt.Rows[i]["userListID"];
                        resp.itemID = (int)dt.Rows[i]["ID"];
                        resp.listName = dt.Rows[i]["listName"].ToString();

                        response.Add(resp);
                    }

                    return response;
                }
                else
                {
                    response = null;
                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }

        }

        #endregion

        #region ShoppingList DeleteShoppingList

        public async Task<bool> DeleteShoppingList(int itemID, int userID)
        {
            int result = 0;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@itemID", itemID);
                    comm.Parameters.AddWithValue("@userID", userID);

                    result = await comm.ExecuteNonQueryAsync();
                }

                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region ShoppingList AddToShoppingList

        public async Task<bool> AddToShoppingList(ShoppingListDTO list)
        {
            int result = 0;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;

                    comm.Parameters.AddWithValue("@title", list.title);
                    comm.Parameters.AddWithValue("@brand", list.brand);
                    comm.Parameters.AddWithValue("@barcode", list.barcode);
                    comm.Parameters.AddWithValue("@quantity", list.quantity);
                    comm.Parameters.AddWithValue("@price", list.price);
                    comm.Parameters.AddWithValue("@listName", list.shoppingListName);
                    comm.Parameters.AddWithValue("@listID", list.shoppingListID);

                    result = await comm.ExecuteNonQueryAsync();
                }

                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region ShoppingList GetListItems

        public async Task<List<ShoppingListDTO>> GetListItems(int userID, string listName)
        {
            List<ShoppingListDTO> response = new List<ShoppingListDTO>();
            DataTable dt = new DataTable();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@listID", userID);
                    comm.Parameters.AddWithValue("@listName", listName);

                    await dataAdapter.FillAsync(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        ShoppingListDTO resp = new ShoppingListDTO();
                        resp.barcode = dt.Rows[i]["barcode"].ToString();
                        resp.brand = dt.Rows[i]["brand"].ToString();
                        resp.itemID = (int)dt.Rows[i]["ID"];
                        resp.price = Convert.ToDecimal(dt.Rows[i]["price"]);
                        resp.quantity = (int)dt.Rows[i]["quantity"];
                        resp.title = dt.Rows[i]["productTitle"].ToString();
                        resp.shoppingListID = (int)dt.Rows[i]["listID"];
                        resp.shoppingListName = dt.Rows[i]["listName"].ToString();

                        response.Add(resp);
                    }

                    return response;
                }
                else
                {
                    response = null;
                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region ShoppingList DeleteFromShoppingList

        public async Task<bool> DeleteFromShoppingList(int listID, int ID)
        {
            int result = 0;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@listID", listID);
                    comm.Parameters.AddWithValue("@ID", ID);

                    result = await comm.ExecuteNonQueryAsync();
                }

                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region ShoppingList GetUserDetails

        public async Task<UserDTO> GetUserDetails(int userID)
        {
            UserDTO response = new UserDTO();
            DataTable dt = new DataTable();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@userID", userID);
                    await dataAdapter.FillAsync(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        response.firstName = dt.Rows[i]["firstName"].ToString();
                        response.surname = dt.Rows[i]["lastName"].ToString();
                        response.email = dt.Rows[i]["email"].ToString();
                        response.userRole = dt.Rows[i]["userRole"].ToString();
                        response.userID = (int)dt.Rows[i]["userID"];
                        response.doj = Convert.ToDateTime(dt.Rows[i]["doj"]);
                    }

                    return response;
                }
                else
                {
                    response = null;
                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region ShoppingList GetListsCount

        public async Task<int> GetListsCount(int userID)
        {
            int result = 0;
            DataTable dt = new DataTable();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    await conn.OpenAsync();
                    comm.Parameters.AddWithValue("@userID", userID);
                    await dataAdapter.FillAsync(dt);
                }

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    result++;
                }

                return result;
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Catalogue

        public async Task<IEnumerable<SearchResponseDTO>> catalogue()
        {
            DataTable dt = new DataTable();
            List<SearchResponseDTO> responseList = new List<SearchResponseDTO>();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    await dataAdapter.FillAsync(dt);
                }

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    SearchResponseDTO resp = new SearchResponseDTO();
                    resp.productID = Convert.ToInt32(dt.Rows[i]["productID"]);
                    resp.title = dt.Rows[i]["title"].ToString();
                    resp.category = dt.Rows[i]["category"].ToString();
                    resp.brand = dt.Rows[i]["brand"].ToString();
                    resp.uri = dt.Rows[i]["uri"].ToString();
                    resp.price = Convert.ToDouble(dt.Rows[i]["price"]);
                    resp.description = dt.Rows[i]["descr"].ToString();
                    resp.features = dt.Rows[i]["features"].ToString();
                    resp.usage = dt.Rows[i]["_usage"].ToString();
                    resp.quantity = dt.Rows[i]["quantity"].ToString();
                    resp.barcode = dt.Rows[i]["barcode"].ToString();

                    responseList.Add(resp);
                }

                return responseList.ToList();
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region SearchAll

        public async Task<List<SearchResponseDTO>> searchAll(SearchRequestDTO request)
        {
            try
            {
                DataTable dt = new DataTable();
                List<SearchResponseDTO> response = new List<SearchResponseDTO>();

                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@item", request.item);
                    await dataAdapter.FillAsync(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SearchResponseDTO resp = new SearchResponseDTO();
                        resp.productID = (int)dt.Rows[i]["productID"];
                        resp.title = dt.Rows[i]["title"].ToString();
                        resp.brand = dt.Rows[i]["brand"].ToString();
                        resp.category = dt.Rows[i]["category"].ToString();
                        resp.uri = dt.Rows[i]["uri"].ToString();
                        resp.price = Convert.ToDouble(dt.Rows[i]["price"]);
                        resp.description = dt.Rows[i]["descr"].ToString();
                        resp.features = dt.Rows[i]["features"].ToString();
                        resp.usage = dt.Rows[i]["_usage"].ToString();
                        resp.quantity = dt.Rows[i]["quantity"].ToString();
                        resp.barcode = dt.Rows[i]["barcode"].ToString();

                        response.Add(resp);
                    }

                    return response;
                }
                else
                {
                    SearchResponseDTO resp = new SearchResponseDTO();
                    response.Add(resp);
                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Reset Password

        public async Task<ResetResponseDTO> resetPassword(UserDTO user)
        {
            SendEmailService sendEmail = new SendEmailService();

            try
            {
                ResetResponseDTO response = new ResetResponseDTO();
                DataTable dt = new DataTable();
                response.email = user.email;

                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@email", user.email);
                    await dataAdapter.FillAsync(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    await sendEmail.sendConfirmationEmail(response.email);
                    response.emailSent = true;
                    return response;
                }
                else
                {
                    response.emailSent = false;
                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Complete Password Reset

        public async Task<CompleteResponseDTO> complete(CompleteResetDTO user)
        {
            try
            {
                CompleteResponseDTO response = new CompleteResponseDTO();
                var encPassword = UserHasherService.encrypt(user.password);
                string reset_hash = user.hash;

                SendEmailService detailsService = new SendEmailService();
                string userFullName = await getUserFullName(user.email);
                string userResetHash = UserHasherService.encrypt(user.email + "&" + userFullName);

                if (reset_hash == userResetHash)
                {

                    using (MySqlConnection conn = new MySqlConnection(connStr))
                    using (MySqlCommand comm = new MySqlCommand(query, conn))
                    {
                        await conn.OpenAsync();
                        comm.CommandType = CommandType.Text;
                        comm.Parameters.AddWithValue("@encPassword", encPassword);
                        comm.Parameters.AddWithValue("@email", user.email);

                        int result = await comm.ExecuteNonQueryAsync();

                        if (result != 0)
                        {
                            response.email = user.email;
                            response.passwordChanged = true;
                        }
                        else
                        {
                            response.email = user.email;
                            response.passwordChanged = false;
                        }

                        return response;
                    }
                }
                else
                {
                    response.passwordChanged = false;
                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Recipes Startup

        public async Task<List<RecipeDTO>> startupRecipes()
        {
            List<RecipeDTO> response = new List<RecipeDTO>();
            DataTable dt = new DataTable();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    await dataAdapter.FillAsync(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        RecipeDTO resp = new RecipeDTO();
                        resp.recipeID = (int)dt.Rows[i]["recipeID"];
                        resp.title = dt.Rows[i]["recipeTitle"].ToString();
                        resp.recipeImage = dt.Rows[i]["recipeImage"].ToString();

                        response.Add(resp);
                    }

                    return response;
                }
                else
                {
                    response = null;
                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Recipes Get Recipe

        public async Task<List<RecipeCompleteDTO>> getRecipe(int recipeID)
        {
            DataTable dt = new DataTable();
            List<RecipeCompleteDTO> response = new List<RecipeCompleteDTO>();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@recipeID", recipeID);

                    await dataAdapter.FillAsync(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        RecipeCompleteDTO resp = new RecipeCompleteDTO();
                        resp.recipeImage = dt.Rows[i]["recipeImage"].ToString();
                        resp.recipeTitle = dt.Rows[i]["recipeTitle"].ToString();
                        resp.recID = (int)dt.Rows[i]["recID"];
                        resp.brand = dt.Rows[i]["brand"].ToString();
                        resp.category = dt.Rows[i]["category"].ToString();
                        resp.quantity = dt.Rows[i]["quantity"].ToString();
                        resp.ingredientTitle = dt.Rows[i]["ingredientTitle"].ToString();
                        resp.price = Convert.ToDecimal(dt.Rows[i]["price"]);
                        resp.barcode = dt.Rows[i]["barcode"].ToString();
                        resp.ingredientImage = dt.Rows[i]["ingredientImage"].ToString();

                        response.Add(resp);
                    }

                    return response;
                }
                else
                {
                    response = null;
                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Recipes Get All recipes

        public async Task<List<RecipeCompleteDTO>> getAllRecipes()
        {
            List<RecipeCompleteDTO> response = new List<RecipeCompleteDTO>();
            DataTable dt = new DataTable();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    await dataAdapter.FillAsync(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        RecipeCompleteDTO resp = new RecipeCompleteDTO();
                        resp.recipeTitle = dt.Rows[i]["recipeTitle"].ToString();
                        resp.recipeImage = dt.Rows[i]["recipeImage"].ToString();
                        resp.recID = (int)dt.Rows[i]["recID"];
                        resp.brand = dt.Rows[i]["brand"].ToString();
                        resp.category = dt.Rows[i]["category"].ToString();
                        resp.quantity = dt.Rows[i]["quantity"].ToString();
                        resp.ingredientTitle = dt.Rows[i]["ingredientTitle"].ToString();
                        resp.price = Convert.ToDecimal(dt.Rows[i]["price"]);
                        resp.barcode = dt.Rows[i]["barcode"].ToString();
                        resp.ingredientImage = dt.Rows[i]["ingredientImage"].ToString();

                        response.Add(resp);
                    }

                    return response;
                }
                else
                {
                    response = null;
                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Products GetProduct

        public async Task<ProductDTO> getProduct(ProductDTO product)
        {
            DataTable dt = new DataTable();
            ProductDTO response = new ProductDTO();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@barcode", product.barcode);
                    await dataAdapter.FillAsync(dt);
                }

                bool hasProduct = dt.Rows.GetEnumerator().MoveNext();

                if (hasProduct)
                {
                    response.productID = (int)dt.Rows[0]["productID"];
                    response.title = dt.Rows[0]["title"].ToString();
                    response.brand = dt.Rows[0]["brand"].ToString();
                    response.category = dt.Rows[0]["category"].ToString();
                    response.uri = dt.Rows[0]["uri"].ToString();
                    response.price = Convert.ToDouble(dt.Rows[0]["price"]);
                    response.description = dt.Rows[0]["descr"].ToString();
                    response.features = dt.Rows[0]["features"].ToString();
                    response.usage = dt.Rows[0]["_usage"].ToString();
                    response.quantity = dt.Rows[0]["quantity"].ToString();
                    response.barcode = dt.Rows[0]["barcode"].ToString();

                    return response;
                }
                else
                {
                    response = null;
                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Products GetProducts

        public async Task<List<SearchResponseDTO>> getProducts()
        {
            List<SearchResponseDTO> response = new List<SearchResponseDTO>();
            DataTable dt = new DataTable();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    await dataAdapter.FillAsync(dt);
                }

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    SearchResponseDTO resp = new SearchResponseDTO();
                    resp.productID = (int)dt.Rows[i]["productID"];
                    resp.title = dt.Rows[i]["title"].ToString();
                    resp.category = dt.Rows[i]["category"].ToString();
                    resp.brand = dt.Rows[i]["brand"].ToString();
                    resp.uri = dt.Rows[i]["uri"].ToString();
                    resp.price = Convert.ToDouble(dt.Rows[i]["price"]);
                    resp.description = dt.Rows[i]["descr"].ToString();
                    resp.features = dt.Rows[i]["features"].ToString();
                    resp.usage = dt.Rows[i]["_usage"].ToString();
                    resp.quantity = dt.Rows[i]["quantity"].ToString();
                    resp.barcode = dt.Rows[i]["barcode"].ToString();

                    response.Add(resp);
                }

                return response;
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Products DeleteProduct

        public async Task<bool> deleteProduct(ProductDTO product)
        {
            int result;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@barcode", product.barcode);

                    result = await comm.ExecuteNonQueryAsync();
                }

                if (result == -1)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Products EditProduct

        public async Task<bool> editProduct(ProductUpdateDTO product)
        {
            int result;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@title", product.title);
                    comm.Parameters.AddWithValue("@category", product.category);
                    comm.Parameters.AddWithValue("@brand", product.brand);
                    comm.Parameters.AddWithValue("@uri", product.uri);
                    comm.Parameters.AddWithValue("@price", Convert.ToDecimal(product.price));
                    comm.Parameters.AddWithValue("@description", product.description);
                    comm.Parameters.AddWithValue("@features", product.features);
                    comm.Parameters.AddWithValue("@usage", product.usage);
                    comm.Parameters.AddWithValue("@quantity", product.quantity);
                    comm.Parameters.AddWithValue("@barcode", product.barcode);
                    comm.Parameters.AddWithValue("@_barcode", product._barcode);

                    result = await comm.ExecuteNonQueryAsync();
                }

                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Paid Items AddToPaidFor

        public async Task<bool> AddToPaidFor(CartDTO item)
        {
            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                {
                    int result = 0;

                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;

                    comm.Parameters.AddWithValue("@userID", item.userID);
                    comm.Parameters.AddWithValue("@title", item.title);
                    comm.Parameters.AddWithValue("@brand", item.brand);
                    comm.Parameters.AddWithValue("@barcode", item.barcode);
                    comm.Parameters.AddWithValue("@quantity", item.quantity);
                    comm.Parameters.AddWithValue("@price", item.price);
                    result = await comm.ExecuteNonQueryAsync();
                    comm.Parameters.Clear();

                    if (result > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Paid Items GetPaidFor

        public async Task<List<CartDTO>> GetPaidFor(int userID)
        {
            DataTable dt = new DataTable();
            List<CartDTO> results = new List<CartDTO>();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@userID", userID);

                    await dataAdapter.FillAsync(dt);
                    comm.Parameters.Clear();
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        CartDTO result = new CartDTO();

                        result.userID = (int)dt.Rows[i]["pid"];
                        result.brand = dt.Rows[i]["brand"].ToString();
                        result.title = dt.Rows[i]["productTitle"].ToString();
                        result.price = Convert.ToDouble(dt.Rows[i]["price"]);
                        result.quantity = Convert.ToInt32(dt.Rows[i]["quantity"]);
                        result.barcode = dt.Rows[i]["barcode"].ToString();

                        results.Add(result);
                    }

                    return results;
                }
                else
                {
                    results = null;
                    return results;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Paid Items ClearPaidItems

        public async Task<bool> ClearPaidItems(int userID)
        {
            bool result = false;
            int response = 0;
            string clearQuery = "DELETE FROM PaidForItems WHERE pid = @userID";

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(clearQuery, conn))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@userID", userID);

                    response = await comm.ExecuteNonQueryAsync();
                }

                if (response > 0)
                {
                    result = true;
                }

                return result;
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Favorites GetFavorites

        public async Task<List<FavoritesDTO>> GetFavorites(int userID)
        {
            List<FavoritesDTO> response = new List<FavoritesDTO>();
            DataTable dt = new DataTable();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@userID", userID);
                    await dataAdapter.FillAsync(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        FavoritesDTO resp = new FavoritesDTO();
                        resp.userID = (int)dt.Rows[i]["favID"];
                        resp.favID = (int)dt.Rows[i]["favID"];
                        resp.ID = (int)dt.Rows[i]["ID"];
                        resp.productID = (int)dt.Rows[i]["productID"];
                        resp.title = dt.Rows[i]["productTitle"].ToString();
                        resp.description = dt.Rows[i]["description"].ToString();
                        resp.brand = dt.Rows[i]["brand"].ToString();
                        resp.quantity = dt.Rows[i]["quantity"].ToString();
                        resp.uri = dt.Rows[i]["uri"].ToString();
                        resp.barcode = dt.Rows[i]["barcode"].ToString();
                        resp.price = Convert.ToDouble(dt.Rows[i]["price"]);

                        response.Add(resp);
                    }

                    return response;
                }
                else
                {
                    response = null;
                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Favorites AddToFavorites

        public async Task<bool> AddToFavorites(FavoritesDTO item)
        {
            bool response = false;
            int resultFav;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;

                    comm.Parameters.AddWithValue("@userID", item.userID);
                    comm.Parameters.AddWithValue("@favID", item.userID);
                    comm.Parameters.AddWithValue("@uri", item.uri);
                    comm.Parameters.AddWithValue("@description", item.description);
                    comm.Parameters.AddWithValue("@productTitle", item.title);
                    comm.Parameters.AddWithValue("@brand", item.brand);
                    comm.Parameters.AddWithValue("@barcode", item.barcode);
                    comm.Parameters.AddWithValue("@quantity", item.quantity);
                    comm.Parameters.AddWithValue("@price", item.price);
                    comm.Parameters.AddWithValue("@productID", item.productID);

                    resultFav = await comm.ExecuteNonQueryAsync();

                    comm.Parameters.Clear();
                }

                if (resultFav > 0)
                {
                    response = true;
                    return response;
                }

                return response;
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Favorites RemoveFromFavorites

        public async Task<bool> RemoveFromFavorites(int ID, int favID)
        {
            bool result = false;
            int resultFavorite;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@ID", ID);
                    comm.Parameters.AddWithValue("@favID", favID);

                    resultFavorite = await comm.ExecuteNonQueryAsync();
                }

                if (resultFavorite > 0)
                {
                    result = true;
                    return result;
                }

                return result;
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Cart AddToCart

        public async Task<bool> AddToCart(CartDTO item)
        {
            int resultCart;
            bool result = false;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                {
                    await conn.OpenAsync();

                    comm.CommandType = CommandType.Text;
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@userID", item.userID);
                    comm.Parameters.AddWithValue("@cartID", item.userID);
                    comm.Parameters.AddWithValue("@productTitle", item.title);
                    comm.Parameters.AddWithValue("@brand", item.brand);
                    comm.Parameters.AddWithValue("@barcode", item.barcode);
                    comm.Parameters.AddWithValue("@quantity", item.quantity);
                    comm.Parameters.AddWithValue("@price", item.price);

                    resultCart = await comm.ExecuteNonQueryAsync();
                }

                if (resultCart > 0)
                {
                    result = true;
                    return result;
                }

                return result;
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Deliveries GetDeliveries

        public async Task<List<DeliveryItemDTO>> getDeliveries(int userID)
        {
            List<DeliveryItemDTO> response = new List<DeliveryItemDTO>();
            DataTable dt = new DataTable();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@userID", userID);

                    await dataAdapter.FillAsync(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DeliveryItemDTO item = new DeliveryItemDTO();
                        item.userID = (int)dt.Rows[i]["userID"];
                        item.deliveryID = (int)dt.Rows[i]["deliveryID"];
                        item.userName = dt.Rows[i]["userName"].ToString();
                        item.brand = dt.Rows[i]["brand"].ToString();
                        item.selectedAddress = dt.Rows[i]["selectedAddress"].ToString();
                        item.deliveryDate = Convert.ToDateTime(dt.Rows[i]["deliveryDate"]);
                        item.title = dt.Rows[i]["title"].ToString();
                        item.quantity = (int)dt.Rows[i]["quantity"];
                        item.price = Convert.ToDouble(dt.Rows[i]["price"]);

                        response.Add(item);
                    }
                    return response;
                }
                else
                {
                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region CompleteTransaction CompleteCollection

        public async Task<bool> CompleteCollection(CompleteTransactionDTO input)
        {
            bool response = false;
            int output;
            string queryA = "INSERT INTO Collections(collectionID, userID, userName, selectedShop, selectedCollectionDate, transactionDate) VALUES(@collectionID, @userID, @userName, @selectedShop, @selectedCollectionDate, @transactionDate);";
            string queryB = "INSERT INTO CollectionItems(colID, title, brand, quantity, price, productID) VALUES(@collectionID, @title, @brand, @quantity, @price, @productID);";

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand commCollection = new MySqlCommand(queryA, conn))
                using (MySqlCommand commCollectionItems = new MySqlCommand(queryB, conn))
                {
                    await conn.OpenAsync();
                    commCollection.CommandType = CommandType.Text;
                    commCollectionItems.CommandType = CommandType.Text;

                    commCollection.Parameters.AddWithValue("@collectionID", input.userID);
                    commCollection.Parameters.AddWithValue("@userID", input.userID);
                    commCollection.Parameters.AddWithValue("@userName", input.userName);
                    commCollection.Parameters.AddWithValue("@selectedShop", input.selectedShop);
                    commCollection.Parameters.AddWithValue("@selectedCollectionDate", input.selectedCollectionDate);
                    commCollection.Parameters.AddWithValue("@transactionDate", input.transactionDate);

                    output = await commCollection.ExecuteNonQueryAsync();

                    if (output > 0)
                    {
                        for (int i = 0; i < input.paidItems.Length; i++)
                        {
                            commCollectionItems.Parameters.Clear();

                            commCollectionItems.Parameters.AddWithValue("@collectionID", input.userID);
                            commCollectionItems.Parameters.AddWithValue("@title", input.paidItems[i].title);
                            commCollectionItems.Parameters.AddWithValue("@brand", input.paidItems[i].brand);
                            commCollectionItems.Parameters.AddWithValue("@quantity", input.paidItems[i].quantity);
                            commCollectionItems.Parameters.AddWithValue("@price", input.paidItems[i].price);
                            commCollectionItems.Parameters.AddWithValue("@productID", input.paidItems[i].productID);

                            commCollectionItems.ExecuteNonQuery();
                        }

                        response = await ClearPaidItems(input.userID);
                    }
                    else
                    {
                        response = false;
                    }
                }

                return response;
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region CompleteTransaction CompleteDelivery

        public async Task<bool> CompleteDelivery(CompleteTransactionDTO input)
        {
            bool response = false;
            int output;
            string queryB = "INSERT INTO DeliveryItems(delID, title, brand, quantity, price, productID) VALUES(@deliveryID, @title, @brand, @quantity, @price, @productID)";

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand commDelivery = new MySqlCommand(query, conn))
                using (MySqlCommand commDeliveryItems = new MySqlCommand(queryB, conn))
                {
                    await conn.OpenAsync();
                    commDelivery.CommandType = CommandType.Text;
                    commDeliveryItems.CommandType = CommandType.Text;

                    commDelivery.Parameters.AddWithValue("@deliveryID", input.userID);
                    commDelivery.Parameters.AddWithValue("@userID", input.userID);
                    commDelivery.Parameters.AddWithValue("@userName", input.userName);
                    commDelivery.Parameters.AddWithValue("@selectedAddress", input.selectedAddress);
                    commDelivery.Parameters.AddWithValue("@deliveryDate", input.deliveryDate);

                    output = await commDelivery.ExecuteNonQueryAsync();

                    if (output > 0)
                    {
                        for (int i = 0; i < input.paidItems.Length; i++)
                        {
                            commDeliveryItems.Parameters.Clear();

                            commDeliveryItems.Parameters.AddWithValue("@deliveryID", input.userID);
                            commDeliveryItems.Parameters.AddWithValue("@title", input.paidItems[i].title);
                            commDeliveryItems.Parameters.AddWithValue("@brand", input.paidItems[i].brand);
                            commDeliveryItems.Parameters.AddWithValue("@quantity", input.paidItems[i].quantity);
                            commDeliveryItems.Parameters.AddWithValue("@price", input.paidItems[i].price);
                            commDeliveryItems.Parameters.AddWithValue("@productID", input.paidItems[i].productID);

                            commDeliveryItems.ExecuteNonQuery();
                        }

                        response = await ClearPaidItems(input.userID);
                    }
                    else
                    {
                        response = false;
                    }
                }

                return response;
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Collection GetCollection

        public async Task<List<CollectionItemDTO>> getCollections(int userID)
        {
            List<CollectionItemDTO> response = new List<CollectionItemDTO>();
            DataTable dt = new DataTable();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@userID", userID);

                    await dataAdapter.FillAsync(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        CollectionItemDTO item = new CollectionItemDTO();

                        item.userID = (int)dt.Rows[i]["userID"];
                        item.userName = dt.Rows[i]["userName"].ToString();
                        item.brand = dt.Rows[i]["brand"].ToString();
                        item.title = dt.Rows[i]["title"].ToString();
                        item.quantity = (int)dt.Rows[i]["quantity"];
                        item.price = Convert.ToDouble(dt.Rows[i]["price"]);
                        item.selectedShop = dt.Rows[i]["selectedShop"].ToString();
                        item.selectedCollectionDate = Convert.ToDateTime(dt.Rows[i]["selectedCollectionDate"]);
                        item.transactionDate = Convert.ToDateTime(dt.Rows[i]["transactionDate"]);

                        response.Add(item);
                    }

                    return response;
                }
                else
                {
                    response = null;
                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Catalogue GetCatalogue

        public async Task<List<CatalogueDTO>> getCatalogues(string category)
        {
            DataTable dt = new DataTable();
            List<CatalogueDTO> response = new List<CatalogueDTO>();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    await dataAdapter.FillAsync(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        CatalogueDTO resp = new CatalogueDTO();
                        resp.brand = dt.Rows[i]["brand"].ToString();
                        resp.category = dt.Rows[i]["category"].ToString();

                        response.Add(resp);
                    }

                    return response;
                }
                else
                {
                    response = null;
                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Cart GetCartItems

        public async Task<List<CartItemDTO>> getCartItems(int userID)
        {
            DataTable dt = new DataTable();
            List<CartItemDTO> cartItems = new List<CartItemDTO>();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@userID", userID);
                    await dataAdapter.FillAsync(dt);
                }

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    CartItemDTO result = new CartItemDTO();
                    result.userID = (int)dt.Rows[i]["cartID"];
                    result.cartItemID = (int)dt.Rows[i]["ID"];
                    result.brand = dt.Rows[i]["brand"].ToString();
                    result.title = dt.Rows[i]["productTitle"].ToString();
                    result.price = Convert.ToDouble(dt.Rows[i]["price"]);
                    result.quantity = (int)dt.Rows[i]["quantity"];
                    result.barcode = dt.Rows[i]["barcode"].ToString();

                    cartItems.Add(result);
                }

                return cartItems;
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Cart RemoveFromCart

        public async Task<bool> RemoveFromCart(int cartItemID, int cartID)
        {
            int result;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@userID", cartID);
                    comm.Parameters.AddWithValue("@cartItemID", cartItemID);

                    result = await comm.ExecuteNonQueryAsync();
                    comm.Parameters.Clear();
                }

                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Cart CartCount

        public async Task<int> cartCount(int userID)
        {
            try
            {
                if (userID != 0)
                {
                    DataTable dt = new DataTable();
                    int count = 0;

                    using (MySqlConnection conn = new MySqlConnection(connStr))
                    using (MySqlCommand comm = new MySqlCommand(query, conn))
                    using (var dataAdapter = new MySqlDataAdapter(comm))
                    {
                        await conn.OpenAsync();
                        comm.CommandType = CommandType.Text;
                        comm.Parameters.AddWithValue("@userID", userID);
                        await dataAdapter.FillAsync(dt);
                    }

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        count += (int)dt.Rows[i]["quantity"];
                    }

                    return count;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Cart UpdateCartItem

        public async Task<bool> updateCartItem(CartItemUpdateDTO item)
        {
            int result;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@quantity", item.quantity);
                    comm.Parameters.AddWithValue("@userID", item.userID);
                    comm.Parameters.AddWithValue("@cartItemID", item.cartItemID);

                    result = await comm.ExecuteNonQueryAsync();
                }

                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Cart RemoveAllItems

        public async Task<bool> removeAllItems(int userID)
        {
            int result;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@userID", userID);
                    comm.Parameters.AddWithValue("@cartID", userID);

                    result = await comm.ExecuteNonQueryAsync();

                    if (result > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Add Ingredients To Cart

        public async Task<int> addIngredientsToCart(List<CartDTO> items)
        {
            int response = 0;
            var _cartCount = 0;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;

                    for (var i = 0; i < items.Count(); i++)
                    {
                        comm.Parameters.AddWithValue("@userID", items[i].userID);
                        comm.Parameters.AddWithValue("@cartID", items[i].userID);
                        comm.Parameters.AddWithValue("@productTitle", items[i].title);
                        comm.Parameters.AddWithValue("@brand", items[i].brand);
                        comm.Parameters.AddWithValue("@barcode", items[i].barcode);
                        comm.Parameters.AddWithValue("@quantity", items[i].quantity);
                        comm.Parameters.AddWithValue("@price", items[i].price);

                        response = await comm.ExecuteNonQueryAsync();
                        comm.Parameters.Clear();
                    }
                }

                if (response > 0)
                {
                    var _query = "SELECT COUNT(*) FROM CartItems WHERE cartID = @userID";

                    using (MySqlConnection conn = new MySqlConnection(connStr))
                    using (MySqlCommand comm = new MySqlCommand(_query, conn))
                    {
                        await conn.OpenAsync();
                        comm.CommandType = CommandType.Text;
                        comm.Parameters.AddWithValue("@userID", (int)items[0].userID);
                        _cartCount = int.Parse(comm.ExecuteScalar().ToString());
                        Console.WriteLine(_cartCount);
                        await conn.CloseAsync();
                    }
                }

                return _cartCount;
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Authentication Login

        public async Task<LoginResponseDTO> login(UserDTO user, string token)
        {
            var encPassword = UserHasherService.encrypt(user.password);
            DataTable dt = new DataTable();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@email", user.email);
                    comm.Parameters.AddWithValue("@encPassword", encPassword);
                    await dataAdapter.FillAsync(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    UserDTO currentUser = await GetUser(user);

                    LoginResponseDTO response = new LoginResponseDTO();
                    response.expiresIn = (2 * 60 * 60).ToString();
                    response.userId = (int)dt.Rows[0]["userID"];
                    response.userName = dt.Rows[0]["firstName"].ToString();
                    response.userSurname = dt.Rows[0]["lastName"].ToString();
                    response.doj = dt.Rows[0]["doj"].ToString();
                    response.userRole = dt.Rows[0]["userRole"].ToString();
                    response.token = token;
                    response.success = true;

                    return response;
                }
                else
                {
                    LoginResponseDTO response = new LoginResponseDTO();
                    response.expiresIn = null;
                    response.userName = user.email;
                    response.userRole = string.Empty;
                    response.token = "";
                    response.success = false;

                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Authentication Signup

        public async Task<SignupResponseDTO> signup(UserDTO user)
        {
            try
            {
                SignupResponseDTO response = new SignupResponseDTO();
                DataTable dt = new DataTable();
                string encPassword = UserHasherService.encrypt(user.password);

                response.firstName = user.firstName;
                response.surname = user.surname;
                response.email = user.email;
                response.encPassword = encPassword;
                response.success = true;
                DateTime doj = DateTime.Now;
                response.doj = doj;

                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@firstName", response.firstName);
                    comm.Parameters.AddWithValue("@lastName", response.surname);
                    comm.Parameters.AddWithValue("@email", response.email);
                    comm.Parameters.AddWithValue("@encPassword", response.encPassword);
                    comm.Parameters.AddWithValue("@doj", response.doj);

                    await dataAdapter.FillAsync(dt);
                }

                return response;
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Addresses CreateAddress

        public async Task<string> createAddress(AddressDTO address)
        {
            int result;
            DataTable dt = new DataTable();
            string _query = "SELECT * FROM AddressesNew WHERE addressNickName = '" + address.addressNickName + "'";

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand commExists = new MySqlCommand(_query, conn))
                using (var dataAdapter = new MySqlDataAdapter(commExists))
                {
                    await conn.OpenAsync();
                    commExists.CommandType = CommandType.Text;

                    commExists.Parameters.AddWithValue("@userID", address.addressID);
                    commExists.Parameters.AddWithValue("@addressNickName", address.addressNickName);
                    commExists.Parameters.AddWithValue("@userAddress", address.userAddress);

                    await dataAdapter.FillAsync(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    return nameof(AddressAdded.Exists);
                }
                else
                {
                    using (MySqlConnection conn = new MySqlConnection(connStr))
                    using (MySqlCommand comm = new MySqlCommand(query, conn))
                    {
                        await conn.OpenAsync();
                        comm.CommandType = CommandType.Text;

                        comm.Parameters.AddWithValue("@userID", address.addressID);
                        comm.Parameters.AddWithValue("@userAddress", address.userAddress);
                        comm.Parameters.AddWithValue("@addressNickName", address.addressNickName);
                        comm.Parameters.AddWithValue("@isDefault", address.isDefault);

                        result = await comm.ExecuteNonQueryAsync();
                    }

                    if (result > 0)
                    {
                        return nameof(AddressAdded.True);
                    }
                    else
                    {
                        return nameof(AddressAdded.False);
                    }
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Addresses GetAddress

        public async Task<List<AddressDTO>> getAddresses(int userID)
        {
            List<AddressDTO> response = new List<AddressDTO>();
            DataTable dt = new DataTable();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@userID", userID);

                    await dataAdapter.FillAsync(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        AddressDTO resp = new AddressDTO();
                        resp.addressID = (int)dt.Rows[i]["addressID"];
                        resp.userAddress = dt.Rows[i]["userAddress"].ToString();
                        resp.ID = (int)dt.Rows[i]["ID"];
                        resp.addressNickName = dt.Rows[i]["addressNickName"].ToString();
                        resp.isDefault = dt.Rows[i]["isDefault"].ToString();

                        response.Add(resp);
                    }

                    return response;
                }
                else
                {
                    response = null;
                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Addresses DeleteAddress

        public async Task<bool> deleteAddress(int addressID, int userID)
        {
            int response;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@addressID", addressID);
                    comm.Parameters.AddWithValue("@userID", userID);

                    response = await comm.ExecuteNonQueryAsync();
                }

                if (response > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Addresses UpdateAddress

        public async Task<bool> updateAddress(AddressDTO address)
        {
            int response = 0;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@userAddress", address.userAddress);
                    comm.Parameters.AddWithValue("@ID", address.ID);
                    comm.Parameters.AddWithValue("@userID", address.addressID);
                    comm.Parameters.AddWithValue("@addressNickName", address.addressNickName);
                    comm.Parameters.AddWithValue("@isDefault", address.isDefault);

                    response = await comm.ExecuteNonQueryAsync();
                }

                if (response > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        #endregion
    }
}
