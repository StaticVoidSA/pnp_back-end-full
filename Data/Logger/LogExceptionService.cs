﻿using System;

namespace RnR.Data.Logger
{
    public class LogExceptionService
    {
        public static void Log(Exception e)
        {
            LogService logger = new LogService();
            logger.LogWrite(string.Format("Error: {0}", arg0: e.Message));
        }
    }
}
