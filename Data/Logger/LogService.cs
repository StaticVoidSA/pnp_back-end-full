﻿using System;
using System.IO;
using System.Reflection;

namespace RnR.Data.Logger
{
    #region Interfaces

    public interface ILogger
    {
        public void LogWrite(string logMessage);
        public void Log(string logMessage, TextWriter txtWriter);
    }

    #endregion

    public class LogService : ILogger
    {
        string path = string.Empty;

        public LogService()
        {
            path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        }

        public void LogWrite(string logMessage)
        {
            try
            {
                using (StreamWriter writer = File.AppendText(path + "/log.txt"))
                {
                    Log(logMessage, writer);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void LogWrite(string logMessage, string _value)
        {
            try
            {
                using (StreamWriter writer = File.AppendText(path + "/log.txt")) 
                {
                    Log((logMessage + " " + _value), writer);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void AdminLogWrite(string logMessage)
        {
            try
            {
                using (StreamWriter writer = File.AppendText(path + "/ADMIN_log.txt"))
                {
                    Log(logMessage, writer);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void AdminLogWrite(string logMessage, string _value)
        {
            try
            {
                using (StreamWriter writer = File.AppendText(path + "/ADMIN_log.txt"))
                {
                    Log((logMessage + " " + _value), writer);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void Log(string logMessage, TextWriter textWriter)
        {
            try
            {
                textWriter.Write("------------------------------\r\nLog Entry : ");
                textWriter.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString());
                textWriter.WriteLine("{0}", logMessage);
                textWriter.WriteLine("------------------------------");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
