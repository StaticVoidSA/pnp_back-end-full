﻿using System;
using RnR.Data.DTOs;
using RnR.Data.DAOs;
using RnR.Services.HashingService;
using MimeKit;
using MailKit.Net.Smtp;
using System.Threading.Tasks;

namespace RnR.Services.MailService
{
    public interface IEmail
    {
        public Task<bool> sendConfirmationEmail(string email);
        public Task<bool> sendReceiptEmail(string email, CompleteTransactionDTO transaction);
        public Task<UserTokenObject> getUserTokenObject(string email);
    }

    public class UserTokenObject
    {
        public string fullName { get; set; }
        public string userToken { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
    }

    public class SendEmailService : IEmail
    {
        private readonly string connStr = "Server=remotemysql.com;Database=rCi8OTH6kH;Uid=rCi8OTH6kH;Pwd=jQoZZLNlfq;";
        private readonly string query = "SELECT * FROM Users WHERE email = @email";
        UserService service;

        public SendEmailService()
        {
            service = new UserService(connStr, query);
        }

        public async Task<UserTokenObject> getUserTokenObject(string email)
        {

            UserTokenObject tokenObject = new UserTokenObject();

            tokenObject.fullName = await service.GetUserFullname(email);
            tokenObject.userToken = UserHasherService.encrypt(email + "&" + tokenObject.fullName);
            tokenObject.name = tokenObject.fullName.Split(" ")[0];
            tokenObject.surname = tokenObject.fullName.Split(" ")[1];

            return tokenObject;
        }

        public async Task<bool> sendConfirmationEmail(string email)
        {
            try
            {
                UserTokenObject tokenObject = await getUserTokenObject(email);

                MimeMessage message = new MimeMessage();

                MailboxAddress from = new MailboxAddress("Admin", "admin@rnr.com");
                message.From.Add(from);

                MailboxAddress to = new MailboxAddress(tokenObject.fullName, email);
                message.To.Add(to);

                message.Subject = "Rick And Ray Reset Password";

                BodyBuilder bodyBuilder = new BodyBuilder();

                bodyBuilder.HtmlBody = "<h2 style='color: #B9154A;'><u>RICK AND RAY<u></h2>" +
                    "<br /><br />" +
                    "<label>Welcome " + tokenObject.name + "</label><br /><br />" +
                    "<p>Click the link to reset your password https://ecommerce-front-end-jonathan-j.herokuapp.com/reset/" + tokenObject.userToken +
                    "</p><br /><br /><label style='color:#B9154A;'>Please do not tamper the URL...</Label>";

                message.Body = bodyBuilder.ToMessageBody();

                using (SmtpClient client = new SmtpClient())
                {
                    client.Connect("smtp.gmail.com", 465, true);
                    client.Authenticate("email", "password");

                    client.Send(message);
                    client.Disconnect(true);
                }

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        // TODO:
        // Complete this section to send a more detailed email to client
        public async Task<bool> sendReceiptEmail(string email, CompleteTransactionDTO transaction)
        {
            try
            {
                bool response = false;
                UserTokenObject tokenObject = await getUserTokenObject(email);


                MimeMessage message = new MimeMessage();

                MailboxAddress from = new MailboxAddress("Admin", "admin@rnr.com");
                message.From.Add(from);

                MailboxAddress to = new MailboxAddress(tokenObject.fullName, email);
                message.To.Add(to);

                message.Subject = "Rick And Ray Reciept";

                BodyBuilder bodyBuilder = new BodyBuilder();

                bodyBuilder.HtmlBody = "<h2 style='color: #B9154A;'><u>RICK AND RAY<u></h2>" +
                    "<h3>Thank you for your purchase</h3><br />" +
                    "<label>Hi " + tokenObject.name + " " + tokenObject.surname + "</label><br />" +
                    "<p>Thank you for your purchase</p>" +
                    "<p>You have selected: " + transaction.userSelection + "</p><br />" +
                    "<p>Number Of Paid Items:" + transaction.paidItems.Length + "</p></br />";

                message.Body = bodyBuilder.ToMessageBody();

                using (SmtpClient client = new SmtpClient())
                {
                    client.Connect("smtp.gmail.com", 465, true);
                    client.Authenticate("email", "password");

                    client.Send(message);
                    client.Disconnect(true);
                    response = true;
                }

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
