﻿namespace RnR.Data.Models
{
    public class Lists
    {
        public int userID { get; set; }
        public string listName { get; set; }
        public int itemID { get; set; }
    }
}
