﻿namespace RnR.Data.Models
{
    public class Favorites : Product
    {
        public int ID { get; set; }
        public int userID { get; set; }
        public int favID { get; set; }

        public Favorites() : base()
        {

        }
    }
}
