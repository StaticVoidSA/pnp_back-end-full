﻿namespace RnR.Data.Models
{
    public class ResetResponse
    {
        public string email { get; set; }
        public bool emailSent { get; set; }
    }
}
