﻿namespace RnR.Data.Models
{
    public class CompleteResponse
    {
        public string email { get; set; }
        public bool passwordChanged { get; set; }
    }
}
