﻿namespace RnR.Data.Models
{
    public class SearchRequest : Product
    {
        public string item { get; set; }
        public double? minRange { get; set; }
        public double? maxRange { get; set; }

        public SearchRequest() : base()
        {

        }
    }
}
