﻿using System;

namespace RnR.Data.Models
{
    public class IngredientCartModel 
    {
        public int userID { get; set; }
        public int cartID { get; set; }
        public string title { get; set; }
        public string brand { get; set; }
        public double price { get; set; }
        public int quantity { get; set; }
        public string barcode { get; set; }
        public string productID { get; set; }
    }
}
