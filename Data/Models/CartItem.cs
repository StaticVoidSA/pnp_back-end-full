﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RnR.Data.Models
{
    public class CartItem : Cart
    {
        public int cartItemID { get; set; }

        public CartItem() : base()
        {

        }
    }
}
