﻿namespace RnR.Data.Models
{
    public class CompleteReset
    {
        public string email { get; set; }
        public string password { get; set; }
        public bool isComplete { get; set; }
        public string hash { get; set; }
    }
}
