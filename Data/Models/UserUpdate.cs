﻿using System;

namespace RnR.Data.Models
{
    public class UserUpdate : User
    {
        public string _email { get; set; }

        public UserUpdate() : base()
        {

        }
    }
}
