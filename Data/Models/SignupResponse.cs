﻿namespace RnR.Data.Models
{
    public class SignupResponse : User
    {
        public string encPassword { get; set; }
        public bool success { get; set; } = false;

        public SignupResponse() : base()
        {

        }
    }
}
