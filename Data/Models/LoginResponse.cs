﻿namespace RnR.Data.Models
{
    public class LoginResponse
    {
        public string token { get; set; }
        public string email { get; set; }
        public int userId { get; set; }
        public string expiresIn { get; set; }
        public bool success { get; set; }
        public string userName { get; set; }
        public string userSurname { get; set; }
        public string userRole { get; set; }
        public string doj { get; set; }
    }
}
