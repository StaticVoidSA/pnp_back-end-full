﻿namespace RnR.Data.Models
{
    public class Ingredients : Product
    {
        public int recID { get; set; }
        public string ingredientImage { get; set; }

        public Ingredients() : base()
        {

        }
    }
}
