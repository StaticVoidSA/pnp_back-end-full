﻿using System;

namespace RnR.Data.Models
{
    public class User
    {
        public string firstName { get; set; }
        public string surname { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public DateTime doj { get; set; }
        public string userRole { get; set; }
        public int userID { get; set; }
        public string token { get; set; }
    }
}
