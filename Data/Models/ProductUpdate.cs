﻿namespace RnR.Data.Models
{
    public class ProductUpdate : Product
    {
        public string _barcode { get; set; }

        public ProductUpdate() : base()
        {

        }
    }
}
