﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RnR.Data.Models
{
    public class Address
    {
        public int addressID { get; set; }
        public int ID { get; set; }
        public string userAddress { get; set; }
        public string addressNickName { get; set; }
        public string isDefault { get; set; }
    }
}
