﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RnR.Data.Models
{
    public class Cart
    {
        public int cartID { get; set; }
        public int productID { get; set; }
        public string title { get; set; }
        public string brand { get; set; }
        public int quantity { get; set; }
        public double price { get; set; }
        public int userID { get; set; }
        public string barcode { get; set; }
    }
}
