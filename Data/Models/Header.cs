﻿namespace RnR.Data.Models
{
    public class Header
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
