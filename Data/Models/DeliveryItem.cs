﻿using System;

namespace RnR.Data.Models
{
    public class DeliveryItem
    {
        public int userID { get; set; }
        public int deliveryID { get; set; }
        public string userName { get; set; }
        public string selectedAddress { get; set; }
        public DateTime deliveryDate { get; set; }
        public string title { get; set; }
        public string brand { get; set; }
        public int quantity { get; set; }
        public double price { get; set; }
    }
}
