﻿namespace RnR.Data.Models
{
    public class Recipe
    {
        public int recipeID { get; set; }
        public string title { get; set; }
        public string recipeImage { get; set; }
    }
}
