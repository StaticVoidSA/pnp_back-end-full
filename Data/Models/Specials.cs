﻿namespace RnR.Data.Models
{
    public class Specials : Product
    {
        public string discount { get; set; }
        public double oldPrice { get; set; }

        public Specials() : base()
        {

        }
    }
}
