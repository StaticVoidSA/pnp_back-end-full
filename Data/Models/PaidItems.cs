﻿namespace RnR.Data.Models
{
    public class PaidItems
    {
        public int userID { get; set; }
        public string title { get; set; }
        public string brand { get; set; }
        public string barcode { get; set; }
        public int quantity { get; set; }
        public double price { get; set; }
        public int productID { get; set; }
    }
}
