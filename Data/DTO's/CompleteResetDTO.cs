﻿using System;

namespace RnR.Data.DTOs
{
    public class CompleteResetDTO
    {
        public string email { get; set; }
        public string password { get; set; }
        public bool isComplete { get; set; }
        public string hash { get; set; }
    }
}
