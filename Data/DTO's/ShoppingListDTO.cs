﻿using System;

namespace RnR.Data.DTOs
{
    public class ShoppingListDTO
    {
        public int userID { get; set; }
        public string shoppingListName { get; set; }
        public string title { get; set; }
        public string brand { get; set; }
        public string barcode { get; set; }
        public int quantity { get; set; }
        public decimal price { get; set; }
        public int shoppingListID { get; set; }
        public int itemID { get; set; }

    }
}
