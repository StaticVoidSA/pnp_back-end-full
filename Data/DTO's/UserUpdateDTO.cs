﻿using System;

namespace RnR.Data.DTOs
{
    public class UserUpdateDTO : UserDTO
    {
        public string _email { get; set; }

        public UserUpdateDTO() : base()
        {

        }
    }
}
