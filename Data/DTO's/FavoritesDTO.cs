﻿using System;

namespace RnR.Data.DTOs
{
    public class FavoritesDTO : ProductDTO
    {
        public int ID { get; set; }
        public int userID { get; set; }
        public int favID { get; set; }

        public FavoritesDTO() : base()
        {

        }
    }
}
