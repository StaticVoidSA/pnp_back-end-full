﻿using System;

namespace RnR.Data.DTOs
{
    public class CartItemDTO : CartDTO
    {
        public int cartItemID { get; set; }
    }
}
