﻿using System;

namespace RnR.Data.DTOs
{
    public class SignupResponseDTO : UserDTO
    {
        public string encPassword { get; set; }
        public bool success { get; set; } = false;

        public SignupResponseDTO() : base()
        {

        }
    }
}
