﻿using System;

namespace RnR.Data.DTOs
{
    public class SpecialsDTO : ProductDTO
    {
        public string discount { get; set; }
        public double oldPrice { get; set; }

        public SpecialsDTO() : base()
        {

        }
    }
}
