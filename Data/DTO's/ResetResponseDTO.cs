﻿using System;

namespace RnR.Data.DTOs
{
    public class ResetResponseDTO
    {
        public string email { get; set; }
        public bool emailSent { get; set; }
    }
}
