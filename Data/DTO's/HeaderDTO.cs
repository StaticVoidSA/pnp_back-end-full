﻿using System;

namespace RnR.Data.DTOs
{
    public class HeaderDTO
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
