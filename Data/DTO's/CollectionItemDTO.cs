﻿using System;

namespace RnR.Data.DTOs
{
    public class CollectionItemDTO
    {
        public int userID { get; set; }
        public int collectionID { get; set; }
        public string userName { get; set; }
        public string selectedShop { get; set; }
        public DateTime selectedCollectionDate { get; set; }
        public DateTime transactionDate { get; set; }
        public string title { get; set; }
        public string brand { get; set; }
        public int quantity { get; set; }
        public double price { get; set; }
    }
}
