﻿using System;

namespace RnR.Data.DTOs
{
    public class CompleteResponseDTO
    {
        public string email { get; set; }
        public bool passwordChanged { get; set; }
    }
}
