﻿using System;

namespace RnR.Data.DTOs
{
    public class RecipeCompleteDTO 
    {
        public string recipeTitle { get; set; }
        public string recipeImage { get; set; }
        public int recID { get; set; }
        public string brand { get; set; }
        public string category { get; set; }
        public string quantity { get; set; }
        public string ingredientTitle { get; set; }
        public string ingredientImage { get; set; }
        public decimal price { get; set; }
        public string barcode { get; set; }
    }
}
