﻿using System;

namespace RnR.Data.DTOs
{
    public class ProductDTO
    {
        public int productID { get; set; }
        public string title { get; set; }
        public string category { get; set; }
        public string brand { get; set; }
        public string uri { get; set; }
        public double price { get; set; }
        public string description { get; set; }
        public string features { get; set; }
        public string usage { get; set; }
        public string quantity { get; set; }
        public string barcode { get; set; }
    }
}
