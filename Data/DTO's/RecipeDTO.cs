﻿using System;

namespace RnR.Data.DTOs
{
    public class RecipeDTO
    {
        public int recipeID { get; set; }
        public string title { get; set; }
        public string recipeImage { get; set; }
    }
}
