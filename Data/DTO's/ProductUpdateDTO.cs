﻿using System;

namespace RnR.Data.DTOs
{
    public class ProductUpdateDTO : ProductDTO
    {
        public string _barcode { get; set; }

        public ProductUpdateDTO() : base()
        {

        }
    }
}
