﻿using System;

namespace RnR.Data.DTOs
{
    public class IngredientsDTO : ProductDTO
    {
        public int recID { get; set; }
        public string ingredientImage { get; set; }

        public IngredientsDTO() : base()
        {

        }
    }
}
