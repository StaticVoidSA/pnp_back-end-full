﻿using System;

namespace RnR.Data.DTOs
{
    public class SearchRequestDTO : ProductDTO
    {
        public string item { get; set; }
        public double? minRange { get; set; }
        public double? maxRange { get; set; }

        public SearchRequestDTO() : base()
        {

        }
    }
}
