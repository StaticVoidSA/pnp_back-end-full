﻿using System;

namespace RnR.Data.DTOs
{
    public class AddressDTO
    {
        public int addressID { get; set; }
        public int ID { get; set; }
        public string userAddress { get; set; }
        public string addressNickName { get; set; }
        public string isDefault { get; set; }
    }
}
