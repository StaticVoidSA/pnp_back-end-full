﻿using System;

namespace RnR.Data.DTOs
{
    public class ListsDTO
    {
        public int userID { get; set; }
        public string listName { get; set; }
        public int itemID { get; set; }
    }
}
