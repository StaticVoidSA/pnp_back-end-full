﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace RnR.Services.HashingService
{
    public class UserHasherService
    {
        public static string encrypt(string input)
        {
            try
            {
                byte[] bytes = Encoding.Unicode.GetBytes(input);
                SHA256Managed hashstring = new SHA256Managed();
                byte[] hash = hashstring.ComputeHash(bytes);
                string hashString = string.Empty;

                foreach (byte x in hash)
                {
                    hashString += String.Format("{0:x2}", x);
                }

                return hashString;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }

}
