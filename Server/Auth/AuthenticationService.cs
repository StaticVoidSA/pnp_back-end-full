﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using RnR.Data.DAOs;
using RnR.Data.DTOs;
using RnR.Data.Logger;
using RnR.Security.Models;
using RnR.Security.TokenServices;
using RnR.Services.HashingService;

namespace RnR.Server.Auth
{
    #region Interfaces

    public interface IAuth
    {
        public Task<List<StartupResponseDTO>> getUsers();
        public Task<SignupResponseDTO> signup(UserDTO user);
        public Task<LoginResponseDTO> login(UserDTO user, ITokenService tokenService);
    }

    #endregion

    public class AuthService : IAuth
    {
        //private LocalStorageService storageService;
        private readonly IConfiguration _config;
        private string connStr = string.Empty;
        private string query = string.Empty;
        private string generatedToken = string.Empty;

        public AuthService(IConfiguration config, string _connStr, string _query)
        {
            _config = config;
            connStr = _connStr;
            query = _query;
        }

        public async Task<List<StartupResponseDTO>> getUsers()
        {
            List<StartupResponseDTO> response = new List<StartupResponseDTO>();
            DataTable dt = new DataTable();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    await dataAdapter.FillAsync(dt);
                }

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    StartupResponseDTO resp = new StartupResponseDTO();
                    resp.userID = (int)dt.Rows[i]["userID"];
                    resp.firstName = dt.Rows[i]["firstName"].ToString();
                    resp.surname = dt.Rows[i]["lastName"].ToString();
                    resp.doj = Convert.ToDateTime(dt.Rows[i]["doj"]);
                    resp.userRole = dt.Rows[i]["userRole"].ToString();

                    response.Add(resp);
                }

                return response.ToList();

            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        public async Task<SignupResponseDTO> signup(UserDTO user)
        {
            try
            {
                SignupResponseDTO response = new SignupResponseDTO();
                DataTable dt = new DataTable();
                string encPassword = UserHasherService.encrypt(user.password);

                response.firstName = user.firstName;
                response.surname = user.surname;
                response.email = user.email;
                response.encPassword = encPassword;
                response.success = true;
                DateTime doj = DateTime.Now;

                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@firstName", response.firstName);
                    comm.Parameters.AddWithValue("@lastName", response.surname);
                    comm.Parameters.AddWithValue("@email", response.email);
                    comm.Parameters.AddWithValue("@encPassword", response.encPassword);
                    comm.Parameters.AddWithValue("@doj", doj);
                    await dataAdapter.FillAsync(dt);
                }

                return response;
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }

        public async Task<LoginResponseDTO> login(UserDTO user, ITokenService tokenService)
        {
            var encPassword = UserHasherService.encrypt(user.password);
            DataTable dt = new DataTable();
            string userQuery = _config.GetValue<string>("Queries:GetUserQuery");
            UserService userService = new UserService(connStr, userQuery);

            try
            {
                using (MySqlConnection conn = new MySqlConnection(connStr))
                using (MySqlCommand comm = new MySqlCommand(query, conn))
                using (var dataAdapter = new MySqlDataAdapter(comm))
                {
                    await conn.OpenAsync();
                    comm.CommandType = CommandType.Text;
                    comm.Parameters.AddWithValue("@email", user.email);
                    comm.Parameters.AddWithValue("@encPassword", encPassword);
                    await dataAdapter.FillAsync(dt);
                }

                bool hasRows = dt.Rows.GetEnumerator().MoveNext();

                if (hasRows)
                {
                    UserDTO currentUser = await userService.getUser(user);

                    LoginDTOModel _model = new LoginDTOModel()
                    {
                        email = currentUser.email,
                        password = user.password,
                        role = currentUser.userRole
                    };

                    generatedToken = tokenService.BuildToken(_config["Jwt:Key"].ToString(),
                        _config["Jwt:Issuer"].ToString(),
                        _config["Jwt:Audience"].ToString(),
                        _model);

                    LoginResponseDTO response = new LoginResponseDTO();
                    response.expiresIn = (2 * 60 * 60).ToString();
                    response.userId = (int)dt.Rows[0]["userID"];
                    response.userName = dt.Rows[0]["firstName"].ToString();
                    response.userSurname = dt.Rows[0]["lastName"].ToString();
                    response.doj = dt.Rows[0]["doj"].ToString();
                    response.userRole = dt.Rows[0]["userRole"].ToString();
                    response.email = dt.Rows[0]["email"].ToString();
                    response.token = generatedToken;
                    response.success = true;

                    return response;
                }
                else
                {
                    LoginResponseDTO response = new LoginResponseDTO();
                    response.expiresIn = null;
                    response.userName = user.email;
                    response.userRole = string.Empty;
                    response.token = "";
                    response.success = false;

                    return response;
                }
            }
            catch (Exception ex)
            {
                LogExceptionService.Log(ex);
                throw new Exception(ex.Message);
            }
        }
    }
}
