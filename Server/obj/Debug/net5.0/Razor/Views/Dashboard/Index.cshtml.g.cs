#pragma checksum "C:\VS Projects\Angular\pnp\pnp_back-end-full\Server\Views\Dashboard\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "d6e5ad18483e14d27aa420abbc9ffc3b04da6606"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Dashboard_Index), @"mvc.1.0.view", @"/Views/Dashboard/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d6e5ad18483e14d27aa420abbc9ffc3b04da6606", @"/Views/Dashboard/Index.cshtml")]
    public class Views_Dashboard_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("<!DOCTYPE html>\r\n<html>\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("head", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "d6e5ad18483e14d27aa420abbc9ffc3b04da66062728", async() => {
                WriteLiteral(@"
    <meta charset=""UTF-8"">
    <meta http-equiv=""X-UA-Compatible"" content=""IE=edge"">
    <meta name=""viewport"" content=""width=device-width, initial-scale=1.0"">
    <title>RnR Dashboard</title>
    <link href=""https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"" rel=""stylesheet"" integrity=""sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"" crossorigin=""anonymous"">
    <script src=""https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"" integrity=""sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"" crossorigin=""anonymous""></script>
    <script src=""https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js""></script>
    <script src=""https://code.jquery.com/jquery-3.6.0.min.js""
            integrity=""sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="" crossorigin=""anonymous""></script>
");
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("body", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "d6e5ad18483e14d27aa420abbc9ffc3b04da66064622", async() => {
                WriteLiteral(@"
    <nav class=""navbar navbar-expand-lg navbar-dark"" style=""background-color:#B9154A;"">
        <div class=""container-fluid"">
            <a class=""navbar-brand"" href=""#"" style=""padding-left: 65px;"">RNR DASHBOARD</a>
            <button class=""navbar-toggler"" type=""button"" data-bs-toggle=""collapse"" data-bs-target=""#navbarText"" aria-expanded=""false"" aria-label=""Toggle navigation"">
                <span class=""navbar-toggler-icon""></span>
            </button>
            <div class=""collapse navbar-collapse"" id=""navbarText"">
");
                WriteLiteral(@"            </div>
        </div>
    </nav>
    <main>
        <div class=""container"">
            <div class=""row"">
                <div class=""col-9"" style=""width:100%;height:50px;border:1px solid #575757;margin:25px 10px;"">

                </div>
                <div class=""col-4"">
                    <div id=""userchart__area"" class=""card"" style=""width:40rem;padding:1rem;display:none;"">
                        <canvas id=""bar-chart-grouped"" class=""card-img-top""></canvas>
                        <div class=""card-body"">
                            <h5 class=""card-title"">Current Users</h5>
                            <p class=""card-text"">Users logged in over the past year</p>
                        </div>
                    </div>
                </div>
");
                WriteLiteral(@"            </div>
        </div>
    </main>
    <script type=""text/javascript"">const WEEKDAYS = [""Monday"", ""Tuesday"", ""Wednesday"", ""Thursday"", ""Friday"", ""Saturday"", ""Sunday""];
        const MONTHS = [""January"", ""February"", ""March"", ""April"", ""May"", ""June"", ""July"", ""August"", ""September"", ""October"", ""November"", ""December""];
        const COLORS = [""#66a3ff"", ""#80e5ff"", ""#80ffe5"", ""#aa80ff"", ""#ff66ff"", ""#80ff80"", ""#ffff66"", ""#ff8533"", ""#ffc61a"", ""#66cc99"", ""#ff4dff"", ""#ff4d4d""];

        var users = [];
        var products = [];
        var brands = [];
        var userData = [];

        $(document).ready(() => {
            GetData().then(() => {
                createUserData(users);
                createUsersChart();
            }).catch(error => alert(JSON.stringify(error)));
        });

        const getBrandData = () => {
            products.forEach(product => {
                brands.push(product.brand);
            });
            brands = Array.from(new Set(brands));
      ");
                WriteLiteral(@"  }

        const getProductsPromise = new Promise((resolve, reject) => {
            var xhr = new XMLHttpRequest();
            xhr.responseType = ""json"";
            xhr.overrideMimeType(""application/json"");
            xhr.open(""GET"", ""https://localhost:4446/dashboard/getproducts"", true);
            xhr.onload = function () {
                if (this.status == 200) {
                    resolve(xhr.response);
                } else {
                    reject(xhr.response);
                }
            };
            xhr.send();
        });

        const getUsersPromise = new Promise((resolve, reject) => {
            $.ajax({
                url: ""https://localhost:4446/dashboard/getusers"",
                dataType: ""json"",
                type: ""GET"",
                async: true,
                success: function (data) {
                    resolve(data);
                },
                error: function (data, textStatus, jqxhr) {
                    reject(data);
   ");
                WriteLiteral(@"             }
            });
        });

        async function GetData() {
            await getUsersPromise.then((value) => { users = value; });
            await getProductsPromise.then((value) => { products = value }).then(() => {
                getBrandData();
            });
        }

        const createUsersChart = () => {
            new Chart(document.getElementById(""bar-chart-grouped""), {
                type: 'bar',
                data: {
                    labels: MONTHS,
                    datasets: userData
                },
                options: {
                    title: {
                        display: true,
                        text: 'Number Of Users Logged In Past Year'
                    }
                }
            });

            $('#userchart__area').show();
        }

        const createUserData = (users) => {
            users.forEach(user => {
                const _user = {
                    label: user.firstName,
        ");
                WriteLiteral("            backgroundColor: COLORS[Math.floor(Math.random() * 12)],\r\n                    data: [0, 8, 4, 2, 6, 4, 5, 3, 7, 8, 11, 12]\r\n                }\r\n                userData.push(_user);\r\n            });\r\n        }\r\n    </script>\r\n");
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n</html>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
