﻿using System;
using System.IO;
using System.IO.IsolatedStorage;

namespace RnR.Server.Storage
{
    public interface IStorage
    {
        public void write(string jwt);
        public string read();
    }

    public class LocalStorageService : IStorage
    {
        public IsolatedStorageFile isoStore = IsolatedStorageFile.GetStore(IsolatedStorageScope.User | IsolatedStorageScope.Assembly, null, null);

        public void write(string jwt)
        {
            using (IsolatedStorageFileStream isoStream = new IsolatedStorageFileStream("jwt.txt", FileMode.OpenOrCreate, isoStore))
            {
                using (StreamWriter writer = new StreamWriter(isoStream))
                {
                    writer.WriteLine(jwt);
                }
            }

        }

        public string read()
        {
            string result = "";

            using (IsolatedStorageFileStream isoStream = new IsolatedStorageFileStream("jwt.txt", FileMode.Open, isoStore))
            {
                using (StreamReader reader = new StreamReader(isoStream))
                {
                    Console.WriteLine("Reading contents:");
                    result = reader.ReadToEnd();
                }
            }

            return result;
        }
    }

}
