using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using RnR.Security.TokenServices;

namespace RnR.Server
{
    public class Startup
    {
        //private string MyAllowSpecificOrigins = "_mySpecificAllowOrigins";
      
        public IConfiguration _config { get; }

        public Startup(IConfiguration configuration)
        {
            _config = configuration;
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAutoMapper(typeof(Startup));

            // For Authentication
            services.AddTransient<ITokenService, TokenService>();
            services.AddScoped<ITokenService, TokenService>();
            services.AddControllersWithViews().AddSessionStateTempDataProvider();
            services.AddSession();
            services.AddHttpContextAccessor();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = _config["Jwt:Issuer"],
                    ValidAudience = _config["Jwt:Audience"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]))
                };
            });

            //services.AddCors(options =>
            //{
            //    options.AddPolicy(name: MyAllowSpecificOrigins,
            //        builder =>
            //        {
            //            builder.WithOrigins("https://localhost:4200").AllowAnyHeader().AllowAnyMethod();
            //            builder.WithOrigins("http://127.0.0.1:5500").AllowAnyHeader().AllowAnyMethod();
            //            builder.WithOrigins("https://localhost:4446").AllowAnyHeader().AllowAnyMethod();
            //            builder.WithOrigins("http://localhost:4445").AllowAnyHeader().AllowAnyMethod();
            //            builder.WithOrigins("*", "*").AllowAnyHeader().AllowAnyMethod();
            //            builder.WithOrigins("https://ecommerce-front-end-jonathan-j.herokuapp.com").AllowAnyHeader().AllowAnyMethod();
            //            //builder.WithOrigins("*", "*").AllowAnyHeader().AllowAnyMethod();
            //        });
            //});

            services.AddCors(options =>
            {
                options.AddDefaultPolicy(
                    builder =>
                    {
                        builder.WithOrigins("https://localhost:4200").AllowAnyHeader().AllowAnyMethod();
                        builder.WithOrigins("http://127.0.0.1:5500").AllowAnyHeader().AllowAnyMethod();
                        builder.WithOrigins("https://localhost:4446").AllowAnyHeader().AllowAnyMethod();
                        builder.WithOrigins("http://localhost:4445").AllowAnyHeader().AllowAnyMethod();
                        builder.WithOrigins("*", "*").AllowAnyHeader().AllowAnyMethod();
                        builder.WithOrigins("https://ecommerce-front-end-jonathan-j.herokuapp.com").AllowAnyHeader().AllowAnyMethod();
                        //builder.WithOrigins("*", "*").AllowAnyHeader().AllowAnyMethod();
                    });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSession();

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors();

            // JWT Auth Middleware - Add token to header
            app.Use(async (context, next) =>
            {
                var token = context.Session.GetString("Token");

                if (!string.IsNullOrEmpty(token))
                {
                    context.Request.Headers.Add("Authorization", "Bearer " + token);
                }

                await next();
            });

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
