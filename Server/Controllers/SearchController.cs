﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using RnR.Data.DAOs;
using RnR.Data.DTOs;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RnR.Server.Controllers
{
    [ApiController]
    public class SearchController : Controller
    {
        private readonly IConfiguration _config;
        private string connStr = string.Empty;
        private string query = string.Empty;
        private SearchService service;

        public SearchController(IConfiguration config)
        {
            this._config = config;
            connStr = _config.GetValue<string>("ConnectionStrings:DefaultConnection");
        }

        [AllowAnonymous]
        [HttpGet, Route("/api/search/catalogue")]
        [ProducesDefaultResponseType(typeof(IEnumerable<SearchResponseDTO>))]
        public async Task<IActionResult> catalogue()
        {
            query = _config.GetValue<string>("Queries:CatalogueQuery");
            service = new SearchService(connStr, query);

            IEnumerable<SearchResponseDTO> response = await service.Catalogue();
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost, Route("/api/search/searchAll")]
        [ProducesDefaultResponseType(typeof(List<SearchResponseDTO>))]
        public async Task<IActionResult> searchAll(SearchRequestDTO request)
        {
            query = _config.GetValue<string>("Queries:SearchAllQuery");
            service = new SearchService(connStr, query);

            List<SearchResponseDTO> response = await service.SearchAll(request);
            return Ok(response);
        }
    }
}
