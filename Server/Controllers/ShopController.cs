﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using RnR.Data.DAOs;
using RnR.Data.DTOs;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RnR.Server.Controllers
{
    [ApiController]
    public class ShopController : Controller
    {
        private readonly IConfiguration _config;
        private string connStr = string.Empty;
        private string query = string.Empty;
        private ShopService service;

        public ShopController(IConfiguration config)
        {
            this._config = config;
            connStr = _config.GetValue<string>("ConnectionStrings:DefaultConnection");
        }

        [AllowAnonymous]
        [HttpGet, Route("/api/shop/startup")]
        [ProducesDefaultResponseType(typeof(List<SearchResponseDTO>))]
        public async Task<IActionResult> get()
        {
            query = _config.GetValue<string>("Queries:CatalogueQuery");
            service = new ShopService(connStr, query);

            List<SearchResponseDTO> response = await service.onStartup();
            return Ok(response);

        }

        [AllowAnonymous]
        [HttpPost, Route("/api/shop/searchBrand")]
        [ProducesDefaultResponseType(typeof(List<SearchResponseDTO>))]
        public async Task<IActionResult> searchBrand(SearchRequestDTO request)
        {
            query = _config.GetValue<string>("Queries:FilterByBrandQuery");
            service = new ShopService(connStr, query);

            if (request.brand == null)
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                List<SearchResponseDTO> response = await service.filterByBrand(request);
                return Ok(response);
            }
        }

        [AllowAnonymous]
        [HttpPost, Route("/api/shop/searchPrice")]
        [ProducesDefaultResponseType(typeof(List<SearchResponseDTO>))]
        public async Task<IActionResult> searchPrice(SearchRequestDTO request)
        {
            query = _config.GetValue<string>("Queries:FilterByPriceQuery");
            service = new ShopService(connStr, query);

            if (request.minRange == null || request.maxRange == null || string.IsNullOrEmpty(request.category))
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                List<SearchResponseDTO> response = await service.filterByPrice(request);
                return Ok(response);
            }
        }

        [AllowAnonymous]
        [HttpPost, Route("/api/shop/searchQuantity")]
        [ProducesDefaultResponseType(typeof(List<SearchResponseDTO>))]
        public async Task<IActionResult> searchQuantity(SearchRequestDTO request)
        {
            query = _config.GetValue<string>("Queries:FilterByQuantityQuery");
            service = new ShopService(connStr, query);

            if (string.IsNullOrEmpty(request.quantity) || string.IsNullOrEmpty(request.category))
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                List<SearchResponseDTO> response = await service.filterByQuantity(request);
                return Ok(response);
            }
        }
    }
}
