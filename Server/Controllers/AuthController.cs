﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using RnR.Data.DTOs;
using RnR.Server.Auth;
using RnR.Data.Logger;
using RnR.Security.TokenServices;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RnR.Server.Controllers
{
    [ApiController]
    public class AuthController : Controller
    {
        private readonly IConfiguration _config;
        private string connStr = string.Empty;
        private string query = string.Empty;
        private LogService logger;
        private AuthService service;
        private readonly ITokenService tokenService;

        public AuthController(IConfiguration config, ITokenService _tokenService)
        {
            this._config = config;
            connStr = _config.GetValue<string>("ConnectionStrings:DefaultConnection");
            logger = new LogService();
            tokenService = _tokenService;
        }

        [AllowAnonymous]
        [HttpPost, Route("/api/auth/signup")]
        [ProducesDefaultResponseType(typeof(SignupResponseDTO))]
        public async Task<IActionResult> signup(UserDTO user)
        {
            if (user.firstName == string.Empty
                || user.password == string.Empty
                || user.userRole == string.Empty
                || user.email == string.Empty)
            {
                logger.LogWrite(string.Format("Email: {0} failed to signup", arg0: user.email));
                return BadRequest("Invalid Request");
            }
            else
            {
                query = _config.GetValue<string>("Queries:SignupQuery");
                service = new AuthService(_config, connStr, query);
                SignupResponseDTO response = await service.signup(user);

                if (response.success)
                {
                    logger.LogWrite(string.Format("Email: {0} successfully signed up", arg0: user.email));
                    return Ok(response);
                }
                else
                {
                    logger.LogWrite(string.Format("Error While Signing up User Email: {0}", arg0: user.email));
                    return StatusCode(406);
                }
            }
        }

        [AllowAnonymous]
        [HttpPost, Route("/api/auth/login")]
        [ProducesDefaultResponseType(typeof(LoginResponseDTO))]
        public async Task<IActionResult> login(UserDTO user)
        {
            if (user.password == null || user.email == null)
            {
                logger.LogWrite(string.Format("Email: {0} attempted to login - UserName Or Email Not Present", arg0: user.email));
                return BadRequest("Invalid Request");
            }
            else
            {
                query = _config.GetValue<string>("Queries:LoginQuery");
                service = new AuthService(_config, connStr, query);
                LoginResponseDTO response = await service.login(user, tokenService);

                if (response.success)
                {
                    logger.LogWrite(string.Format("Email: {0} Role: {1} successfully logged in", arg0: response.email, arg1: response.userRole));
                    HttpContext.Session.SetString("Token", response.token);
                    return Ok(response);
                }
                else
                {
                    logger.LogWrite(string.Format("Email: {0} failed to log in", arg0: user.email));
                    return Ok(response);
                }
            }
        }
    }
}