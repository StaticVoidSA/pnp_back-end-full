﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using RnR.Data.DAOs;
using RnR.Data.DTOs;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Server.Controllers
{
    [ApiController]
    public class DashboardController : Controller
    {
        private readonly IConfiguration _config;
        private string connStr = string.Empty;
        private string query = string.Empty;
        private UserService userService;
        private ProductsService productService;

        public DashboardController(IConfiguration config)
        {
            this._config = config;
            connStr = _config.GetValue<string>("ConnectionStrings:DefaultConnection");
        }

        [AllowAnonymous]
        [HttpGet, Route("/dashboard/index")]
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpGet, Route("/dashboard/getusers")]
        [ProducesDefaultResponseType(typeof(List<UserDTO>))]
        public async Task<List<UserDTO>> GetAllUsers()
        {
            query = _config.GetValue<string>("Queries:GetUsers");
            userService = new UserService(connStr, query);
            List<UserDTO> response = await userService.getUsers();
            return response;
        }

        [AllowAnonymous]
        [HttpGet, Route("/dashboard/getproducts")]
        public async Task<List<SearchResponseDTO>> GetAllProducts()
        {
            query = _config.GetValue<string>("Queries:ProductsQuery");
            productService = new ProductsService(connStr, query);
            List<SearchResponseDTO> response = await productService.getProducts();
            return response;
        }
    }
}

/*
 * remotemysql.com
 * Username: rCi8OTH6kH

Database name: rCi8OTH6kH

Password: jQoZZLNlfq

Server: remotemysql.com

Port: 3306
 * */