﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using RnR.Data.DAOs;
using RnR.Data.DTOs;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RnR.Server.Controllers
{
    [ApiController]
    public class UsersController : Controller
    {
        private readonly IConfiguration _config;
        private string connStr = string.Empty;
        private string query = string.Empty;
        private UserService service;

        public UsersController(IConfiguration config)
        {
            this._config = config;
            connStr = _config.GetValue<string>("ConnectionStrings:DefaultConnection");
        }

        [Authorize]
        [HttpGet, Route("/api/admin/getUsers")]
        [ProducesDefaultResponseType(typeof(List<UserDTO>))]
        public async Task<IActionResult> getUsers()
        {
            query = _config.GetValue<string>("Queries:GetUsers");
            service = new UserService(connStr, query);
            List<UserDTO> response = await service.getUsers();
            return Ok(response);
        }

        [Authorize]
        [HttpPost, Route("/api/admin/getUser")]
        [ProducesDefaultResponseType(typeof(UserDTO))]
        public async Task<IActionResult> getUser(UserDTO user)
        {
            query = _config.GetValue<string>("Queries:GetUserQuery");
            service = new UserService(connStr, query);
            UserDTO response = await service.getUser(user);
            return Ok(response);
        }

        [Authorize]
        [HttpPost, Route("/api/admin/getUserByID")]
        [ProducesDefaultResponseType(typeof(UserDTO))]
        public async Task<IActionResult> getUserByID(int userID)
        {
            query = _config.GetValue<string>("Queries:GetUserByIDQuery");
            service = new UserService(connStr, query);
            UserDTO response = await service.getUserByID(userID);
            return Ok(response);
        }

        [Authorize]
        [HttpPost, Route("/api/admin/deleteUser")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> deleteUser(UserDTO user)
        {
            query = _config.GetValue<string>("Queries:DeleteUserQuery");
            service = new UserService(connStr, query);
            bool response = await service.deleteUser(user);
            return Ok(response);
        }

        [Authorize]
        [HttpPut, Route("/api/admin/updateUser")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> updateUser(UserUpdateDTO user)
        {
            query = _config.GetValue<string>("Queries:StartupQuery");
            service = new UserService(connStr, query);
            bool response = await service.updateUser(user);
            return Ok(response);
        }
    }
}
