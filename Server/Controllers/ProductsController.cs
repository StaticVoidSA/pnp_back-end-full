﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using RnR.Data.DAOs;
using RnR.Data.DTOs;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RnR.Server.Controllers
{
    [ApiController]
    public class ProductsController : Controller
    {
        private readonly IConfiguration _config;
        private string connStr = string.Empty;
        private string query = string.Empty;
        private ProductsService service;

        public ProductsController(IConfiguration config)
        {
            this._config = config;
            connStr = _config.GetValue<string>("ConnectionStrings:DefaultConnection");
        }
        [AllowAnonymous]
        [HttpGet, Route("/api/products/getProducts")]
        [ProducesDefaultResponseType(typeof(List<SearchResponseDTO>))]
        public async Task<IActionResult> getProducts()
        {
            query = _config.GetValue<string>("Queries:ProductsQuery");
            service = new ProductsService(connStr, query);
            List<SearchResponseDTO> response = await service.getProducts();
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost, Route("/api/products/getProduct")]
        [ProducesDefaultResponseType(typeof(ProductDTO))]
        public async Task<IActionResult> getProduct(ProductDTO product)
        {
            query = _config.GetValue<string>("Queries:GetProductsQuery");
            service = new ProductsService(connStr, query);
            ProductDTO response = await service.getProduct(product);
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPut, Route("/api/products/updateProduct")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> updateProduct(ProductUpdateDTO product)
        {
            query = _config.GetValue<string>("Queries:UpdateProductQuery");
            service = new ProductsService(connStr, query);
            bool response = await service.editProduct(product);
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost, Route("/api/products/deleteProduct")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> deleteProduct(ProductDTO product)
        {
            query = _config.GetValue<string>("Queries:DeleteProductQuery");
            service = new ProductsService(connStr, query);
            bool response = await service.deleteProduct(product);
            return Ok(response);
        }
    }
}
