﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using RnR.Data.DAOs;
using RnR.Data.DTOs;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RnR.Server.Controllers
{
    [ApiController]
    public class DeliveriesController : Controller
    {
        private readonly IConfiguration _config;
        private string connStr = string.Empty;
        private string query = string.Empty;
        private DeliveriesService service;

        public DeliveriesController(IConfiguration config)
        {
            this._config = config;
            connStr = _config.GetValue<string>("ConnectionStrings:DefaultConnection");
            query = _config.GetValue<string>("Queries:GetDeliveries");
            service = new DeliveriesService(connStr, query);
        }

        [Authorize]
        [HttpGet, Route("/api/deliveries/getDeliveries")]
        [ProducesDefaultResponseType(typeof(IActionResult))]
        public async Task<IActionResult> getDeliveries(int userID)
        {
            if (userID <= 0)
            {
                return BadRequest("Bad Request");
            }

            List<DeliveryItemDTO> response = await service.getDeliveries(userID);

            return Ok(response);
        }
    }

}
