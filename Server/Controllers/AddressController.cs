﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using RnR.Data.DAOs;
using RnR.Data.DTOs;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RnR.Server.Controllers
{
    [ApiController]
    public class AddressController : Controller
    {
        private readonly IConfiguration _config;
        private string connStr = string.Empty;
        private string query = string.Empty;
        private AddressService service;

        public AddressController(IConfiguration config)
        {
            this._config = config;
            connStr = _config.GetValue<string>("ConnectionStrings:DefaultConnection");
        }

        [Authorize]
        [HttpPost, Route("/api/address/createAddress")]
        [ProducesDefaultResponseType(typeof(string))]
        public async Task<IActionResult> createAddress(AddressDTO address)
        {
            query = _config.GetValue<string>("Queries:addAddressQuery");
            service = new AddressService(connStr, query);

            if (address.addressID <= 0
                || address.userAddress == string.Empty
                || address.addressNickName == string.Empty)
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                string response = await service.createAddress(address);
                return Ok(response);
            }
        }

        [Authorize]
        [HttpGet, Route("/api/address/getAddresses")]
        [ProducesDefaultResponseType(typeof(List<AddressDTO>))]
        public async Task<IActionResult> getAddresses(int userID)
        {
            query = _config.GetValue<string>("Queries:getAddressesQuery");
            service = new AddressService(connStr, query);

            if (userID <= 0)
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                List<AddressDTO> response = await service.getAddresses(userID);
                return Ok(response);
            }
        }

        [Authorize]
        [HttpDelete, Route("/api/address/deleteAddress")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> deleteAddress(int addressID, int userID)
        {
            query = _config.GetValue<string>("Queries:deleteAddressQuery");
            service = new AddressService(connStr, query);

            if (addressID <= 0 || userID <= 0)
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                bool response = await service.deleteAddress(addressID, userID);
                return Ok(response);
            }
        }

        [Authorize]
        [HttpPut, Route("/api/address/updateAddress")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> updateAddress(AddressDTO address)
        {
            query = _config.GetValue<string>("Queries:updateAddressQuery");
            service = new AddressService(connStr, query);

            if (address.addressID <= 0
                || address.ID <= 0
                || address.userAddress == string.Empty
                || address.addressNickName == string.Empty)
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                bool response = await service.updateAddress(address);
                return Ok(response);
            }
        }
    }
}
