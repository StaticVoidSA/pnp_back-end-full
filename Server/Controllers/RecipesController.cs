﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using RnR.Data.DAOs;
using RnR.Data.DTOs;
using RnR.Data.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RnR.Server.Controllers
{
    [ApiController]
    public class RecipesController : Controller
    {
        private readonly IConfiguration _config;
        private string connStr = string.Empty;
        private string query = string.Empty;
        private RecipesService service;

        public RecipesController(IConfiguration config)
        {
            this._config = config;
            connStr = _config.GetValue<string>("ConnectionStrings:DefaultConnection");
        }

        [AllowAnonymous]
        [HttpGet, Route("/api/recipes/startupRecipes")]
        [ProducesDefaultResponseType(typeof(List<RecipeDTO>))]
        public async Task<IActionResult> startupRecipes()
        {
            query = _config.GetValue<string>("Queries:GetAllRecipesQuery");
            service = new RecipesService(connStr, query);

            List<RecipeDTO> response = await service.startupRecipes();
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpGet, Route("/api/recipes/getRecipe")]
        [ProducesDefaultResponseType(typeof(List<RecipeCompleteDTO>))]
        public async Task<IActionResult> getRecipe(int recID)
        {
            query = _config.GetValue<string>("Queries:GetRecipeQuery");
            service = new RecipesService(connStr, query);

            List<RecipeCompleteDTO> response = await service.getRecipe(recID);
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpGet, Route("/api/recipes/getRecipes")]
        [ProducesDefaultResponseType(typeof(List<RecipeCompleteDTO>))]
        public async Task<IActionResult> getRecipes()
        {
            query = _config.GetValue<string>("Queries:GetRecipesQuery");
            service = new RecipesService(connStr, query);

            List<RecipeCompleteDTO> response = await service.getAllRecipes();
            return Ok(response);
        }

        [Authorize]
        [HttpPost, Route("/api/recipes/addIngredientsToCart")]
        [ProducesDefaultResponseType(typeof(int))]
        public async Task<IActionResult> addIngredientsToCart(List<CartDTO> items)
        {
            if (items.Count <= 0)
            {
                return BadRequest("Bad Request! No items to add to cart");
            }
            else
            {
                query = _config.GetValue<string>("Queries:AddCartQuery");
                service = new RecipesService(connStr, query);

                int response = await service.addIngredientsToCart(items);
                return Ok(response);
            }
        }

        [Authorize]
        [HttpPost, Route("/api/recipes/addIngredientsToFavorites")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> addIngredientsToFavorites(List<FavoritesDTO> items)
        {
            bool response = false;
            query = _config.GetValue<string>("Queries:AddToFavoritesQuery");

            if (items.Count <= 0)
            {
                return BadRequest(string.Format("{0}: Bad Request! No items to add to cart", arg0: response));
            }
            else
            {
                service = new RecipesService(connStr, query);
                response = await service.addIngredientsToFavorites(items);
                return Ok(response);
            }
        }
    }
}
