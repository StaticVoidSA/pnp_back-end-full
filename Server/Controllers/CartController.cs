﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using RnR.Data.DAOs;
using RnR.Data.DTOs;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RnR.Server.Controllers
{
    [ApiController]
    public class CartController : Controller
    {
        private readonly IConfiguration _config;
        private string connStr = string.Empty;
        private string query = string.Empty;
        private CartService service;
        private PaidItemsService paidService;

        public CartController(IConfiguration config)
        {
            this._config = config;
            connStr = _config.GetValue<string>("ConnectionStrings:DefaultConnection");
        }

        [Authorize]
        [HttpGet, Route("/api/cart/getItems")]
        [ProducesDefaultResponseType(typeof(List<CartItemDTO>))]
        public async Task<IActionResult> getItems(int userID)
        {
            query = _config.GetValue<string>("Queries:GetCartItems");
            service = new CartService(connStr, query);

            if (userID < 0)
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                List<CartItemDTO> results = await service.getCartItems(userID);

                return Ok(results);
            }
        }

        [Authorize]
        [HttpPost, Route("/api/cart/addToCart")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> addToCart(CartDTO item)
        {
            query = _config.GetValue<string>("Queries:AddCartQuery");
            service = new CartService(connStr, query);

            bool result = await service.AddToCart(item);

            return Ok(result);
        }

        //[Authorize]
        [HttpDelete, Route("/api/cart/removeFromCart")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> removeFromCart(int cartItemID, int cartID)
        {
            query = _config.GetValue<string>("Queries:RemoveFromCart");
            service = new CartService(connStr, query);

            if (cartItemID < 0 || cartID < 0)
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                bool result = await service.RemoveFromCart(cartItemID, cartID);
                return Ok(result);
            }
        }

        [Authorize]
        [HttpGet, Route("/api/cart/cartCount")]
        [ProducesDefaultResponseType(typeof(int))]
        public async Task<IActionResult> cartCount(int userID)
        {
            query = _config.GetValue<string>("Queries:GetCartItems");
            service = new CartService(connStr, query);

            int result = await service.cartCount(userID);

            return Ok(result);
        }

        [Authorize]
        [HttpPut, Route("/api/cart/updateCartItem")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> updateCartItem(CartItemUpdateDTO item)
        {
            query = _config.GetValue<string>("Queries:UpdateCartItem");
            service = new CartService(connStr, query);

            if (item.userID < 0 || item.cartItemID < 0)
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                bool result = await service.updateCartItem(item);

                return Ok(result);
            }
        }

        [Authorize]
        [HttpDelete, Route("/api/cart/clearCartItems")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> removeItems(int userID)
        {
            query = _config.GetValue<string>("Queries:ClearCart");
            service = new CartService(connStr, query);

            if (userID <= 0)
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                bool result = await service.removeAllItems(userID);

                if (!result)
                {
                    return BadRequest("An Error Occurred");
                }

                return Ok(result);
            }
        }

        [Authorize]
        [HttpPost, Route("/api/cart/addToPaidItems")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> addToPaidItems(CartDTO item)
        {
            query = _config.GetValue<string>("Queries:AddPaidFor");
            service = new CartService(connStr, query);
            string clearCartQuery = _config.GetValue<string>("Queries:AddPaidFor");
            paidService = new PaidItemsService(connStr, clearCartQuery);

            bool result = await paidService.AddToPaidFor(item);

            if (!result)
            {
                return BadRequest("An Error Occurred");
            }

            
            return Ok(result);
        }
    }
}
