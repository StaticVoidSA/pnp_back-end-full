﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using RnR.Data.DAOs;
using RnR.Data.DTOs;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RnR.Server.Controllers
{
    [ApiController]
    public class FavoritesController : Controller
    {
        private readonly IConfiguration _config;
        private string connStr = string.Empty;
        private string query = string.Empty;
        private FavoritesService service;

        public FavoritesController(IConfiguration config)
        {
            this._config = config;
            connStr = _config.GetValue<string>("ConnectionStrings:DefaultConnection");
        }

        [Authorize]
        [HttpGet, Route("/api/favorites/getfavorites")]
        [ProducesDefaultResponseType(typeof(List<FavoritesDTO>))]
        public async Task<IActionResult> getFavorites(int userID)
        {
            query = _config.GetValue<string>("Queries:GetFavoritesQuery");
            service = new FavoritesService(connStr, query);

            if (userID < 0)
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                List<FavoritesDTO> response = await service.getFavorites(userID);
                return Ok(response);
            }
        }

        [Authorize]
        [HttpPost, Route("/api/favorites/addToFavorites")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> addToFavorites(FavoritesDTO item)
        {
            query = _config.GetValue<string>("Queries:AddToFavoritesQuery");
            service = new FavoritesService(connStr, query);

            if (item.userID < 0 || item.ID < 0)
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                bool result = await service.addToFavorites(item);
                return Ok(result);
            }
        }

        [Authorize]
        [HttpDelete, Route("/api/favorites/removeFromFavorites")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> removeFromFavorites(int ID, int favID)
        {
            query = _config.GetValue<string>("Queries:RemoveFromFavoritesQuery");
            service = new FavoritesService(connStr, query);

            if (ID < 0 || favID < 0)
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                bool result = await service.removeFromFavorites(ID, favID);
                return Ok(result);
            }
        }

        [Authorize]
        [HttpPost, Route("/api/favorites/addToCart")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> addToCart(CartDTO item)
        {
            query = _config.GetValue<string>("Queries:AddToCartQuery");
            service = new FavoritesService(connStr, query);

            if (item.userID < 0 || item.cartID < 0)
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                bool response = await service.addToCart(item);
                return Ok(response);
            }
        }
    }
}
