﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using RnR.Data.DAOs;
using RnR.Data.DTOs;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RnR.Server.Controllers
{
    [ApiController]
    public class CollectionsController : Controller
    {
        private readonly IConfiguration _config;
        private string connStr = string.Empty;
        private string query = string.Empty;
        private CollectionService service;

        public CollectionsController(IConfiguration config)
        {
            this._config = config;
            connStr = _config.GetValue<string>("ConnectionStrings:DefaultConnection");
            query = _config.GetValue<string>("Queries:GetCollections");
            service = new CollectionService(connStr, query);
        }

        [Authorize]
        [HttpGet, Route("/api/collections/getCollections")]
        [ProducesDefaultResponseType(typeof(IActionResult))]
        public async Task<IActionResult> getCollections(int userID)
        {
            if (userID <= 0)
            {
                return BadRequest("Invalid Request");
            }

            List<CollectionItemDTO> response = await service.getCollections(userID);

            return Ok(response);
        }
    }
}
