﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using RnR.Data.DAOs;
using RnR.Data.DTOs;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RnR.Server.Controllers
{
    [ApiController]
    public class SpecialsController : Controller
    {
        private readonly IConfiguration _config;
        private string connStr = string.Empty;
        private string query = string.Empty;
        private SpecialsService service;

        public SpecialsController(IConfiguration config)
        {
            this._config = config;
            connStr = _config.GetValue<string>("ConnectionStrings:DefaultConnection");
            query = _config.GetValue<string>("Queries:GetSpecials");
            service = new SpecialsService(connStr, query);
        }

        [AllowAnonymous]
        [HttpGet, Route("/api/specials/getSpecials")]
        [ProducesDefaultResponseType(typeof(List<SpecialsDTO>))]
        public async Task<IActionResult> getSpecials(string category)
        {
            if (category == null)
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                List<SpecialsDTO> response = await service.getSpecials(category);
                return Ok(response);
            }
        }
    }

}
