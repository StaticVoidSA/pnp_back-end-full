﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using RnR.Data.DAOs;
using RnR.Data.DTOs;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RnR.Server.Controllers
{
    [ApiController]
    public class ShoppingListsController : Controller
    {
        private readonly IConfiguration _config;
        private string connStr = string.Empty;
        private string query = string.Empty;
        private ShoppingListService service;

        public ShoppingListsController(IConfiguration config)
        {
            this._config = config;
            connStr = _config.GetValue<string>("ConnectionStrings:DefaultConnection");
        }

        [Authorize]
        [HttpPost, Route("/api/shoppingLists/createShoppingList")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> createShoppingList(ShoppingListDTO list)
        {
            query = _config.GetValue<string>("Queries:CreateShoppingListQuery");
            service = new ShoppingListService(connStr, query);

            if (list.shoppingListName == null)
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                bool result = await service.CreateList(list);
                return Ok(result);
            }
        }

        [Authorize]
        [HttpGet, Route("/api/shoppingLists/getUserShoppingList")]
        [ProducesDefaultResponseType(typeof(List<ListsDTO>))]
        public async Task<IActionResult> getUserShoppingList(int userID)
        {
            query = _config.GetValue<string>("Queries:GetUserShoppingLists");
            service = new ShoppingListService(connStr, query);

            List<ListsDTO> response = await service.GetUserLists(userID);
            return Ok(response);
        }

        [Authorize]
        [HttpDelete, Route("/api/shoppingLists/deleteShoppingList")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> deleteShoppingList(int itemID, int userID)
        {
            query = _config.GetValue<string>("Queries:DeleteShoppingListQuery");
            service = new ShoppingListService(connStr, query);

            bool result = await service.DeleteShoppingList(itemID, userID);
            return Ok(result);
        }

        [Authorize]
        [HttpPost, Route("/api/shoppingLists/addToShoppingList")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> addToShoppingList(ShoppingListDTO list)
        {
            query = _config.GetValue<string>("Queries:AddToShoppingListQuery");
            service = new ShoppingListService(connStr, query);

            bool result = await service.AddToShoppingList(list);
            return Ok(result);
        }

        [HttpGet, Route("/api/shoppingLists/getListItems")]
        [ProducesDefaultResponseType(typeof(List<ShoppingListDTO>))]
        public async Task<IActionResult> getListItems(int userID, string listName)
        {
            query = _config.GetValue<string>("Queries:GetListItems");
            service = new ShoppingListService(connStr, query);

            List<ShoppingListDTO> response = await service.GetListItems(userID, listName);
            return Ok(response);
        }

        [Authorize]
        [HttpDelete, Route("/api/shoppingLists/deleteFromList")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> deleteFromList(int listID, int ID)
        {
            query = _config.GetValue<string>("Queries:DeleteFromShoppingListQuery");
            service = new ShoppingListService(connStr, query);

            bool result = await service.DeleteFromShoppingList(listID, ID);
            return Ok(result);
        }

        [Authorize]
        [HttpGet, Route("/api/shoppingLists/getUserDetails")]
        [ProducesDefaultResponseType(typeof(UserDTO))]
        public async Task<IActionResult> getuserDetails(int userID)
        {
            query = _config.GetValue<string>("Queries:ShoppingListGetUserDetailsQuery");
            service = new ShoppingListService(connStr, query);

            if (userID < 0)
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                UserDTO response = await service.GetUserDetails(userID);
                return Ok(response);
            }
        }

        [Authorize]
        [HttpGet, Route("/api/shoppingLists/getListCount")]
        [ProducesDefaultResponseType(typeof(int))]
        public async Task<IActionResult> getListCount(int userID)
        {
            query = _config.GetValue<string>("Queries:GetListsCountQuery");
            service = new ShoppingListService(connStr, query);

            if (userID < 0)
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                int response = await service.GetListsCount(userID);
                return Ok(response);
            }
        }
    }

}
