﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using RnR.Data.DAOs;
using RnR.Data.DTOs;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RnR.Server.Controllers
{
    [ApiController]
    public class ResetPasswordController : Controller
    {
        private readonly IConfiguration _config;
        private string connStr = string.Empty;
        private string query = string.Empty;
        private ResetService service;

        public ResetPasswordController(IConfiguration config)
        {
            this._config = config;
            connStr = _config.GetValue<string>("ConnectionStrings:DefaultConnection");
        }

        [AllowAnonymous]
        [HttpPost, Route("/api/resetpassword/reset")]
        [ProducesDefaultResponseType(typeof(ResetResponseDTO))]
        public async Task<IActionResult> reset(UserDTO user)
        {
            query = _config.GetValue<string>("Queries:ResetPasswordQuery");
            service = new ResetService(connStr, query);

            if (user.userID < 0)
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                ResetResponseDTO response = await service.resetPassword(user);
                return Ok(response);
            }
        }

        [AllowAnonymous]
        [HttpPost, Route("/api/resetpassword/complete")]
        [ProducesDefaultResponseType(typeof(CompleteResponseDTO))]
        public async Task<IActionResult> complete(CompleteResetDTO user)
        {
            query = _config.GetValue<string>("Queries:CompleteResetPasswordQuery");
            service = new ResetService(connStr, query);

            if (user.email == null || user.password == null)
            {
                return BadRequest("Invalid Request");
            }
            else
            {
                CompleteResponseDTO response = await service.complete(user);
                return Ok(response);
            }
        }
    }
}
