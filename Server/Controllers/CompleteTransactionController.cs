﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using RnR.Data.DAOs;
using RnR.Data.DTOs;
using RnR.Services.MailService;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RnR.Server.Controllers
{
    [ApiController]
    public class CompleteTransactionController : Controller
    {
        private readonly IConfiguration _config;
        private string connStr = string.Empty;
        private string query = string.Empty;
        private CompleteTransactionService service;
        private SendEmailService emailService;

        public CompleteTransactionController(IConfiguration config)
        {
            this._config = config;
            connStr = _config.GetValue<string>("ConnectionStrings:DefaultConnection");

            service = new CompleteTransactionService(connStr, query);
            emailService = new SendEmailService();
        }

        [Authorize]
        [HttpPost, Route("/api/completeTransaction/complete")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> CompleteTransaction(CompleteTransactionDTO transaction)
        {
            bool response = false;

            if (transaction.userID < 0 || transaction.userEmail == null)
            {
                return BadRequest("Bad Request");
            }

            switch (transaction.userSelection.ToString())
            {
                case "Collection":
                    query = _config.GetValue<string>("Queries:AddCollection");
                    service = new CompleteTransactionService(connStr, query);
                    response = await service.CompleteCollection(transaction);
                    break;
                case "Delivery":
                    query = _config.GetValue<string>("Queries:AddDelivery");
                    service = new CompleteTransactionService(connStr, query);
                    response = await service.CompleteDelivery(transaction);
                    break;
                default:
                    return BadRequest("Bad Request");
            }

            bool emailResponse = await emailService.sendReceiptEmail(transaction.userEmail, transaction);

            if (emailResponse)
            {
                return Ok(response);
            }
            else
            {
                return BadRequest("Unable to send email to user");
            }
        }
    }
}
