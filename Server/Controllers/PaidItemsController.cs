﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using RnR.Data.DAOs;
using RnR.Data.DTOs;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RnR.Server.Controllers
{
    [ApiController]
    public class PaidItemsController : Controller
    {
        private readonly IConfiguration _config;
        private string connStr = string.Empty;
        private string query = string.Empty;
        private PaidItemsService service;

        public PaidItemsController(IConfiguration config)
        {
            this._config = config;
            connStr = _config.GetValue<string>("ConnectionStrings:DefaultConnection");
        }

        [Authorize]
        [HttpGet, Route("/api/paidItems/getItems")]
        [ProducesDefaultResponseType(typeof(List<CartDTO>))]
        public async Task<IActionResult> getItems(int userID)
        {
            query = _config.GetValue<string>("Queries:GetPaidFor");
            service = new PaidItemsService(connStr, query);

            if (userID <= 0)
            {
                return BadRequest("Invalid request sent");
            }
            else
            {
                List<CartDTO> results = await service.GetPaidFor(userID);
                return Ok(results);
            }
        }

        [Authorize]
        [HttpDelete, Route("/api/paidItems/clearAll")]
        [ProducesDefaultResponseType(typeof(bool))]
        public async Task<IActionResult> clearAll(int userID)
        {
            if (userID <= 0)
            {
                return BadRequest("Invalid request sent");
            }
            else
            {
                query = _config.GetValue<string>("Queries:ClearPaidFor");
                service = new PaidItemsService(connStr, query);

                bool response = await service.ClearPaidItems(userID);
                return Ok(response);
            }
        }
    }
}
