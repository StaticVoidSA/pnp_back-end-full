﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RnR.Admin.Entities
{
    [Table("ShoppingLists", Schema = "PnPDB")]
    public class ShoppingLists
    {
        private int _ID;
        private string _listName;
        private int _userListID;

        public ICollection<ShoppingListItems> ShoppingListItems { get; set; }

        [Key]
        [Required(ErrorMessage = "ID is required")]
        [Column("ID")]
        public int ID
        {
            get
            {
                return _ID;
            }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("Invalid shopping list ID");
                }

                _ID = value;
            }
        }

        [Required(ErrorMessage = "List Name is required")]
        [Column("listName")]
        public string listName
        {
            get
            {
                return _listName;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid shopping list ID");
                }

                _listName = value;
            }
        }

        [ForeignKey("userListID")]
        [Required(ErrorMessage = "ID is required")]
        [Column("userListID")]
        public int userListID
        {
            get
            {
                return _userListID;
            }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("Invalid shopping list user ID");
                }

                _userListID = value;
            }
        }
    }
}
