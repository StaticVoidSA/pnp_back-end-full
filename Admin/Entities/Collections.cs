﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RnR.Admin.Entities
{
    [Table("Collections", Schema="PnPDB")]
    public class Collections
    {
        private int _id;
        private int _collectionID;
        private int _userID;
        private string _userName;
        private string _selectedShop;
        private DateTime _selectedCollectionDate;
        private DateTime _transactionDate;

        [Key]
        [Required(ErrorMessage = "Collection User ID is required")]
        [Column("id")]
        public int id
        {
            get
            {
                return _id;
            }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("Invalid ID");
                }

                _id = value;
            }
        }

        [ForeignKey("collectionID")]
        [Required(ErrorMessage = "Collection ID is required")]
        [Column("collectionID")]
        public int collectionID
        {
            get
            {
                return _collectionID;
            }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("Invalid collection ID");
                }

                _collectionID = value;
            }
        }

        [Required(ErrorMessage = "Customer ID is required")]
        [Column("userID")]
        public int userID
        {
            get
            {
                return _userID;
            }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("Invalid user ID");
                }

                _userID = value;
            }
        }

        [Required(ErrorMessage = "Customer Name is required")]
        [Column("userName"), DataType(DataType.Text), MaxLength(100)]
        public string userName
        {
            get
            {
                return _userName;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid user name");
                }

                _userName = value;
            }
        }

        [Required(ErrorMessage = "Selected Shop is required")]
        [Column("selectedShop"), DataType(DataType.Text), MaxLength(100)]
        public string selectedShot
        {
            get
            {
                return _selectedShop;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid selected shop");
                }

                _selectedShop = value;
            }
        }

        [Required(ErrorMessage = "Collection Date is required")]
        [Column("selectedCollectionDate"), DataType(DataType.Date)]
        public DateTime selectedCollectionDate
        {
            get
            {
                return _selectedCollectionDate;
            }
            set
            {
                _selectedCollectionDate = value;
            }
        }

        [Required(ErrorMessage = "Transaction Date is required")]
        [Column("transactionDate"), DataType(DataType.Date)]
        public DateTime transactionDate
        {
            get
            {
                return _transactionDate;
            }
            set
            {
                _transactionDate = value;
            }
        }
    }
}
