﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RnR.Admin.Entities
{
    public class Deliveries
    {
        private int _id;
        private int _deliveryID;
        private int _userID;
        private string _userName;
        private string _selectedAddress;
        private DateTime _deliveryDate;

        [Key]
        [Required(ErrorMessage = "Delivery User ID is required")]
        [Column("id")]
        public int id
        {
            get
            {
                return _id;
            }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("Invalid ID");
                }

                _id = value;
            }
        }

        [ForeignKey("deliveryID")]
        [Required(ErrorMessage = "Delivery ID is required")]
        [Column("deliveryID")]
        public int deliveryID
        {
            get
            {
                return _deliveryID;
            }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("Invalid delivery ID");
                }

                _deliveryID = value;
            }
        }

        [Required(ErrorMessage = "Customer ID is required")]
        [Column("userID")]
        public int userID
        {
            get
            {
                return _userID;
            }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("Invalid user ID");
                }

                _userID = value;
            }
        }

        [Required(ErrorMessage = "Customer Name is required")]
        [Column("userName"), DataType(DataType.Text), MaxLength(100)]
        public string userName
        {
            get
            {
                return _userName;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid user name");
                }

                _userName = value;
            }
        }

        [Required(ErrorMessage = "Delivery Address is required")]
        [Column("selectedAddress"), DataType(DataType.Text), MaxLength(100)]
        public string selectedAddress
        {
            get
            {
                return _selectedAddress;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid selected address");
                }

                _selectedAddress = value;
            }
        }

        [Required(ErrorMessage = "Delivery Date is required")]
        [Column("deliveryDate"), DataType(DataType.Date)]
        public DateTime deliveryDate
        {
            get
            {
                return _deliveryDate;
            }
            set
            {
                _deliveryDate = value;
            }
        }
    }
}
