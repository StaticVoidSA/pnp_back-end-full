﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RnR.Admin.Entities
{
    [Table("ShoppingListItems", Schema = "PnPDB")]
    public class ShoppingListItems
    {
        private int _ID;
        private int _listID;
        private string _productTitle;
        private string _brand;
        private string _barcode;
        private int _quantity;
        private double _price;
        private string _listName;

        [Key]
        [Required(ErrorMessage = "ID is required")]
        [Column("ID")]
        public int ID
        {
            get
            {
                return _ID;
            }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("Invalid ID");
                }

                _ID = value;
            }
        }

        [ForeignKey("listID")]
        [Required(ErrorMessage = "List ID is required")]
        [Column("listID")]
        public int listID
        {
            get
            {
                return _listID;
            }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("Invalid shopping list ID");
                }

                _listID = value;
            }
        }

        [Required(ErrorMessage = "Product Title is required")]
        [Column("productTitle"), DataType(DataType.Text), MaxLength(1000)]
        public string productTitle
        {
            get
            {
                return _productTitle;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid product title");
                }

                _productTitle = value;
            }
        }

        [Required(ErrorMessage = "Product Brand is required")]
        [Column("brand"), DataType(DataType.Text), MaxLength(1000)]
        public string brand
        {
            get
            {
                return _brand;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid product brand");
                }

                _brand = value;
            }
        }

        [Required(ErrorMessage = "Product Barcode is required")]
        [Column("barcode"), DataType(DataType.Text), MaxLength(1000)]
        public string barcode
        {
            get
            {
                return _barcode;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid product barcode");
                }

                _barcode = value;
            }
        }

        [Required(ErrorMessage = "Product Quantity is required")]
        [Column("quantity")]
        public int quantity
        {
            get
            {
                return _quantity;
            }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("Invalid product quantity");
                }

                _quantity = value;
            }
        }

        [Required(ErrorMessage = "Product Price is required")]
        [Column("price")]
        public double price
        {
            get
            {
                return _price;
            }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("Invalid product price");
                }

                _price = value;
            }
        }

        [Required(ErrorMessage = "List Name is required")]
        [Column("listName"), DataType(DataType.Text), MaxLength(1000)]
        public string listName
        {
            get
            {
                return _listName;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid list name");
                }

                _listName = value;
            }
        }
    }
}
