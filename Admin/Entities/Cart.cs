﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RnR.Admin.Entities
{
    [Table("Cart", Schema="PnPDB")]
    public class Cart
    {
        private int _cartID;
        private int _userID;

        public ICollection<CartItems> CartItems;

        [Key]
        [Required(ErrorMessage = "Cart ID is required")]
        [Column("cartID")]
        public int cartID
        {
            get
            {
                return _cartID;
            }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("Invalid cart ID");
                }

                _cartID = value;
            }
        }

        [ForeignKey("userID")]
        [Required(ErrorMessage = "Customer ID is required")]
        [Column("userID")]
        public int userID
        {
            get
            {
                return _userID;
            }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("Invalid user ID");
                }

                _userID = value;
            }
        }
    }
}
