﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RnR.Admin.Entities
{
    [Table("PaidFor", Schema = "PnPDB")]
    public class PaidFor
    {
        private int _ID;
        private int _uid;
        public ICollection<PaidForItems> PaidForItems { get; set; }

        [Key]
        [Required(ErrorMessage = "ID is required"), Display(Name = "ID")]
        [Column("ID")]
        public int ID
        {
            get
            {
                return _ID;
            }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("Invalid ID");
                }

                _ID = value;
            }
        }

        [ForeignKey("uid")]
        [Required(ErrorMessage = "UID is required")]
        [Column("uid")]
        public int uid
        {
            get
            {
                return _uid;
            }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("Invalid UID");
                }

                _uid = value;
            }
        }
    }
}
