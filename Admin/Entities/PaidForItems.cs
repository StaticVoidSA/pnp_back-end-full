﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RnR.Admin.Entities
{
    [Table("PaidForItems", Schema = "PnPDB")]
    public class PaidForItems
    {
        private int _ID;
        private int _pid;
        private string _productTitle;
        private string _brand;
        private string _barcode;
        private string _quantity;
        private double _price;
        public virtual PaidFor PaidFor { get; set; }

        [Key]
        [Required(ErrorMessage = "ID is required")]
        [Column("ID")]
        public int ID
        {
            get
            {
                return _ID;
            }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("Invalid ID");
                }

                _ID = value;
            }
        }

        [ForeignKey("pid")]
        [Required(ErrorMessage = "Paid For Item ID is required")]
        [Column("pid")]
        public int pid
        {
            get
            {
                return _pid;
            }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("Invalid paid for item ID");
                }

                _pid = value;
            }
        }

        [Required(ErrorMessage = "Product Title is required")]
        [Column("productTitle"), DataType(DataType.Text), MaxLength(1000)]
        public string productTitle
        {
            get
            {
                return _productTitle;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid product title");
                }

                _productTitle = value;
            }
        }

        [Required(ErrorMessage = "Product Brand is required")]
        [Column("brand"), DataType(DataType.Text), MaxLength(1000)]
        public string brand
        {
            get
            {
                return _brand;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid product brand");
                }

                _brand = value;
            }
        }

        [Required(ErrorMessage = "Product Barcode is required")]
        [Column("barcode"), DataType(DataType.Text), MaxLength(1000)]
        public string barcode
        {
            get
            {
                return _barcode;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid product barcode");
                }

                _barcode = value;
            }
        }

        [Required(ErrorMessage = "Product Quantity is required")]
        [Column("quantity"), DataType(DataType.Text), MaxLength(1000)]
        public string quantity
        {
            get
            {
                return _quantity;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid product quantity");
                }

                _quantity = value;
            }
        }

        [Required(ErrorMessage = "Product Price is required")]
        [Column("price")]
        public double price
        {
            get
            {
                return _price;
            }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("Invalid product price");
                }

                _price = value;
            }
        }
    }
}
