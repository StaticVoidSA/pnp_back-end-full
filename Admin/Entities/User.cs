﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RnR.Admin.Entities
{
    [Table("Users", Schema="PnPDB")]
    public class User
    {
        private int _userID;
        private string _firstName;
        private string _lastName;
        private string _email;
        private string _encPassword;
        private DateTime _doj;
        private string _userRole;

        public virtual List<Favorites> Favorites { get; set; }
        public virtual List<FavoriteItems> FavoritesItems { get; set; }
        public virtual List<Cart> Carts { get; set; }
        public virtual List<CartItems> CartItems { get; set; }
        public virtual List<Collections> Collections { get; set; }
        public virtual List<Deliveries> Deliveries { get; set; }
        public virtual List<Address> Addresses { get; set; }
        public virtual List<PaidFor> PaidFor { get; set; }
        public virtual List<PaidForItems> PaidForItems { get; set; }
        public virtual List<ShoppingLists> ShoppingLists { get; set; }
        public virtual List<ShoppingListItems> ShoppingListItems { get; set; }

        [Key]
        [Required(ErrorMessage = "User ID is required"), Display(Name = "User ID")]
        [Column("userID")]
        public int userID
        {
            get
            {
                return _userID;
            }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("Invalid product ID");
                }

                _userID = value;
            }
        }

        [Required(ErrorMessage = "User First Name is required"), Display(Name = "First Name")]
        [Column("firstName"), DataType(DataType.Text), MaxLength(100)]
        public string firstName
        {
            get
            {
                return _firstName;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid user first name");
                }

                _firstName = value;
            }
        }

        [Required(ErrorMessage = "User Last Name is required"), Display(Name = "Last Name")]
        [Column("lastName"), DataType(DataType.Text), MaxLength(100)]
        public string lastName
        {
            get
            {
                return _lastName;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid user last name");
                }

                _lastName = value;
            }
        }

        [Required(ErrorMessage = "User Email is required"), Display(Name = "Email")]
        [Column("email"), DataType(DataType.Text), MaxLength(100)]
        public string email
        {
            get
            {
                return _email;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid user email");
                }

                _email = value;
            }
        }

        [Required(ErrorMessage = "User Password is required"), Display(Name = "Password")]
        [Column("encPassword"), DataType(DataType.Text), MaxLength(100)]
        public string encPassword
        {
            get
            {
                return _encPassword;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid user password");
                }

                _encPassword = value;
            }
        }

        [Required(ErrorMessage = "User DOJ is required"), Display(Name = "DOJ")]
        [Column("doj"), DataType(DataType.DateTime)]
        public DateTime doj
        {
            get
            {
                return _doj;
            }
            set
            {
                _doj = value;
            }
        }

        [Required(ErrorMessage = "User Role is required"), Display(Name = "User Role")]
        [Column("userRole"), DataType(DataType.Text), MaxLength(10)]
        public string userRole
        {
            get
            {
                return _userRole;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid user role");
                }

                _userRole = value;
            }
        }
    }
}
