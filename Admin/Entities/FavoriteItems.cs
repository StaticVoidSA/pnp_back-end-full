﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RnR.Admin.Entities
{
    [Table("FavoriteItems", Schema = "PnPDB")]
    public class FavoriteItems
    {
        private int _ID;
        private int _favID;
        private string _descr;
        private string _productTitle;
        private string _brand;
        private string _uri;
        private string _barcode;
        private string _quantity;
        private int _productID;
        private double _price;
        public Favorites Favorites { get; set; }

        [Key]
        [Required(ErrorMessage = "ID is required")]
        [Column("ID")]
        public int ID
        {
            get
            {
                return _ID;
            }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("Invalid ID");
                }

                _ID = value;
            }
        }

        [ForeignKey("favID")]
        [Required(ErrorMessage = "Favorite Item ID is required")]
        [Column("favID")]
        public int favID
        {
            get
            {
                return _favID;
            }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("Invalid Favorite ID");
                }

                _favID = value;
            }
        }

        [Required(ErrorMessage = "Favorite Item Description is required"), Display(Name = "Description")]
        [Column("description"), DataType(DataType.Text), MaxLength(1000)]
        public string descr
        {
            get
            {
                return _descr;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid favorite item description");
                }

                _descr = value;
            }
        }

        [Required(ErrorMessage = "Favorite Item Product Title is required"), Display(Name = "Product Title")]
        [Column("productTitle"), DataType(DataType.Text), MaxLength(1000)]
        public string productTitle
        {
            get
            {
                return _productTitle;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid favorite item product title");
                }

                _productTitle = value;
            }
        }

        [Required(ErrorMessage = "Favorite Item Brand is required"), Display(Name = "Brand")]
        [Column("brand"), DataType(DataType.Text), MaxLength(1000)]
        public string brand
        {
            get
            {
                return _brand;
            }
            set
            {
                if (string.IsNullOrEmpty(value) || !value.Equals(typeof(string)))
                {
                    throw new Exception("Invalid favorite item brand");
                }

                _brand = value;
            }
        }

        [Required(ErrorMessage = "Favorite Item URI is required"), Display(Name = "URI")]
        [Column("uri"), DataType(DataType.Text), MaxLength(1000)]
        public string uri
        {
            get
            {
                return _uri;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid favorite item uri");
                }

                _uri = value;
            }
        }

        [Required(ErrorMessage = "Favorite Item Barcode is required"), Display(Name = "Barcode")]
        [Column("barcode"), DataType(DataType.Text), MaxLength(1000)]
        public string barcode
        {
            get
            {
                return _barcode;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid favorite item barcode");
                }

                _barcode = value;
            }
        }

        [Required(ErrorMessage = "Favorite Item Quantity is required"), Display(Name = "Quantity")]
        [Column("quantity"), DataType(DataType.Text), MaxLength(1000)]
        public string quantity
        {
            get
            {
                return _quantity;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid favorite item quantity");
                }

                _quantity = value;
            }
        }

        [Required(ErrorMessage = "Favorite Item Product ID is required"), Display(Name = "Product ID")]
        [Column("productID"), DataType(DataType.Text), MaxLength(1000)]
        public int productID
        {
            get
            {
                return _productID;
            }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("Invalid Product ID");
                }

                _productID = value;
            }
        }

        [Required(ErrorMessage = "Favorite Item Product Price is required"), Display(Name = "Product Price")]
        [Column("price")]
        public double price
        {
            get
            {
                return _price;
            }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("Invalid Product Price");
                }

                _price = value;
            }
        }
    }
}
