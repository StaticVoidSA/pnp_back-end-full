﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RnR.Admin.Entities
{
    [Table("CartItems", Schema="PnPDB")]
    public class CartItems 
    {
        private int _ID;
        private int _cartID;
        private string _productTitle;
        private string _brand;
        private string _barcode;
        private int _quantity;
        private double _price;
        public virtual Cart Cart { get; set; }

        [Key]
        [Required(ErrorMessage = "Cart Item Customer ID is required")]
        [Column("ID")]
        public int ID
        {
            get
            {
                return _ID;
            }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("Invalid cart item ID");
                }

                _ID = value;
            }
        }

        [ForeignKey("cartID")]
        [Required(ErrorMessage = "Cart Item ID is required")]
        [Column("cartID")]
        public int cartID
        {
            get
            {
                return _cartID;
            }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("Invalid cart item ID");
                }

                _cartID = value;
            }
        }

        [Required(ErrorMessage = "Cart Item Title is required")]
        [Column("productTitle"), DataType(DataType.Text), MaxLength(1000)]
        public string productTitle
        {
            get
            {
                return _productTitle;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid cart item product title");
                }

                _productTitle = value;
            }
        }

        [Required(ErrorMessage = "Cart Item Brand is required")]
        [Column("brand"), DataType(DataType.Text), MaxLength(1000)]
        public string brand
        {
            get
            {
                return _brand;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid cart item product brand");
                }

                _brand = value;
            }
        }

        [Required(ErrorMessage = "Cart Item Barcode is required")]
        [Column("barcode"), DataType(DataType.Text), MaxLength(1000)]
        public string barcode
        {
            get
            {
                return _barcode;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid cart item product barcode");
                }

                _barcode = value;
            }
        }

        [Required(ErrorMessage = "Cart Item Quantity is required")]
        [Column("quantity")]
        public int quantity
        {
            get
            {
                return _quantity;
            }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("Invalid cart item quantity");
                }

                _quantity = value;
            }
        }

        [Required(ErrorMessage = "Cart Item Price is required")]
        [Column("price")]
        public double price
        {
            get
            {
                return _price;
            }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("Invalid cart item price");
                }

                _price = value;
            }
        }
    }
}
