﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RnR.Admin.Entities
{
    [Table("AddressesNew", Schema="PnPDB")]
    public class Address
    {
        private int _ID;
        private int _addressID;
        private string _userAddress;
        private string _addressNickName;
        private string _isDefault;
        public User User { get; set; }

        [Key]
        [Required(ErrorMessage = "Customer Address ID is required")]
        [Column("ID")]
        public int ID
        {
            get
            {
                return _ID;
            }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("Invalid user ID");
                }

                _ID = value;
            }
        }

        [ForeignKey("addressID")]
        [Required(ErrorMessage = "Address ID is required")]
        [Column("addressID")]
        public int addressID
        {
            get
            {
                return _addressID;
            }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("Invalid user address ID");
                }

                _addressID = value;
            }
        }

        [Required(ErrorMessage = "Customer Address is required")]
        [Column("userAddress"), DataType(DataType.Text), MaxLength(1000)]
        public string userAddress
        {
            get
            {
                return _userAddress;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid user address");
                }

                _userAddress = value;
            }
        }

        [Required(ErrorMessage = "Customer Address NickName is required")]
        [Column("addressNickName"), DataType(DataType.Text), MaxLength(1000)]
        public string addressNickName
        {
            get
            {
                return _addressNickName;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid address nickname");
                }

                _addressNickName = value;
            }
        }

        [Required(ErrorMessage = "Address IsDefault is required")]
        [Column("isDefault"), DataType(DataType.Text), MaxLength(10)]
        public string isDefault
        {
            get
            {
                return _isDefault;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid user address");
                }

                _isDefault = value;
            }
        }
    }
}
