﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RnR.Admin.Entities
{
    [Table("Favorites", Schema = "PnPDB")]
    public class Favorites
    {
        private int _ID;
        private int _currentUserID;
        public ICollection<FavoriteItems> FavoriteItems { get; set; }

        [Key]
        [Required(ErrorMessage = "Favorite ID is required")]
        [Column("ID")]
        public int ID
        {
            get
            {
                return _ID;
            }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("Invalid favorite ID");
                }

                _ID = value;
            }
        }

        [ForeignKey("currentUserID")]
        [Required(ErrorMessage = "Current User ID is required")]
        [Column("currentUserID")]
        public int currentUserID
        {
            get
            {
                return _currentUserID;
            }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("Invalid current user ID");
                }

                _currentUserID = value;
            }
        }
    }
}
