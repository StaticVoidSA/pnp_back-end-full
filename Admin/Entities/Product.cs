﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RnR.Admin.Entities
{
    [Table("Catalogue", Schema="PnPDB")]
    public class Product
    {
        private int _productID;
        private string _title;
        private string _category;
        private string _brand;
        private string _uri;
        private double _price;
        private string _descr;
        private string _features;
        private string _USAGE;
        private string _quantity;
        private string _barcode;

        [Key]
        //[Required(ErrorMessage = "Product ID is required")]
        [Column("productID")]
        public int productID
        {
            get
            {
                return _productID;
            }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("Invalid product ID");
                }

                _productID = value;
            }
        }

        [Required(ErrorMessage = "Product Title is required")]
        [Column("title"), DataType(DataType.Text), MaxLength(100)]
        public string title
        {
            get
            {
                return _title;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid product title");
                }

                _title = value;
            }
        }

        [Required(ErrorMessage = "Product Category is required")]
        [Column("category"), DataType(DataType.Text), MaxLength(100)]
        public string category
        {
            get
            {
                return _category;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid product category");
                }

                _category = value;
            }
        }

        [Required(ErrorMessage = "Product Brand is required")]
        [Column("brand"), DataType(DataType.Text), MaxLength(100)]
        public string brand
        {
            get
            {
                return _brand;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid product brand");
                }

                _brand = value;
            }
        }

        [Required(ErrorMessage = "Product URI is required")]
        [Column("uri"), DataType(DataType.Text), MaxLength(100)]
        public string uri
        {
            get
            {
                return _uri;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid product URI");
                }

                _uri = value;
            }
        }

        [Required(ErrorMessage = "Product Price is required")]
        [Column("price")]
        public double price
        {
            get
            {
                return _price;
            }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("Invalid product price");
                }

                _price = value;
            }
        }

        [Required(ErrorMessage = "Product Description is required")]
        [Column("descr"), DataType(DataType.Text), MaxLength(1000)]
        public string descr
        {
            get
            {
                return _descr;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid product description");
                }

                _descr = value;
            }
        }

        [Required(ErrorMessage = "Product Features is required")]
        [Column("features"), DataType(DataType.Text), MaxLength(1000)]
        public string features
        {
            get
            {
                return _features;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid product feature");
                }

                _features = value;
            }
        }

        [Required(ErrorMessage = "Product Usage is required")]
        [Column("_usage"), DataType(DataType.Text), MaxLength(1000)]
        public string _usage
        {
            get
            {
                return _USAGE;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid product usage");
                }

                _USAGE = value;
            }
        }

        [Required(ErrorMessage = "Product Quantity is required")]
        [Column("quantity"), DataType(DataType.Text), MaxLength(100)]
        public string quantity
        {
            get
            {
                return _quantity;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid product quantity");
                }

                _quantity = value;
            }
        }

        [Required(ErrorMessage = "Product Barcode is required")]
        [Column("barcode"), DataType(DataType.Text), MaxLength(100)]
        public string barcode
        {
            get
            {
                return _barcode;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid product barcode");
                }

                _barcode = value;
            }
        }
    }
}
