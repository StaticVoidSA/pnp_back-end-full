﻿using System;
using System.Collections.Generic;
using RnR.Security.Models;
using RnR.Admin.Entities;

namespace RnR.Admin.Repositories
{
    public interface IUserRepository
    {
        LoginDTOModel ValidateUser(LoginModel userModel, IEnumerable<User> users);
    }
}
