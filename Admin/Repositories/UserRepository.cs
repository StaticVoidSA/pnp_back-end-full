﻿using System.Collections.Generic;
using System.Linq;
using RnR.Security.Models;
using RnR.Admin.Entities;
using RnR.Services.HashingService;

namespace RnR.Admin.Repositories
{
    public class UserRepository : IUserRepository
    {
        public LoginDTOModel ValidateUser(LoginModel userModel, IEnumerable<User> users)
        {
            LoginDTOModel _user = new LoginDTOModel()
            {
                email = null,
                password = null,
                role = null
            };

            if (!string.IsNullOrEmpty(userModel.password) || !string.IsNullOrEmpty(userModel.email))
            {
                var encPassword = UserHasherService.encrypt(userModel.password);

                var user = users.Where(u => u.email == userModel.email && u.encPassword == encPassword).FirstOrDefault();

                _user.email = user.email;
                _user.password = encPassword;
                _user.role = user.userRole;

                return _user;
            }
            else
            {
                return _user;
            }
        }
    }
}
