﻿using System;
using System.Diagnostics;
using System.Linq;
using RnR.Admin.DbContexts;
using RnR.Admin.Models;
using Microsoft.AspNetCore.Mvc;
using RnR.Admin.Entities;
using RnR.Services.HashingService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using RnR.Admin.Repositories;
using RnR.Security.TokenServices;
using Microsoft.AspNetCore.Http;
using RnR.Security.Models;
using RnR.Data.Logger;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Admin.Controllers
{
    [ApiController]
    public class AdminLoginController : Controller
    {
        private MyDbContext dbContext;
        private readonly IConfiguration config;
        private readonly IUserRepository userRepo;
        private readonly ITokenService tokenService;
        private string generatedToken;
        private LogService logService;

        public AdminLoginController(MyDbContext _context, IConfiguration _config, IUserRepository _userRepo, ITokenService _tokenService)
        {
            dbContext = _context;
            config = _config;
            userRepo = _userRepo;
            tokenService = _tokenService;
            logService = new LogService();
        }

        [AllowAnonymous]
        [HttpGet, Route("/Login")]
        public IActionResult Login()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost, Route("/Login")]
        public IActionResult Login([FromForm] LoginModel model)
        {
            var encPassword = UserHasherService.encrypt(model.password);

            var query = from usr in this.dbContext.Users
                        where usr.encPassword == encPassword
                            && usr.email == model.email
                            && usr.userRole == "Admin"
                        select usr;

            if (query.Count() <= 0)
            {
                ViewBag.FailedLogin = "Unable to login, please try again";
                logService.AdminLogWrite("Error:", string.Format("Unable to login user {0}", arg0: model.email));
                return View(model);
            }
            else
            {
                var user = query.FirstOrDefault<User>();

                LoginDTOModel validUser = new LoginDTOModel()
                {
                    email = user.email,
                    password = user.encPassword,
                    role = user.userRole
                };

                if (validUser.role == "Admin" && validUser.email == model.email)
                {
                    generatedToken = tokenService.BuildToken(config["Jwt:Key"].ToString(),
                        config["Jwt:Issuer"].ToString(),
                        config["Jwt:Audience"].ToString(),
                        validUser);

                    HttpContext.Session.SetString("Token", generatedToken);
                    HttpContext.Session.SetString("UserName", user.firstName);
                    HttpContext.Session.SetString("UserEmail", user.email);
                    logService.AdminLogWrite("Info:", string.Format("Administrator: {0} Logged In...", arg0: validUser.email));

                    return Redirect("/AdminLanding/Index");
                }
                else
                {
                    ViewBag.FailedLogin = "Unable to login, please try again";
                    return View(model);
                }
            }
        }
        
        [HttpGet, Route("/Home/Error")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
