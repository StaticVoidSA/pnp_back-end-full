﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using RnR.Admin.DbContexts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RnR.Admin.Models;
using X.PagedList;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Admin.Controllers
{
    [ApiController]
    public class AdminCartController : Controller
    {
        private MyDbContext dbContext;

        public AdminCartController(MyDbContext context)
        {
            dbContext = context;
        }

        [Authorize]
        [HttpGet, Route("/AdminCart")]
        public async Task<IActionResult> Index(string sortOrder, string searchString, string currentFilter, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.UserIDSortParam = sortOrder == "id" ? "id_desc" : "id";
            ViewBag.UserNameSortParam = sortOrder == "userName" ? "userName_desc" : "userName";
            ViewBag.ProductTitleSortParam = sortOrder == "productTitle" ? "productTitle_desc" : "productTitle";
            ViewBag.BrandSortParam = sortOrder == "brand" ? "brand_desc" : "brand";
            ViewBag.QuantitySortParam = sortOrder == "quantity" ? "quantity_desc" : "quantity";
            ViewBag.PriceSortParam = sortOrder == "price" ? "price_desc" : "price";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
            ViewBag.UserToken = HttpContext.Session.GetString("Token");

            var cartItems = await (from usr in this.dbContext.Users
                            join crt in this.dbContext.CartItems on usr.userID equals crt.cartID
                            select new CartItemsViewModel()
                            {
                                ID = crt.ID,
                                cartID = crt.cartID,
                                productTitle = crt.productTitle,
                                brand = crt.brand,
                                barcode = crt.barcode,
                                quantity = crt.quantity,
                                price = crt.price,
                                fullUserName = usr.firstName + " " + usr.lastName
                            }).ToListAsync();

            if (!String.IsNullOrEmpty(searchString))
            {
                ViewBag.NoCartItemsFound = null;

                cartItems = await (cartItems.Where(crt => crt.productTitle.Contains(searchString)
                    || crt.brand.Contains(searchString)
                    || crt.fullUserName.Contains(searchString))).ToListAsync();

                if (!cartItems.Any())
                {
                    ViewBag.NoCartItemsFound = "No Cart Items Found";
                }
            }

            switch (sortOrder)
            {
                case "id_desc":
                    cartItems = await (cartItems.OrderByDescending(crt => crt.cartID)).ToListAsync();
                    break;
                case "id":
                    cartItems = await (cartItems.OrderBy(crt => crt.cartID)).ToListAsync();
                    break;
                case "userName_desc":
                    cartItems = await (cartItems.OrderByDescending(crt => crt.fullUserName)).ToListAsync();
                    break;
                case "userName":
                    cartItems = await (cartItems.OrderBy(crt => crt.fullUserName)).ToListAsync();
                    break;
                case "productTitle_desc":
                    cartItems = await (cartItems.OrderByDescending(crt => crt.productTitle)).ToListAsync();
                    break;
                case "productTitle":
                    cartItems = await (cartItems.OrderBy(crt => crt.productTitle)).ToListAsync();
                    break;
                case "brand_desc":
                    cartItems = await (cartItems.OrderByDescending(crt => crt.brand)).ToListAsync();
                    break;
                case "brand":
                    cartItems = await (cartItems.OrderBy(crt => crt.brand)).ToListAsync();
                    break;
                case "quantity_desc":
                    cartItems = await (cartItems.OrderByDescending(crt => crt.quantity)).ToListAsync();
                    break;
                case "quantity":
                    cartItems = await (cartItems.OrderBy(crt => crt.quantity)).ToListAsync();
                    break;
                case "price_desc":
                    cartItems = await (cartItems.OrderByDescending(crt => crt.price)).ToListAsync();
                    break;
                case "price":
                    cartItems = await (cartItems.OrderBy(crt => crt.price)).ToListAsync();
                    break;
                default:
                    break;
            }

            int pageSize = 10;
            int pageNumber = (page ?? 1);

            return View(cartItems.ToPagedList(pageNumber, pageSize));
        }

        [Authorize]
        [HttpGet, Route("/Home/Error")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
