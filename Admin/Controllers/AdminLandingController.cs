﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RnR.Admin.DbContexts;
using RnR.Data.Models;
using X.PagedList;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Admin.Controllers
{
    [ApiController]
    public class AdminLandingController : Controller
    {
        private MyDbContext dbContext;

        public AdminLandingController(MyDbContext _context)
        {
            dbContext = _context;
        }

        [Authorize]
        [HttpGet, Route("/AdminLanding/Index")]
        public async Task<IActionResult> Index()
        {
            ViewBag.UserToken = HttpContext.Session.GetString("Token");
            ViewBag.UserName = HttpContext.Session.GetString("UserName");

            var totalEarnings = 0.0;

            var paidItems = await (from pd in this.dbContext.PaidForItems
                                   select new PaidItems()
                                   {
                                       price = pd.price,
                                       productID = pd.pid,
                                       title = pd.productTitle,
                                       userID = pd.pid,
                                       brand = pd.brand,
                                       barcode = pd.barcode,
                                       quantity = Convert.ToInt32(pd.quantity)

                                   }).ToListAsync();

            var brandsModel = await (from prd in this.dbContext.Products
                                     select new Product()
                                     { brand = prd.brand }).Distinct().OrderBy(b => b.brand).ToListAsync();

            ViewBag.CollectionItemsCount = await (from col in this.dbContext.Collections select col).CountAsync();
            ViewBag.DeliveriesItemsCount = await (from del in this.dbContext.Deliveries select del).CountAsync();
            ViewBag.UsersCount = await (from usr in this.dbContext.Users select usr).CountAsync();
            ViewBag.AdminUsersCount = await (from adm in this.dbContext.Users where adm.userRole == "Admin" select adm).CountAsync();
            ViewBag.ProductsCount = await (from prd in this.dbContext.Products select prd).CountAsync();
            ViewBag.BrandsCount = await (from prd in this.dbContext.Products select prd.brand).Distinct().CountAsync();
            ViewBag.ShoppingListsCount = await (from shplst in this.dbContext.ShoppingLists select shplst).CountAsync();
            ViewBag.ShoppingListItemsCount = await (from shplistitems in this.dbContext.ShoppingListItems select shplistitems).CountAsync();
            ViewBag.FavoriteItemsCount = await (from fav in this.dbContext.FavoriteItems select fav).CountAsync();

            ViewBag.RegularUsersCount = ViewBag.UsersCount - ViewBag.AdminUsersCount;
            ViewBag.TotalEarnings = totalEarnings;

            if (paidItems.Count() > 0)
            {
                var count = 0;

                foreach (var item in paidItems)
                {
                    count += item.quantity;
                    totalEarnings += (item.price * item.quantity);
                }

                ViewBag.PaidItemsCount = count;
                ViewBag.TotalEarnings = string.Format(new CultureInfo("en-ZA"), "{0:C}", Math.Round(totalEarnings, 2));
            }

            return View(brandsModel);
        }

        [Authorize]
        [HttpGet, Route("/AdminLanding/BrandDetails")]
        public async Task<IActionResult> BrandDetails(string sortOrder, string searchString, string currentFilter, int? page, string brand)
        {
            ViewBag.UserToken = HttpContext.Session.GetString("Token");
            ViewBag.HasProducts = null;
            ViewBag.Brand = brand;

            ViewBag.CurrentSort = sortOrder;
            ViewBag.IDSortParam = sortOrder == "id" ? "id_desc" : "id";
            ViewBag.TitleSortParam = sortOrder == "title" ? "title_desc" : "title";
            ViewBag.BrandSortParam = sortOrder == "brand" ? "brand_desc" : "brand";
            ViewBag.CategorySortParam = sortOrder == "category" ? "category_desc" : "category";
            ViewBag.QuantitySortParam = sortOrder == "quantity" ? "quantity_desc" : "quantity";
            ViewBag.PriceSortParam = sortOrder == "price" ? "price_desc" : "price";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            var products = await (from prod in this.dbContext.Products
                                  where prod.brand == brand
                                  select prod).ToListAsync();

            if (!String.IsNullOrEmpty(searchString))
            {
                ViewBag.NoProductsFound = null;

                products = await (products.Where(product => product.title.Contains(searchString)
                    || product.brand.Contains(searchString)
                    || product.barcode.Contains(searchString)
                    || product.category.Contains(searchString)
                    || product.quantity.Contains(searchString))).ToListAsync();

                if (!products.Any())
                {
                    ViewBag.NoProductsFound = "No products found";
                }
            }

            switch (sortOrder)
            {
                case "id_desc":
                    products = await (products.OrderByDescending(product => product.productID)).ToListAsync();
                    break;
                case "id":
                    products = await (products.OrderBy(product => product.productID)).ToListAsync();
                    break;
                case "title_desc":
                    products = await (products.OrderByDescending(product => product.title)).ToListAsync();
                    break;
                case "title":
                    products = await (products.OrderBy(product => product.title)).ToListAsync();
                    break;
                case "brand_desc":
                    products = await (products.OrderByDescending(product => product.brand)).ToListAsync();
                    break;
                case "brand":
                    products = await (products.OrderBy(product => product.brand)).ToListAsync();
                    break;
                case "category_desc":
                    products = await (products.OrderByDescending(product => product.category)).ToListAsync();
                    break;
                case "category":
                    products = await (products.OrderBy(product => product.category)).ToListAsync();
                    break;
                case "quantity_desc":
                    products = await (products.OrderByDescending(product => product.quantity)).ToListAsync();
                    break;
                case "quantity":
                    products = await (products.OrderBy(product => product.quantity)).ToListAsync();
                    break;
                case "price_desc":
                    products = await (products.OrderByDescending(product => product.price)).ToListAsync();
                    break;
                case "price":
                    products = await (products.OrderBy(product => product.price)).ToListAsync();
                    break;
            }

            int pageSize = 5;
            int pageNumber = (page ?? 1);

            return View(products.ToPagedList(pageNumber, pageSize));
        }

        [Authorize]
        [HttpPost, Route("/AdminLanding/BrandDetails")]
        public async Task<IActionResult> BrandDetails([FromForm] string brand, int? page)
        {
            ViewBag.UserToken = HttpContext.Session.GetString("Token");
            ViewBag.HasProducts = null;
            ViewBag.Brand = brand;

            var products = await (from prd in this.dbContext.Products
                                  where prd.brand == brand
                                  select prd).ToListAsync();

            if (products.Count() > 0)
            {
                ViewBag.HasProducts = true;
            }

            int pageSize = 5;
            int pageNumber = (page ?? 1);

            return View(products.ToPagedList(pageNumber, pageSize));
        }
    }
}
