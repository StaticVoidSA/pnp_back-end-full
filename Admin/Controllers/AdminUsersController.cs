using System;
using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using RnR.Admin.Models;
using RnR.Admin.Entities;
using RnR.Admin.DbContexts;
using X.PagedList;
using RnR.Services.HashingService;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace RnR.Admin.Controllers
{
    [ApiController]
    public class AdminUsersController : Controller
    {
        private MyDbContext dbContext;
        private UserService userService;

        public AdminUsersController(MyDbContext context)
        {
            dbContext = context;
            userService = new UserService(dbContext);
        }

        [Authorize]
        [HttpGet, Route("/AdminUsers")]
        public async Task<IActionResult> Index(string sortOrder, string searchString, string currentFilter, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParam = sortOrder == "name" ? "name_desc" : "name";
            ViewBag.UserRoleSortParam = sortOrder == "userRole" ? "userRole_desc" : "userRole";
            ViewBag.LastNameSortParam = sortOrder == "lastName" ? "lastName_desc" : "lastName";
            ViewBag.EmailSortParam = sortOrder == "email" ? "email_desc" : "email";
            ViewBag.DateSortParam = sortOrder == "date" ? "date_desc" : "date";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
            ViewBag.UserToken = HttpContext.Session.GetString("Token");

            var users = await (from usr in this.dbContext.Users
                        select usr).ToListAsync();

            if (!String.IsNullOrEmpty(searchString))
            {
                ViewBag.NoUsersFound = null;

                users = await (users.Where(usr => usr.lastName.Contains(searchString)
                    || usr.firstName.Contains(searchString)
                    || usr.email.Contains(searchString))).ToListAsync();

                if (!users.Any())
                {
                    ViewBag.NoUsersFound = "No users found";
                }
            }

            switch (sortOrder)
            {
                case "userRole_desc":
                    users = await (users.OrderByDescending(user => user.userRole)).ToListAsync();
                    break;
                case "userRole":
                    users = await (users.OrderBy(user => user.userRole)).ToListAsync();
                    break;
                case "name":
                    users = await (users.OrderBy(user => user.firstName)).ToListAsync();
                    break;
                case "name_desc":
                    users = await (users.OrderByDescending(user => user.firstName)).ToListAsync();
                    break;
                case "lastName":
                    users = await (users.OrderBy(user => user.lastName)).ToListAsync(); ;
                    break;
                case "lastName_desc":
                    users = await (users.OrderByDescending(user => user.lastName)).ToListAsync();
                    break;
                case "email":
                    users = await (users.OrderBy(user => user.email)).ToListAsync(); ;
                    break;
                case "email_desc":
                    users = await (users.OrderByDescending(user => user.email)).ToListAsync();
                    break;
                case "date":
                    users = await (users.OrderBy(user => user.doj)).ToListAsync(); ;
                    break;
                case "date_desc":
                    users = await (users.OrderByDescending(user => user.doj)).ToListAsync();
                    break;
                default:
                    users = await (users.OrderBy(user => user.lastName)).ToListAsync();
                    break;
            }

            int pageSize = 5;
            int pageNumber = (page ?? 1);

            return View(users.ToPagedList(pageNumber, pageSize));
        }

        [Authorize]
        [HttpGet, Route("/Details")]
        public async Task<IActionResult> Details(int userID, string email)
        {
            ViewBag.UserToken = HttpContext.Session.Get("Token");
            ViewBag.HasCartItems = null;
            ViewBag.HasPaidForItems = null;
            ViewBag.HasShoppingLists = null;
            ViewBag.HasFavorites = null;

            var user = await userService.getUser(userID, email);
            var cartItems = await userService.getCartItems(userID);
            var paidItems = await userService.getPaidItems(userID);
            var shoppingLists = await userService.getShoppingLists(userID);
            var favoriteItems = await userService.getFavoriteItems(userID);

            UserDetailsViewModel model = new UserDetailsViewModel()
            {
                firstName = user.firstName,
                lastName = user.lastName,
                email = user.email,
                userRole = user.userRole,
                doj = user.doj.ToShortDateString(),
                cartItems = new List<CartItems>(),
                paidForItems = new List<PaidForItems>(),
                shoppingLists = new List<ShoppingLists>(),
                favoriteItems = new List<FavoriteItems>()
            };

            if (cartItems.Any())
            {
                ViewBag.HasCartItems = true;

                foreach (var item in cartItems)
                {
                    model.cartItems.Add(new CartItems()
                    {
                        productTitle = item.productTitle,
                        brand = item.brand,
                        price = item.price,
                        quantity = item.quantity,
                        barcode = item.barcode
                    });
                }
            }

            if (paidItems.Any())
            {
                ViewBag.HasPaidForItems = true;

                foreach (var item in paidItems)
                {
                    model.paidForItems.Add(new PaidForItems()
                    {
                        productTitle = item.productTitle,
                        brand = item.brand,
                        price = item.price,
                        quantity = item.quantity,
                        barcode = item.barcode
                    });
                }
            }

            if (shoppingLists.Any())
            {
                ViewBag.HasShoppingLists = true;

                foreach (var list in shoppingLists)
                {
                    model.shoppingLists.Add(new ShoppingLists()
                    {
                        listName = list.listName,
                        userListID = list.ID
                    });
                }
            }

            if (favoriteItems.Any())
            {
                ViewBag.HasFavorites = true;

                foreach (var item in favoriteItems)
                {
                    model.favoriteItems.Add(new FavoriteItems()
                    {
                        favID = item.favID,
                        productTitle = item.productTitle,
                        brand = item.brand,
                        barcode = item.barcode,
                        price = item.price
                    });
                }
            }

            return View(model);
        }

        [Authorize]
        [HttpGet, Route("/Create")]
        public IActionResult Create()
        {
            ViewBag.UserToken = HttpContext.Session.GetString("Token");
            return View();
        }

        [Authorize]
        [HttpPost, Route("/Create")]
        public async Task<IActionResult> Create([FromForm] User user)
        {
            ViewBag.UserToken = HttpContext.Session.GetString("Token");

            string _encPassword = UserHasherService.encrypt(user.encPassword);

            var emailExists = await (from usr in this.dbContext.Users
                              where usr.email == user.email
                              select usr).ToListAsync();

            if (emailExists.Count() > 0)
            {
                ViewBag.ErrorMessage = "Email already exists, please use a different address";

                return View(user);
            }
            else
            {
                User _user = new User()
                {
                    firstName = user.firstName,
                    lastName = user.lastName,
                    email = user.email,
                    doj = DateTime.Now.ToLocalTime(),
                    encPassword = _encPassword,
                    userRole = user.userRole
                };

                try
                {
                    await dbContext.Users.AddAsync(_user);
                    await dbContext.SaveChangesAsync();
                    ViewBag.ErrorMessage = null;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    throw new Exception(e.InnerException.ToString());
                }

                return RedirectToAction("Index");
            }
        }

        [Authorize]
        [HttpGet, Route("/Edit")]
        public async Task<IActionResult> Edit(int userID, string email)
        {
            ViewBag.UserToken = HttpContext.Session.GetString("Token");

            var currentDetails = await userService.getUser(userID, email);

            return View(currentDetails);
        }

        [Authorize]
        [HttpPost, Route("/Edit")]
        public async Task<IActionResult> Edit([FromForm] User user)
        {
            var currentUser = await this.dbContext.Users
                .SingleOrDefaultAsync(u => u.userID == user.userID
                    && u.email == user.email
                    && u.encPassword == user.encPassword);

            if (currentUser != null)
            {
                currentUser.email = user.email;
                currentUser.firstName = user.firstName;
                currentUser.userRole = user.userRole;
                currentUser.lastName = user.lastName;
                currentUser.encPassword = user.encPassword;
                currentUser.userID = user.userID;
                currentUser.doj = user.doj;
            }

            try
            {
                await this.dbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw new Exception(ex.InnerException.ToString());
            }

            return RedirectToAction("Index");
        }

        [Authorize]
        [HttpGet, Route("/Delete")]
        public async Task<IActionResult> Delete(int userID, string email)
        {
            ViewBag.UserToken = HttpContext.Session.GetString("Token");

            var user = await userService.getUser(userID, email);
            var favoriteItems = await userService.getFavoriteItems(user.userID);
            var favorites = await userService.getFavorites(user.userID);
            var shoppingListItems = await userService.getShoppingListItems(user.userID);
            var shoppingLists = await userService.getShoppingLists(user.userID);
            var cartItems = await userService.getCartItems(user.userID);
            var carts = await userService.getCarts(user.userID);

            try
            {
                if (favoriteItems.GetEnumerator().MoveNext())
                {
                    foreach (var item in favoriteItems)
                    {
                        this.dbContext.FavoriteItems.Remove(item);
                    }

                    Console.WriteLine("Successfully removed all favorite items");
                    this.dbContext.SaveChanges();
                }

                if (favorites.GetEnumerator().MoveNext())
                {
                    foreach (var item in favorites)
                    {
                        this.dbContext.Favorites.Remove(item);
                    }

                    Console.WriteLine("Successfully removed all favorites");
                    this.dbContext.SaveChanges();
                }

                if (shoppingListItems.GetEnumerator().MoveNext())
                {
                    foreach (var item in shoppingListItems)
                    {
                        this.dbContext.ShoppingListItems.Remove(item);
                    }

                    Console.WriteLine("Successfully removed all shopping list items");
                    this.dbContext.SaveChanges();
                }

                if (shoppingLists.GetEnumerator().MoveNext())
                {
                    foreach (var item in shoppingLists)
                    {
                        this.dbContext.ShoppingLists.Remove(item);
                    }

                    Console.WriteLine(string.Format("Successfully removed all shopping lists"));
                    this.dbContext.SaveChanges();
                }

                if (cartItems.GetEnumerator().MoveNext())
                {
                    foreach (var item in cartItems)
                    {
                        this.dbContext.CartItems.Remove(item);
                    }

                    Console.WriteLine("Successfully removed cart items");
                    this.dbContext.SaveChanges();
                }

                if (carts.Count() > 0)
                {
                    foreach (var item in carts)
                    {
                        this.dbContext.Carts.Remove(item);
                    }

                    Console.WriteLine(string.Format("Successfully removed carts"));
                    this.dbContext.SaveChanges();
                }

                this.dbContext.Users.Remove(user);
                this.dbContext.SaveChanges();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw new Exception(e.Message);
            }

            return RedirectToAction("Index");
        }

        [Authorize]
        [HttpGet, Route("/Home/Error")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }

    public class UserService
    {

        private MyDbContext dbContext;

        public UserService(MyDbContext context)
        {
            dbContext = context;
        }

        #region Get All Users
        public async Task<User> getUser(int userID, string email)
        {
            var user = await (from u in this.dbContext.Users
                              where u.userID == userID && u.email == email
                              select new User()
                              {
                                  userID = u.userID,
                                  firstName = u.firstName,
                                  lastName = u.lastName,
                                  email = u.email,
                                  encPassword = u.encPassword,
                                  doj = u.doj,
                                  userRole = u.userRole
                              }).FirstOrDefaultAsync();

            return user;
        }
        #endregion

        #region Get User Favorites
        public async Task<List<FavoriteItems>> getFavoriteItems(int userID)
        {
            var favoriteItems = await (from f in this.dbContext.FavoriteItems
                                       where f.favID == userID
                                       select new FavoriteItems()
                                       {
                                           ID = f.ID,
                                           favID = f.favID,
                                           descr = f.descr,
                                           productID = f.productID,
                                           productTitle = f.productTitle,
                                           brand = f.brand,
                                           uri = f.uri,
                                           barcode = f.barcode,
                                           quantity = f.quantity,
                                           price = f.price

                                       }).ToListAsync();

            return favoriteItems;
        }
        #endregion


        #region Get User Favorites
        public async Task<List<Favorites>> getFavorites(int userID)
        {
            var favorites = await (from f in this.dbContext.Favorites
                                   where f.currentUserID == userID
                                   select new Favorites()
                                   {
                                       ID = f.ID,
                                       currentUserID = f.currentUserID
                                   }).ToListAsync();

            return favorites;
        }
        #endregion

        #region Get User Shopping List Items
        public async Task<List<ShoppingListItems>> getShoppingListItems(int userID)
        {
            var shoppingListItems = await (from i in this.dbContext.ShoppingListItems
                                           where i.listID == userID
                                           select new ShoppingListItems()
                                           {
                                               ID = i.ID,
                                               listID = i.listID,
                                               productTitle = i.productTitle,
                                               brand = i.brand,
                                               barcode = i.barcode,
                                               quantity = i.quantity,
                                               price = i.price,
                                               listName = i.listName
                                           }).ToListAsync();

            return shoppingListItems;
        }
        #endregion

        #region Get User Shopping Lists
        public async Task<List<ShoppingLists>> getShoppingLists(int userID)
        {
            var shoppingLists = await (from l in this.dbContext.ShoppingLists
                                       where l.userListID == userID
                                       select new ShoppingLists()
                                       {
                                           ID = l.ID,
                                           listName = l.listName,
                                           userListID = l.userListID
                                       }).ToListAsync();

            return shoppingLists;
        }
        #endregion

        #region Get User Cart Items
        public async Task<List<CartItems>> getCartItems(int userID)
        {
            var cartItems = await (from i in this.dbContext.CartItems
                                   where i.cartID == userID
                                   select new CartItems()
                                   {
                                       ID = i.ID,
                                       cartID = i.cartID,
                                       productTitle = i.productTitle,
                                       brand = i.brand,
                                       barcode = i.barcode,
                                       quantity = i.quantity,
                                       price = i.price
                                   }).ToListAsync();

            return cartItems;
        }
        #endregion

        #region Get User Carts
        public async Task<List<Cart>> getCarts(int userID)
        {
            var carts = await (from c in this.dbContext.Carts
                               where c.cartID == userID
                               select new Cart()
                               {
                                   cartID = c.cartID,
                                   userID = c.userID
                               }).ToListAsync();

            return carts;
        }
        #endregion

        #region Get User Paid Items
        public async Task<List<PaidForItems>> getPaidItems(int userID)
        {
            var paidItems = await (from p in this.dbContext.PaidFor
                                   join i in this.dbContext.PaidForItems
                                   on p.uid equals i.pid
                                   where p.uid == userID
                                   select new PaidForItems()
                                   {
                                       productTitle = i.productTitle,
                                       brand = i.brand,
                                       price = i.price,
                                       quantity = i.quantity,
                                       barcode = i.barcode
                                   }).ToListAsync();

            return paidItems;
        }
        #endregion
    }
}
