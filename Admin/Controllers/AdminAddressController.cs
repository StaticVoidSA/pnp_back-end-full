﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using RnR.Admin.DbContexts;
using Microsoft.AspNetCore.Mvc;
using RnR.Admin.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Admin.Controllers
{
    [ApiController]
    public class AdminAddressController : Controller
    {
        private MyDbContext dbContext;

        public AdminAddressController(MyDbContext context)
        {
            dbContext = context;
        }

        [Authorize]
        [HttpGet, Route("/AdminAddress")]
        public async Task<IActionResult> Index()
        {
            var addresses = await (from usr in this.dbContext.Users
                            join addr in this.dbContext.Addresses
                            on usr.userID equals addr.addressID
                            select new AddressesViewModel()
                            {
                                addressID = addr.addressID,
                                userAddress = addr.userAddress,
                                addressNickName = addr.addressNickName,
                                isDefault = addr.isDefault,
                                ID = addr.ID,
                                userFullName = usr.firstName + " " + usr.lastName

                            }).ToListAsync();

            ViewBag.UserToken = HttpContext.Session.GetString("Token");

            if (addresses.Count() > 0)
            {
                ViewBag.NoAddresses = null;
                return View(addresses);
            }
            else
            {
                ViewBag.NoAddresses = "There are no user addresses";
                return View();
            }
        }

        [Authorize]
        [HttpGet, Route("/Home/Error")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
