using System;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using RnR.Admin.Models;
using RnR.Admin.Entities;
using RnR.Admin.DbContexts;
using System.Linq;
using X.PagedList;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace RnR.Admin.Controllers
{
    [ApiController]
    public class AdminProductsController : Controller
    {
        private MyDbContext dbContext;

        public AdminProductsController(MyDbContext context)
        {
            dbContext = context;
        }

        [Authorize]
        [HttpGet, Route("/AdminProducts")]
        public async Task<IActionResult> Index(string sortOrder, string searchString, string currentFilter, int? page)
        {
            ViewBag.UserToken = HttpContext.Session.GetString("Token");
            ViewBag.CurrentSort = sortOrder;
            ViewBag.IDSortParam = sortOrder == "id" ? "id_desc" : "id";
            ViewBag.TitleSortParam = sortOrder == "title" ? "title_desc" : "title";
            ViewBag.BrandSortParam = sortOrder == "brand" ? "brand_desc" : "brand";
            ViewBag.CategorySortParam = sortOrder == "category" ? "category_desc" : "category";
            ViewBag.QuantitySortParam = sortOrder == "quantity" ? "quantity_desc" : "quantity";
            ViewBag.PriceSortParam = sortOrder == "price" ? "price_desc" : "price";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            var products = await (from prod in this.dbContext.Products
                           select prod).ToListAsync();

            if (!String.IsNullOrEmpty(searchString))
            {
                ViewBag.NoProductsFound = null;

                products = await (products.Where(product => product.title.Contains(searchString)
                    || product.brand.Contains(searchString)
                    || product.barcode.Contains(searchString)
                    || product.category.Contains(searchString)
                    || product.quantity.Contains(searchString))).ToListAsync();

                if (!products.Any())
                {
                    ViewBag.NoProductsFound = "No products found";
                }
            }

            switch (sortOrder)
            {
                case "id_desc":
                    products = await (products.OrderByDescending(product => product.productID)).ToListAsync();
                    break;
                case "id":
                    products = await (products.OrderBy(product => product.productID)).ToListAsync();
                    break;
                case "title_desc":
                    products = await (products.OrderByDescending(product => product.title)).ToListAsync();
                    break;
                case "title":
                    products = await (products.OrderBy(product => product.title)).ToListAsync();
                    break;
                case "brand_desc":
                    products = await (products.OrderByDescending(product => product.brand)).ToListAsync();
                    break;
                case "brand":
                    products = await (products.OrderBy(product => product.brand)).ToListAsync();
                    break;
                case "category_desc":
                    products = await (products.OrderByDescending(product => product.category)).ToListAsync();
                    break;
                case "category":
                    products = await (products.OrderBy(product => product.category)).ToListAsync();
                    break;
                case "quantity_desc":
                    products = await (products.OrderByDescending(product => product.quantity)).ToListAsync();
                    break;
                case "quantity":
                    products = await (products.OrderBy(product => product.quantity)).ToListAsync();
                    break;
                case "price_desc":
                    products = await (products.OrderByDescending(product => product.price)).ToListAsync();
                    break;
                case "price":
                    products = await (products.OrderBy(product => product.price)).ToListAsync();
                    break;
            }

            int pageSize = 10;
            int pageNumber = (page ?? 1);

            return View(products.ToPagedList(pageNumber, pageSize));
        }

        [Authorize]
        [HttpGet, Route("/CreateProduct")]
        public async Task<IActionResult> CreateProduct()
        {
            ViewBag.UserToken = HttpContext.Session.GetString("Token");

            ViewBag.Brands = await (from prd in this.dbContext.Products
                             select new Product()
                             { brand = prd.brand }).Distinct().OrderBy(b => b.brand).ToListAsync();

            ViewBag.Categories = await (from prd in this.dbContext.Products
                                 select new Product()
                                 { category = prd.category }).Distinct().OrderBy(c => c.category).ToListAsync();

            return View();
        }

        [Authorize]
        [HttpPost, Route("/CreateProduct")]
        public async Task<IActionResult> CreateProduct([FromForm] Product product)
        {
            ViewBag.UserToken = HttpContext.Session.GetString("Token");


            Product newProduct = new Product()
            {
                title = product.title,
                category = product.category,
                brand = product.brand,
                uri = product.uri,
                price = product.price,
                descr = product.descr,
                features = product.features,
                _usage = product._usage,
                quantity = product.quantity,
                barcode = product.barcode
            };

            try
            {
                await this.dbContext.Products.AddAsync(newProduct);
                await this.dbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw new Exception(ex.InnerException.ToString());
            }

            return RedirectToAction("Index", "AdminLanding");
        }

        [Authorize]
        [HttpGet, Route("/EditProduct")]
        public async Task<IActionResult> EditProduct(string barcode, int productID)
        {
            ViewBag.UserToken = HttpContext.Session.GetString("Token");

            var products = from prod in this.dbContext.Products
                           select prod;

            Product product = await products
                .Where(p => p.barcode == barcode && p.productID == productID)
                .FirstOrDefaultAsync();

            return View(product);
        }

        [Authorize]
        [HttpPost, Route("/EditProduct")]
        public async Task<IActionResult> EditProduct([FromForm] Product product)
        {
            ViewBag.UserToken = HttpContext.Session.GetString("Token");

            var currentProduct = await (from prd in this.dbContext.Products
                                 where prd.productID == product.productID
                                 select prd).SingleOrDefaultAsync();

            try
            {
                if (currentProduct != null)
                {
                    currentProduct.title = product.title;
                    currentProduct.barcode = product.barcode;
                    currentProduct.category = product.category;
                    currentProduct.brand = product.brand;
                    currentProduct.productID = product.productID;
                    currentProduct.descr = product.descr;
                    currentProduct.price = product.price;
                    currentProduct.quantity = product.quantity;
                    currentProduct.features = product.features;
                    currentProduct.uri = product.uri;
                    currentProduct._usage = product._usage;

                    await this.dbContext.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw new Exception(ex.InnerException.ToString());
            }

            return RedirectToAction("Index");
        }

        [Authorize]
        [HttpGet, Route("/ProductDetails")]
        public async Task<IActionResult> ProductDetails(string barcode, int productID)
        {
            ViewBag.UserToken = HttpContext.Session.GetString("Token");

            var products = await (from prod in this.dbContext.Products
                           select prod).ToListAsync();

            Product product = products
                .Where(p => p.barcode == barcode && p.productID == productID)
                .FirstOrDefault();

            return View(product);
        }

        [Authorize]
        [HttpGet, Route("/DeleteProduct")]
        public async Task<IActionResult> DeleteProduct(string barcode, int productID)
        {
            var currentProduct = await(from prd in this.dbContext.Products
                                 where prd.productID == productID && prd.barcode == barcode
                                 select prd).FirstOrDefaultAsync();

            try
            {
                this.dbContext.Remove(currentProduct);
                await this.dbContext.SaveChangesAsync();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw new Exception(ex.InnerException.ToString());
            }

            return RedirectToAction("Index", "AdminLanding");
        }

        [Authorize]
        [HttpGet, Route("/Home/Error")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
