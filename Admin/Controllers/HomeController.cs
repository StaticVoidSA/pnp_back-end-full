﻿using System.Diagnostics;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RnR.Admin.Models;
using RnR.Data.Logger;

namespace Admin.Controllers
{
    public class HomeController : Controller
    {
        private LogService logService;

        public HomeController()
        {
            this.logService = new LogService();
        }

        [AllowAnonymous]
        public IActionResult Index()
        {
            return View();
        }

        [Authorize]
        public IActionResult Logout()
        {
            var _userEmail = HttpContext.Session.GetString("UserEmail");
            logService.AdminLogWrite("Info:", string.Format("Administrator: {0} Logged Out...", arg0: _userEmail));
            return RedirectToAction("Index");
        }

        [AllowAnonymous]
        public IActionResult Privacy()
        {
            return View();
        }

        [AllowAnonymous]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
