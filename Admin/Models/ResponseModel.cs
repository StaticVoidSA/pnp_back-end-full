﻿using System;

namespace RnR.Admin.Models
{
    public class ResponseModel
    {
        public string Status { get; set; }
        public string Message { get; set; }
    }
}
