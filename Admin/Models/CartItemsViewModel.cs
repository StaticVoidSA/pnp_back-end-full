﻿using System;
using RnR.Admin.Entities;

namespace RnR.Admin.Models
{
    public class CartItemsViewModel : CartItems
    {
        private string _fullUserName;

        public CartItemsViewModel() : base()
        {

        }

        public string fullUserName
        {
            get
            {
                return _fullUserName;
            }
            set
            {
                if (value != string.Empty)
                {
                    _fullUserName = value;
                }
                else
                {
                    throw new Exception("User first name is required");
                }
            }
        }
    }
}
