﻿using System;
using RnR.Admin.Entities;

namespace RnR.Admin.Models
{
    public class AddressesViewModel : Address
    {
        private string _userFullName;

        public AddressesViewModel() : base()
        {

        }

        public string userFullName
        {
            get
            {
                return _userFullName;
            }
            set
            {
                if (value != string.Empty)
                {
                    _userFullName = value;
                }
                else
                {
                    throw new Exception("Full user name is required");
                }
            }
        }
    }
}
