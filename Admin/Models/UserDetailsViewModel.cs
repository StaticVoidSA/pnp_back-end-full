﻿using System;
using System.Collections.Generic;
using RnR.Admin.Entities;

namespace RnR.Admin.Models
{
    public class UserDetailsViewModel
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public string userRole { get; set; }
        public string doj { get; set; }
        public List<CartItems> cartItems { get; set; }
        public List<Address> addresses { get; set; }
        public List<PaidForItems> paidForItems { get; set; }
        public List<ShoppingLists> shoppingLists { get; set; }
        public List<ShoppingListItems> shoppingListItems { get; set; }
        public List<FavoriteItems> favoriteItems { get; set; }
    }
}
