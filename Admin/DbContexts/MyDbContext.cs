﻿using Microsoft.EntityFrameworkCore;
using RnR.Admin.Entities;

namespace RnR.Admin.DbContexts
{
    public class MyDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<CartItems> CartItems { get; set; }
        public DbSet<Cart> Carts { get; set; }
        public DbSet<Favorites> Favorites { get; set; }
        public DbSet<FavoriteItems> FavoriteItems { get; set; }
        public DbSet<ShoppingLists> ShoppingLists { get; set; }
        public DbSet<ShoppingListItems> ShoppingListItems { get; set; }
        public DbSet<PaidFor> PaidFor { get; set; }
        public DbSet<PaidForItems> PaidForItems { get; set; }
        public DbSet<Collections> Collections { get; set; }
        public DbSet<Deliveries> Deliveries { get; set; }

        public MyDbContext(DbContextOptions<MyDbContext> options) : base(options)
        {

        }
    }
}
