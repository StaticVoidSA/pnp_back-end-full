﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RnR.Security.Models
{
    public class LoginModel
    {
        private string _email;
        private string _password;

        [Required(ErrorMessage = "User email is required")]
        public string email
        {
            get
            {
                return _email;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid user email");
                }

                _email = value;
            }
        }

        [Required(ErrorMessage = "User password is required")]
        public string password
        {
            get
            {
                return _password;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Invalid user password");
                }

                _password = value;
            }
        }
    }
}
