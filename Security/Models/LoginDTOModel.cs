﻿using System;

namespace RnR.Security.Models
{
    public class LoginDTOModel
    {
        public string email { get; set; }
        public string password { get; set; }
        public string role { get; set; }
    }
}
