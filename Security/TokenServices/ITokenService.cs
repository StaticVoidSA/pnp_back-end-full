﻿using System;
using RnR.Security.Models;

namespace RnR.Security.TokenServices
{
    public interface ITokenService
    {
        string BuildToken(string key, string issuer, string audience, LoginDTOModel user);
        bool ValidateToken(string key, string issuer, string audience, string token);
    }
}
